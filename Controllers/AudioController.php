<?php

namespace App\Http\Controllers;

use App\Models\Ext;
use App\Models\Audio;
use App\Models\AudioTmp;
use App\Models\SistemaIvr;
use Illuminate\Http\Request;
use App\Utilities\DBErrorDecoder;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Storage;

/**
 * Controller for Audio Module
 *
 * @category Module_Controller
 * @package  NASVOX4.0
 * @author   Lucas Gómez <lucasgomez@nadserv.com>
 * @link     https://nasvox.nadserv.com
 */
class AudioController extends Controller
{
    public $data;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->data['audios'] = Audio::orderBy('id_audio')->get();
        // $this->data['pageName'] = __('page.audio.controller.pageName');
        // $this->data['section'] = __('page.audio.controller.section');
    }

    /**
     * Returns a json list of all audios 
     * with buttons for download, play and delete actions
     *
     * @return void
     */
    public function lista()
    {
        //SELECT id_ext, name, display_name FROM ext WHERE name <> '-' ORDER BY name;
        //SELECT id_audio, audio, descripcion, activo FROM audio ORDER BY id_audio

        $audios = Audio::orderBy('id_audio')->get();

        foreach ($audios as $audio) {

            $is_checked = $audio->activo ? 'checked' : '';

            $audio->activo = '<label class="mt-checkbox mt-checkbox-outline">';
            $audio->activo .= '<input type="checkbox" ';
            $audio->activo .= 'checked="' . $is_checked . '"';
            $audio->activo .= 'disabled><span></span></label>';


            $downloadButton = '<button id="download_' . $audio->id_audio . '" ';
            $downloadButton .= 'class="btn-link download" ';
            $downloadButton .= 'title="' . __('page.audio.tabla.tooltip.accion.download') . '">';
            $downloadButton .= '<span aria-hidden="true"'; 
            $downloadButton .= ' class="icon-link font-purple-seance"></span>';
            $downloadButton .= '</button>';

            $playButton = '<button id="play_' . $audio->id_audio . '" ';
            $playButton .= 'class="btn-link play" '; 
            $playButton .= 'title="'. __('page.audio.tabla.tooltip.accion.play') . '">';
            $playButton .= '<span aria-hidden="true" ';
            $playButton .= 'class="icon-control-play font-green-jungle"></span>';
            $playButton .= '</button>';

            $uploadButton = '<button id="upload_' . $audio->id_audio . '" ';
            $uploadButton .= 'class="btn-link upload" '; 
            $uploadButton .= 'title="' . __('page.audio.tabla.tooltip.accion.upload') . '">';
            $uploadButton .= '<span aria-hidden="true" ';
            $uploadButton .= 'class="icon-cloud-upload  font-yellow-gold"></span>';
            $uploadButton .= '</button>';

            $recordButton = '<button id="record_' . $audio->id_audio . '" ';
            $recordButton .= 'class="btn-link record"';
            $recordButton .= 'title="' . __('page.audio.tabla.tooltip.accion.record') . '">';
            $recordButton .= '<span aria-hidden="true" '; 
            $recordButton .= 'class="icon-call-in font-blue-steel"></span>';
            $recordButton .= '</button>';

            $deleteButton = '<button id="delete_' . $audio->id_audio . '" ';
            $deleteButton .= 'class="btn-link delete" ';
            $deleteButton .= 'title="' . __('page.audio.tabla.tooltip.accion.delete') . '">';
            $deleteButton .= '<span aria-hidden="true" ';
            $deleteButton .= 'class="icon-close font-red-thunderbird"></span>';
            $deleteButton .= '</button>';

            $actionsBlock = '<div class="block-center">';
            $actionsBlock .= '<div class="btn-group">';
            $actionsBlock .= $downloadButton;
            $actionsBlock .= $playButton;
            $actionsBlock .= $uploadButton;
            $actionsBlock .= $recordButton;
            $actionsBlock .= $deleteButton;
            $actionsBlock .= '</div>';
            $actionsBlock .= '</div>';

            $audio->actions = $actionsBlock;
        }

        return response()->json(['aaData' => $audios]);
    }

    /**
     * Module entry point.
     *
     * @return void
     */
    public function index()
    {
        $this->data['breadcrumb'] = 'audio';

        return view('audio.show', $this->data);
    }

    /**
     * Store the new audio name and description
     *
     * @return void
     */
    public function store()
    {
        $this->validate(
            request(), 
            [
                'audio' => 'required',
                'descripcion' => 'required'
            ]
        );

        $audio = new Audio();
        $audio->audio = str_replace(' ', '_', request('audio'));
        $audio->descripcion = request('descripcion');
        $audio->activo = request('activo') ? true : false;

        try {
            $audio->save();
        } catch (\Illuminate\Database\QueryException $ex) {
            $error = new DBErrorDecoder($ex->getCode());

            return back()->withErrors(
                [__('page.audio.controller.notification'). $error->getMessage()], 
                409
            );
        }

        return redirect(asset('/audio'));
    }

    /**
     * Shows selected audio on edit form.
     *
     * @param Audio $audio Selected audio object
     * 
     * @return void
     */
    public function edit(Audio $audio)
    {
        $this->data['breadcrumb'] = 'audioedit';
        $this->data['audio_actual'] = $audio;

        return view('audio.edit', $this->data);
    }

    /**
     * Save changes to audio and description on audio;
     *
     * @param Audio $audio Selected audio object
     * 
     * @return void
     */
    public function update(Audio $audio)
    {
        $this->validate(
            request(), 
            [
                'audio' => 'required',
                'descripcion' => 'required'
            ]
        );

        $audio->audio = str_replace(' ', '_', request('audio'));
        $audio->descripcion = request('descripcion');
        $audio->activo = false;

        try {
            $audio->save();
        } catch (\Illuminate\Database\QueryException $ex) {
            $error = new DBErrorDecoder($ex->getCode());

            return back()->withErrors([__('page.audio.controller.notification') . $error->getMessage()], 409);
        }

        return redirect(asset('/audio'));
    }

    /**
     * Retrieves audio file from server.
     *
     * @param Audio $audio Selected audio object
     * 
     * @return void
     */
    public function download(Audio $audio)
    {
        $this->validate(
            request(), 
            [
                'audio_id' => 'request'
            ]
        );

        //Buscar Audio
        $file_location = '/opt/nasvox/nasvox/var/lib/asterisk/sounds/local/' . $audio->archivo;
        //$file_location = '/Volumes/Macintosh HD/Users/lvkz/Desktop/sample.mp3';

        //Getting Filesize Content:
        $filesize = filesize($file_location);

        //Devolver Audio
        return response(file_get_contents($file_location))
            ->header('Set-Cookie', 'fileDownload=true; path=/; HttpOnly=false')
            ->header('Cache-Control', 'max-age=60, must-revalidate')
            ->header('Content-Length', $filesize)
            ->header('Content-Type', 'application/force-download')
            ->header('Content-Type', 'application/download')
            ->header('Content-Disposition', 'attachment; filename="' . $audio->archivo . '"');
    }

    /**
     * Deletes current audio from DB and audio file
     *
     * @param Audio $audio Selected audio object
     * 
     * @return void
     */
    public function destroy(Audio $audio)
    {
        return redirect(asset('/audio'));
    }

    /**
     * Sends the desired recording to selected extension.
     * 
     * @param Audio $audio Selected audio object
     * @param string $extension Selected extension object
     * 
     * @return void
     */
    public function listen(Audio $audio, $extension)
    {
        AudioTmp::destroy($audio->id_audio);

        $audioTmp = new AudioTmp();
        $audioTmp->id_audio = $audio->id_audio;
        $audioTmp->ext = $extension;
        $audioTmp->escuchar = true;
        $audioTmp->save();
        
        $commando = '/opt/.nasvox/grabar_audio';
        $process = new Process('sudo '.$commando);
        $process->start();

        $process->wait();

        return response()->json([
            'notificacion' => __('page.audio.controller.notification').$process->getOutput(),
        ], 200);
    }

    /**
     * Uploads an audio file to the server and asign it to an audio.
     *
     * @return void
     */
    public function upload(Request $request) {
        $audio_id = request('audio_id');
        
        $file = request()->file('audioFile');
        $ext = $file->guessClientExtension() != 'mpga' ? $file->guessClientExtension() : 'mp3';

        $audio = Audio::find($audio_id);
        $audio->archivo = $audio->audio.'.'.$ext;
        $audio->save();

        Storage::disk('nasvox_audio')->putFileAs('', request()->file('audioFile'), $audio->audio.'.'.$ext);

        return back();
    }

    /**
     * Sends the signal to the extension to record a new message
     *
     * @param Audio $audio
     * @param string $extension
     * 
     * @return void
     */
    public function record(Audio $audio, $extension)
    {
        AudioTmp::destroy($audio->id_audio);

        $audioTmp = new AudioTmp();
        $audioTmp->id_audio = $audio->id_audio;
        $audioTmp->ext = $extension;
        $audioTmp->save();

        $audio->archivo = $audio->audio.'.gsm';
        $audio->save();

        $commando = '/opt/.nasvox/grabar_audio';
        $process = new Process('sudo '.$commando);
        $process->start();

        $process->wait();

        return response()->json([
            'notificacion' => __('page.audio.controller.notification').$process->getOutput(),
        ], 200);
    }

    /**
     * Removes the audio records from AudioTmp and Audio, deletes the audio file from the server.
     *
     * @param $audio_id
     * 
     * @return void
     */
    public function remove($audio_id)
    {
        $archivo = 'testFile.mp3';
        $sistema_ivr = SistemaIvr::where('id_audio', $audio_id)->first();

        if(is_null($sistema_ivr)) {
            //Proceed with removal.
            $audioTmp = AudioTmp::where('id_audio', $audio_id)->first();

            if(!is_null($audioTmp))
                AudioTmp::destroy($audioTmp->id_audio);
            
            $audio = Audio::where('id_audio', $audio_id)->first();

            if(!is_null($audio)) {
                $archivo = $audio->archivo;
                Audio::destroy($audio->id_audio);
            }
            
            $commando = 'rm /opt/nasvox/nasvox/var/lib/asterisk/sounds/local/'.$archivo;
            $process = new Process('sudo '.$commando);
            $process->start();

            $process->wait();

            return response()->json([
                __('page.audio.controller.notification') => __('page.audio.controller.notification.message.audio_deleted')
            ], 200);
        } else {
            // Let the user know that is in use.'
            return response()->json([__('js.audio_en_uso')], 409);
        }
    }
}