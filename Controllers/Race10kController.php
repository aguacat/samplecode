<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use App\Models\Data;
use App\Models\Race10k;
use App\Models\RaceGroup;
use App\Models\Reference;
use App\Utilities\Helpers;
use Illuminate\Http\Request;
use App\Models\LegacyRace10k;
use App\Utilities\AzulHashing;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Notification;
use App\Http\Controllers\PaymentInfoController;
use App\Notifications\RegistrationNotification;

class Race10kController extends Controller
{
    public $data;

    public function __construct()
    {

        $helper = new Helpers();
        $this->data['pageTitle'] = 'Inscripción 10K';
        $this->data['meses_ingles'] = $helper->meses_ingles;
        $this->data['meses_espanol'] = $helper->meses_espanol;
        $this->data['race_groups'] = RaceGroup::where('status', true)->orderBy('name', 'asc')->get();
        $this->data['race_price'] = strip_tags(Data::where('code_name', 'precio-carrera')->pluck('value')->first());
    }

    /**
     * Validation Function
     */
    public function fieldValidation(Request $request)
    {
        $customMessages = [
            'first_name.required' => 'El campo nombres es obligatorio',
            'last_name.required' => 'El campo apellidos es obligatorio',
            'gender.required' => 'El campo sexo es obligatorio',
            'cellphone.required' => 'El campo celular es obligatorio',
            'day.required' => 'El campo día es obligatorio',
            'month.required' => 'El campo mes es obligatorio',
            'year.required' => 'El campo año es obligatorio',
            'phone.required' => 'El campo teléfono es obligatorio',
            'email.required' => 'El campo email es obligatorio',
            'email.email' => 'Debe ser un formato de correo válido',
            'tshirt_size.required' => 'El campo t-shirt es obligatorio',
            'blood_type.required' => 'El campo Tipo de Sangre es obligatorio',
            'runner_id.required' => 'El campo Identificación es obligatorio',
            'city.required' => 'El campo ciudad es obligatorio',
            'address.required' => 'El campo dirección es obligatorio',
            'emergency_contact_first_name.required' => 'El campo nombres en caso de Emergencia es obligatorio',
            'emergency_contact_last_name.required' => 'El campo apellidos en caso de Emergencia es obligatorio',
            'emergency_contact_phone.required' => 'El campo teléfono en caso de Emergencia es obligatorio',
            'allergies.required' => 'El campo alergias es obligatorio',
            'medicaments.required' => 'El campo medicamentos es obligatorio',
            'has_participated_before.required' => 'El campo has participado anteriormente es obligatorio',
            'best_time.required' => 'El campo mejor tiempo es obligatorio',
            'reached_by.required' => 'El campo como te enteraste del evento es obligatorio',
            'group_name.required' => 'El campo grupo de corredores es obligatorio',
            'is_underage.required' => 'Tu fecha de nacimiento es obligatoria',
            'tutor_first_name.required' => 'El campo nombres de Tutor es obligatorio',
            'tutor_last_name.required' => 'El campo apellidos de Tutor es obligatorio',
            'tutor_email.required' => 'El campo correo de Tutor es obligatorio',
            'tutor_cellphone.required' => 'El campo celular de Tutor es obligatorio',
            'tutor_phone.required' => 'El campo teléfono de Tutor es obligatorio',
        ];
        
        //$this->validate($request, $rules, $customMessages);
        
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'phone' => Rule::requiredIf(Gate::denies('is-sales-point')),
            'cellphone' => 'required',
            'day' => 'required',
            'month' => 'required',
            'year' => 'required',
            'email' => 'required|email',
            'tshirt_size' => 'required',
            'blood_type' => 'required',
            'runner_id' => 'required',
            'city' => 'required',
            'address' => Rule::requiredIf(Gate::denies('is-sales-point')),
            'emergency_contact_first_name' => Rule::requiredIf(Gate::denies('is-sales-point')),
            'emergency_contact_last_name' => Rule::requiredIf(Gate::denies('is-sales-point')),
            'emergency_contact_phone' => Rule::requiredIf(Gate::denies('is-sales-point')),
            'allergies' => 'required',
            'medicaments' => 'required',
            'has_participated_before' => 'required',
            'best_time' => Rule::requiredIf(Gate::denies('is-sales-point')),
            'reached_by' => Rule::requiredIf(Gate::denies('is-sales-point')),
            'group_name' => 'required',
            'is_underage' => 'required',
            'tutor_first_name' => 'required_if:is_underage,true',
            'tutor_last_name' => 'required_if:is_underage,true',
            'tutor_email' => 'required_if:is_underage,true',
            'tutor_cellphone' => 'required_if:is_underage,true',
            'tutor_phone' => 'required_if:is_underage,true',
        ], $customMessages);

        return $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.race10k.show', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('backend.race10k.create', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = $this->fieldValidation($request);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }

        if(!request()->approval_number) {
            return Redirect::back()->withInput()->withErrors(['El campo forma de pago es obligatorio']);
        }

        if(!request()->payment_reference) {
            return Redirect::back()->withInput()->withErrors(['El campo referencia es obligatorio']);
        }

        $runner = $this->createRunner($request, null, 'admin');

        //Create Request!!
        $helper = new Helpers();
        //$innerRequest = new Request();
        //$innerRequest->setMethod('GET');
        //$innerRequest->headers->set('Content-Type', 'application/x-www-form-urlencode');
        //$request->headers->set('Authorization', );
        $request->request->add([
            'OrderNumber'       => $runner->registration_number,
            'Amount'            => $helper->getRacePrice(),
            'AuthorizationCode' => request()->approval_number . '__' . request()->payment_reference,
            'DateTime'          => Carbon::now()
        ]);

        $paymentInstance = new PaymentInfoController();
        $paymentInstance->raceParticipantApproved($request);

        return redirect(asset('/admin/race10k'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showDetail($id)
    {
        $runner = Race10k::find($id);
        return response()->json($runner);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        return view('backend.race10k.create', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function showFrontEnd()
    {
        if(Reference::where('name', 'registration_open')->first()->sequence == 'false') {
            return view('frontend.announcement', $this->data);
        }

        return view('frontend.race10k', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if(Gate::denies('can-edit-site')) {
            return redirect()->back();
        }

        $this->data['runner'] = Race10k::find($id);
        return view('backend.race10k.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->fieldValidation($request);
        $this->createRunner($request, $id, 'update');

        return redirect(asset('/admin/race10k'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    /**
     * Return list of runners.
     */
    public function list($raceYear)
    {
        $runners = Race10k::select(
            'id',
            DB::raw('CONCAT("<b>Ns.: </b> ",first_name, "<br><b>As.: </b>", last_name) AS names'),
            DB::raw('CONCAT("<b>C.:</b> ", cellphone, "<br><b>T.:</b> ", phone) AS contact_column_1'),
            DB::raw('CONCAT("<b>C.:</b> ", runner_id, "<br><b>M.:</b> ", email) AS contact_column_2'),
            DB::raw('CONCAT("<b>Reg.:</b> ", registration_number, "<br><b>Cod.:</b> ", approval_number) AS registration'),
            DB::raw('CONCAT("<b>Chip:</b> ", chip_number, "<br><b>Grupo:</b> ", group_name) AS runner_data'),
            // DB::raw('CONCAT("<div class=\"btn-group mx-auto\" role=\"group\"><a href=\"'. asset('/admin/race10k') .'/", id, "/edit\" class=\"btn btn-info btn-sm align-middle\"></a>","<a href=\"'. asset('/race10K') .'/", id, "/edit\" class=\"btn btn-success btn-sm align-middle\">Enviar Mensaje</a>", "<a href=\"'. asset('/race10K') .'/", id, "/edit\" class=\"btn btn-warning btn-sm align-middle\">Entregar Chip</a></div>") AS actions'))
            'registration_type')
            ->where('registration_type', 'PERM')
            ->where('registration_number', 'LIKE', $raceYear. '%')
            ->when(!Gate::allows('can-edit-site'), function ($query) {
                return $query->where('registered_by', Auth::user()->id);
            })
            ->orderBy('names')
            ->get();

        return response()->json(['aaData' => $runners]);
    }

    /**
     * Toggle Registration Status on Runner. 
     */
    public function toggleStatus($runnerId) {
        return response()->json('true');
    }

    /**
     * Runner Existing Data.
     */
    public function retrieveRunnerData($runnerId)
    {
        $runnerData = Race10k::select('first_name as nombres',
                'last_name as apellidos',
                'gender as genero',
                'cellphone as celular',
                'phone as telefono',
                'email as email',
                'tshirt_size as size_t_shirt',
                'blood_type as tipo_sangre',
                'runner_id as identificacion',
                'city as ciudad',
                'address as direccion',
                'emergency_contact_first_name as nombres_persona_emergencia',
                'emergency_contact_last_name as apellidos_persona_emergencia',
                'emergency_contact_phone as telefono_persona_emergencia',
                'allergies as alergias',
                'medicaments as medicamentos',
                'has_participated_before as historia_10k',
                'best_time as mejor_tiempo',
                'reached_by as alcance',
                'group_name as grupo_corredor',
                'tutor_first_name as nombre_tutor',
                'tutor_last_name as apellido_tutor',
                'tutor_email as correo_electronico_tutor',
                'tutor_cellphone as celular_tutor',
                'tutor_phone as telefono_tutor',
                'birthdate as fecha_nacimiento')
            ->whereRaw("REPLACE(runner_id, '-', '') = $runnerId")
            ->orderBy('id', 'DESC')
            ->first();

        
        $runnerData = LegacyRace10k::whereRaw("REPLACE(identificacion, '-', '') = $runnerId")->orderBy('id_participante', 'DESC')->first();
        
        if(!$runnerData) {
            return response()->json('0');
        }

        return response()->json($runnerData);
    }

    /**
     * Create Runner Function
     *
     * @param Reference $reference
     *
     * @return App\Models\Race10k
     */
    public function createRunner(Request $request, $id = null, $operationType = 'create')
    {
        if ($operationType == 'update') {
            $runner = Race10k::find($id);
        } else {
            $runner = new Race10k();
            $runner->registration_number = Reference::GetNextParticipantCode('codigo_anio_actual', 'conteo_carrera')->reference;
            $runner->registered_by = Auth::check() ? Auth::user()->id : '0';
        }

        $runner->first_name = request()->first_name;
        $runner->last_name = request()->last_name;
        $runner->gender = request()->gender;
        $runner->cellphone = request()->cellphone;
        $runner->birthdate = date('Y-m-d', mktime(0, 0, 0, request()->month, request()->day, request()->year));
        $runner->phone = request()->phone;
        $runner->email = request()->email;
        $runner->tshirt_size = request()->tshirt_size;
        $runner->blood_type = request()->blood_type;
        $runner->runner_id = request()->runner_id;
        $runner->city = request()->city;
        $runner->address = request()->address;
        $runner->emergency_contact_first_name = request()->emergency_contact_first_name;
        $runner->emergency_contact_last_name = request()->emergency_contact_last_name;
        $runner->emergency_contact_phone = request()->emergency_contact_phone;
        $runner->allergies = request()->allergies;
        $runner->medicaments = request()->medicaments;
        $runner->has_participated_before = (request()->has_participated_before === 'true');
        $runner->best_time = request()->best_time;
        $runner->reached_by = request()->reached_by;
        $runner->group_name = request()->group_name;
        $runner->chip_number = request()->chip_number;
        $runner->approval_number =  !is_null($runner->approval_number) ? $runner->approval_number : 'PENDIENTE';
        $runner->registration_type = !is_null($runner->registration_type) ? $runner->registration_type : 'TEMP';

        if(request()->is_underdage) {
            // Tutor Data
            $runner->tutor_first_name = request()->tutor_first_name;
            $runner->tutor_last_name = request()->tutor_last_name;
            $runner->tutor_email = request()->tutor_email;
            $runner->tutor_cellphone = request()->tutor_cellphone;
            $runner->tutor_phone = request()->tutor_phone;
        }

        $runner->is_underage = (int)request()->is_underdage;
        $runner->save();

        return $runner;
    }

    /**
     * Pre Save Function
     */
    public function presave(Request $request)
    {
        $this->fieldValidation($request);
        $this->data['runner'] = $this->createRunner($request, null, 'presave');

        return view('frontend.presaved', $this->data);
    }

    /**
     * Save and Pay Function
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function saveAndPay(Request $request)
    {
        $validator = $this->fieldValidation($request);

        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $runner = $this->createRunner($request);
        $amountToPay = 0.00;

        // if (Reference::where('name', 'fecha_descuento')->value('sequence') >= date('Ymd')) {
        //     $amountToPay = strip_tags(Data::where('code_name', 'precio-carrera')->value('value'));
        //     $amountToPay = $amountToPay - ($amountToPay * 0.10);
        // } else {
        //     $amountToPay = strip_tags(Data::where('code_name', 'precio-carrera')->value('value'));
        // }

        $helper = new Helpers();
        $amountToPay = $helper->getRacePrice();

        $dataAzul = [
            'MerchantId' => AzulHashing::$prod_MerchantId,
            'MerchantName' => AzulHashing::$MerchantName,
            'MerchantType' => AzulHashing::$MerchantType,
            'CurrencyCode' => AzulHashing::$CurrencyCode,
            'OrderNumber' => $runner->registration_number,
            'Amount' => $amountToPay,
            'itbis' => AzulHashing::$Itbis,
            'ApprovedUrl' => AzulHashing::$ApprovedUrl,
            'DeclinedUrl' => AzulHashing::$DeclinedUrl,
            'CancelUrl' => AzulHashing::$CancelUrl,
            'ResponsePostUrl' => AzulHashing::$ResponsePostUrl,
            'UseCustomField1' => AzulHashing::$UseCustomField1,
            'CustomField1Label' => AzulHashing::$CustomField1Label,
            'CustomField1Value' => AzulHashing::$CustomField1Value,
            'UseCustomField2' => AzulHashing::$UseCustomField2,
            'CustomField2Label' => AzulHashing::$CustomField2Label,
            'CustomField2Value' => AzulHashing::$CustomField2Value,
            'ShowTransactionResult' => '0',
            'AuthHash' => AzulHashing::generateAuthHashRequest($amountToPay, $runner->registration_number),
        ];

        $this->data['dataAzul'] = $dataAzul;

        return view('frontend.sendToAzul', $this->data);
    }
}
