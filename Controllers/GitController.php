<?php

namespace App\Http\Controllers;

use Symfony\Component\Process\Process;

class GitController extends Controller
{
    public function update()
    {
        //Ir hasta NASVOX
        $changeToProjectDirectory = new Process(['cd', '/var/www/html/nasvoxv4']);
        $changeToProjectDirectory->run();

        //Pull los cambios a NASVOX
        $pullChanges = new Process('sudo git pull');
        $pullChanges->run();

        while ($pullChanges->isRunning()) {
            // code...
        }

        //Ir hasta IONCube Location
        $changeToIonCubeDirectory = new Process(['cd', '~/Downloads/ioncube_encoder5_cerberus_10.0']);
        $changeToIonCubeDirectory->run();

        //Correr la encriptacion
        $encryptProject = new Process('sudo ioncube_lic.sh');
        $encryptProject->run();

        while ($encryptProject->isRunning()) {
            // waiting for process to finish
        }

        //Ir hasta nasvox_encoded
        $changeToEncodedProjectDirectory = new Process(['cd', '/var/www/html/nasvoxv4_encoded']);
        $changeToEncodedProjectDirectory->run();

        //Pull los cambios a NASVOX
        $pushChanges = new Process('sudo git push');
        $pushChanges->run();

        while ($pushChanges->isRunning()) {
        }

        return response()->json('Todo bien!');

        //$this->encrypt();
    }

    public function encrypt()
    {
        //Ir hasta IONCube Location
        $changeToIonCubeDirectory = new Process(['cd', '~/Downloads/ioncube_encoder5_cerberus_10.0']);
        $changeToIonCubeDirectory->run();

        //Correr la encriptacion
        $encryptProject = new Process('sudo ioncube_lic.sh');
        $encryptProject->run();

        while ($encryptProject->isRunning()) {
            // waiting for process to finish
        }

        $this->publish();
    }

    public function publish()
    {
        //Ir hasta nasvox_encoded
        $changeToEncodedProjectDirectory = new Process(['cd', '/var/www/html/nasvoxv4_encoded']);
        $changeToEncodedProjectDirectory->run();

        //Pull los cambios a NASVOX
        $pushChanges = new Process('sudo git push');
        $pushChanges->run();

        while ($pushChanges->isRunning()) {
        }

        return response()->json('Todo bien!');
    }
}
