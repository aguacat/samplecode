<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MgralSistemaModulo extends Model {

    public function scopeModulosDisponiblesUsuario($query, $id_usuario_registrador) {
        return $query->select('mgral_sistema_modulo.id_mgral_sistema_modulo', 'mgral_sistema_modulo.sistema_modulo', 'mgral_sistema_modulo.descripcion')
            ->join('mgral_sistema_acceso', 'mgral_sistema_acceso.id_mgral_sistema_modulo', 'mgral_sistema_modulo.id_mgral_sistema_modulo')
            ->join('mgral_sistema_acceso_usuario', 'mgral_sistema_acceso_usuario.id_mgral_sistema_acceso', 'mgral_sistema_acceso.id_mgral_sistema_acceso')
            ->where('mgral_sistema_acceso_usuario.id_usuario', $id_usuario_registrador)
            ->orderBy('mgral_sistema_modulo.id_mgral_sistema_modulo')
            ->distinct()
            ->get();
    }

    protected $table = 'mgral_sistema_modulo';
    protected $primaryKey = 'id_mgral_sistema_modulo';
    public $timestamps = false;
}