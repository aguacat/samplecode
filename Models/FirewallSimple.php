<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FirewallSimple extends Model {
    public function firewall_tipo() {
        return $this->belongsTo(FirewallTipo::class, 'id_firewall_tipo', 'id_firewall_tipo');
    }

    public function tipo() {
        return $this->belongsTo(Tipo::class, 'id_tipo', 'id_tipo');
    }

    protected $table = 'firewall_simple';
    protected $primaryKey = 'id_firewall_simple';
    public $timestamps = false;
}