<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionData extends Model {
    protected $table = 'configuracion_data';
    protected $primaryKey = 'id_configuracion_data';
    public $timestamps = false;
}