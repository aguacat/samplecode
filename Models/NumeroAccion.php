<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NumeroAccion extends Model {
    public function numero() {
        return $this->hasMany(Numero::class, 'numero_accion', 'numero_accion');
    }

    protected $table = 'numero_accion';
    protected $primaryKey = 'numero_accion';
    public $incrementing = false;
    public $timestamps = false;
}