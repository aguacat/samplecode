<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallReportSummaryExtCantLlamadasSalientes extends Model {
    public function call_report_summary() {
        return $this->belongsTo(CallReportSummary::class, 'referencia', 'referencia');
    }

    protected $table = 'call_report_summary_ext_cant_llamadas_salientes';
    public $timestamps = false;
}