<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccQueueMember extends Model {
    public function ccc_queue() {
        return $this->belongsTo(CccQueue::class, 'id_queue', 'id_queue');
    }

    public function ccc_tipo_miembro() {
        return $this->belongsTo(CccTipoMiembro::class, 'id_tipo_miembro', 'id_tipo_miembro');
    }

    protected $table = 'ccc_queue_member';
    protected $primaryKey = 'id_queue_member';
    public $timestamps = false;
}