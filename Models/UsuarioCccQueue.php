<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioCccQueue extends Model {
    public function usuario() {
        return $this->belongsTo(Usuario::class, 'id_usuario', 'id_usuario');
    }

    public function ccc_queue() {
        return $this->belongsTo(CccQueue::class, 'queue', 'queue');
    }
    
    protected $table = 'usuario_ccc_queue';
    protected $primaryKey = 'id_usuario_ccc_queue';
    public $timestamps = false;
}