<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Crontab extends Model {
    protected $table = 'crontab';
    protected $primaryKey = 'id_crontab';
    public $timestamps = false;
}