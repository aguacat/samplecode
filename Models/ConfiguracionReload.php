<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfiguracionReload extends Model {
    protected $table = 'configuracion_reload';
    protected $primaryKey = 'id_configuracion_reload';
    public $incrementing = false;
    public $timestamps = false;
}