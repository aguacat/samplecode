<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GrupoPage extends Model {
    protected $fillable = ['id_grupo_page', 'grupo_page'];
    protected $table = 'grupo_page';
    protected $primaryKey = 'id_grupo_page';
    public $incrementing = false;
    public $timestamps = false;
}