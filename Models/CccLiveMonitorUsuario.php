<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccLiveMonitorUsuario extends Model {
    public function usuario() {
        return $this->belongsTo(Usuario::Class, 'id_usuario', 'id_usuario');
    }

    protected $table = 'ccc_live_monitor_usuario';
    protected $primaryKey = 'id_ccc_live_monitor_usuario';
    public $timestamps = false;
}