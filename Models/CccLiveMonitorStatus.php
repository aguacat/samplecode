<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccLiveMonitorStatus extends Model {
    protected $table = 'ccc_live_monitor_status';
    protected $primaryKey = 'id_ccc_live_monitor_status';
    public $timestamps = false;
}