<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrLlamadasPerdidas extends Model {
    protected $table = 'cdr_llamadas_perdidas';
    protected $primaryKey = 'id_cdr_llamadas_perdidas';
    public $timestamps = false;
}