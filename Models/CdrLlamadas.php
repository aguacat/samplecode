<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrLlamadas extends Model {
    protected $table = 'cdr_llamadas';
    protected $primaryKey = 'id_cdr_llamadas';
    public $timestamps = false;
}