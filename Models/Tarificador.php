<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tarificador extends Model {
    protected $table = 'Tarificador';
    protected $primaryKey = 'id_tarificador';
    public $timestamps = false;
}