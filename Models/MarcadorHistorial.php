<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarcadorHistorial extends Model {
    protected $table = 'marcador_historial';
    protected $primaryKey = 'id_marcador_historial';
    public $timestamps = false;
}