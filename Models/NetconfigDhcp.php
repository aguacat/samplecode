<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NetconfigDhcp extends Model {
    public function netconfig() {
        return $this->belongsTo(Netconfig::class, 'id_netconfig', 'id_netconfig');
    }
    
    public function netconfig_dhcp_ip_reserv() {
        return $this->hasMany(NetconfigDhcpIpReserv::class, 'id_netconfig_dhcp', 'id_netconfig_dhcp');
    }

    protected $fillable = ['ntp', 'default_gw', 'dns', 'ip_inicio', 'ip_fin', 'tftp', 'activo', 'red', 'mascara', 'id_netconfig'];
    protected $table = 'netconfig_dhcp';
    protected $primaryKey = 'id_netconfig_dhcp';
    public $timestamps = false;
}