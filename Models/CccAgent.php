<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccAgent extends Model {


    protected $table = 'ccc_agent';
    protected $primaryKey = 'id_agent';
    public $timestamps = false;
}