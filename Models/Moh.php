<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Moh extends Model {
    public function moh_audio() {
        return $this->hasMany(MohAudio::class, 'id_moh', 'id_moh');
    }
    
    protected $table = 'moh';
    protected $primaryKey = 'id_moh';
    public $timestamps = false;
}