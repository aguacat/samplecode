<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarcadorIgnorar extends Model {
    protected $table = 'marcador_ignorar';
    protected $primaryKey = 'id_marcador_ignorar';
    public $timestamps = false;
}