<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarcadorMovimiento extends Model {
    public function marcador() {
        return $this->belongsTo(Marcador::class, 'id_marcador', 'id_marcador');
    }
    
    public function marcador_movimiento_tipo() {
        return $this->hasMany(MarcadorMovimientoTipo::class, 'id_marcador_movimiento_tipo', 'id_marcador_movimiento_tipo');
    }
    
    protected $table = 'marcador_movimiento';
    protected $primaryKey = 'id_marcador_movimiento';
    public $timestamps = false;
}