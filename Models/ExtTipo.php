<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtTipo extends Model {
    public function ext() {
        return $this->hasMany(Ext::class, 'id_ext_tipo', 'id_ext_tipo');
    }

    protected $table = 'ext_tipo';
    protected $primaryKey = 'id_ext_tipo';
    public $incrementing = false;
    public $timestamps = false;
}