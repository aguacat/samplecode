<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Canal extends Model {
    public function canal_protocolo() {
        return $this->belongsTo(CanalProtocolo::class, 'id_canal_protocolo', 'id_canal_protocolo');
    }
    
    public function ruta_interna() {
        return $this->hasMany(RutaInterna::class, 'id_canal', 'id_canal');
    }

    public function scopeLlenarCombo($query) {
        return $query->select('id_canal', 'descripcion', 'sufijo', 'id_canal_protocolo', DB::raw("CASE WHEN length(sufijo) > 0 THEN  id_canal_protocolo || '/' || canal || '/' || sufijo ELSE id_canal_protocolo || '/' || coalesce(canal,'') END AS canal"))
            ->from('canal')
            ->orderBy('id_canal')
            ->get();
    }

    protected $table = 'canal';
    protected $primaryKey = 'id_canal';
    public $timestamps = false;
}