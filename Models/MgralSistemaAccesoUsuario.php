<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MgralSistemaAccesoUsuario extends Model {
    public function mgral_sistema_acceso() {
        return $this->belongsTo(MgralSistemaAcceso::class, 'id_mgral_sistema_acceso', 'id_mgral_sistema_acceso');
    }

    public function usuario() {
        return $this->belongsTo(Usuario::class, 'id_usuario', 'id_usuario');
    }

    public function scopeModulosDisponiblesPorUsuario($query, $id_usuario) {
        return $query->select('mgral_sistema_acceso.sistema_acceso')
            ->where('mgral_sistema_acceso_usuario.id_usuario', $id_usuario)
            ->join('mgral_sistema_acceso', 'mgral_sistema_acceso.id_mgral_sistema_acceso', 'mgral_sistema_acceso_usuario.id_mgral_sistema_acceso')
            ->get();
    }

    protected $table = 'mgral_sistema_acceso_usuario';
    protected $primaryKey = 'id_mgral_sistema_acceso_usuario';
    public $timestamps = false;
}