<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GrabacionNuncagrabar extends Model {
    protected $table = 'grabacion_nuncagrabar';
    protected $primaryKey = 'id_grabacion_nuncagrabar';
    public $timestamps = false;
}