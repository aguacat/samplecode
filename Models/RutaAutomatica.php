<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RutaAutomatica extends Model {
    protected $table = 'ruta_automatica';
    protected $primaryKey = 'id_ruta_automatica';
    public $timestamps = false;
}