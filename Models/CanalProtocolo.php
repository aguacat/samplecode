<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CanalProtocolo extends Model {
    public function canal() {
        return $this->hasMany(Canal::class, 'id_canal_protocolo', 'id_canal_protocolo');
    }

    public function canal_protocolo_opcion() {
        return $this->hasMany(CanalProtocoloOpcion::class, 'id_canal_protocolo', 'id_canal_protocolo');
    }

    public function canal_protocolo_opcion_defecto() {
        return $this->hasMany(CanalProtocoloOpcionDefecto::class, 'id_canal_protocolo', 'id_canal_protocolo');
    }

    public function canal_protocolo_template() {
        return $this->hasMany(CanalProtocoloTemplate::class, 'id_canal_protocolo', 'id_canal_protocolo');
    }

    public function canal_protocolo_template_opcion() {
        return $this->hasMany(CanalProtocoloTemplateOpcion::class, 'id_canal_protocolo', 'id_canal_protocolo');
    }

    protected $table = 'canal_protocolo';
    protected $primaryKey = 'id_canal_protocolo';
    public $incrementing = false; //This is necessary because the primary key is string.
    public $timestamps = false;
}