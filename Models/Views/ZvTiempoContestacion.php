<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvTiempoContestacion extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_tiempo_contestacion';
    public $timestamps = false;
}