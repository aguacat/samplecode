<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvRecepcionAcceso extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_recepcion_acceso';
    public $timestamps = false;
}