<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvCdrDepartamentoFuente extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_cdr_departamento_fuente';
    public $timestamps = false;
}