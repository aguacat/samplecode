<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvLlamadasPerdidasDepartamentoSum extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_llamadas_perdidas_departamento_sum';
    public $timestamps = false;
}