<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvSistemaAccesoUsuario extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_sistema_acceso_usuario';
    public $timestamps = false;
}