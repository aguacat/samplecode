<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvFirewallSimple extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_firewall_simple';
    public $timestamps = false;
}