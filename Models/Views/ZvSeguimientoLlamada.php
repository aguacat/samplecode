<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvSeguimientoLlamada extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_seguimiento_llamada';
    public $timestamps = false;
}