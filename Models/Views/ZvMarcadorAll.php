<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvMarcadorAll extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_marcador_all';
    public $timestamps = false;
}