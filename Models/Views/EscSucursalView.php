<?php

namespace App\Models\Views;

use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class EscSucursalView extends Eloquent {
    use ReadOnlyTrait;

    protected $table = 'esc_producto_view';
    public $timestamps = false;
}