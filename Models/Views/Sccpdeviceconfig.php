<?php

namespace App\Models\Views;

use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class Sccpdeviceconfig extends Eloquent {
    use ReadOnlyTrait;

    protected $table = 'sccpdeviceconfig';
    public $timestamps = false;
}