<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvCccEventLog extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_ccc_event_log';
    public $timestamps = false;
}