<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvCdrDepartamentoDestino extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_cdr_departamento_destino';
    public $timestamps = false;
}