<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvCdrLlamadasPerdidas extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_cdr_llamadas_perdidas';
    public $timestamps = false;
}