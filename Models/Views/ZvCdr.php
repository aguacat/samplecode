<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvCdr extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_cdr';
    public $timestamps = false;
}