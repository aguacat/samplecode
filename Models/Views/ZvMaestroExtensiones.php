<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvMaestroExtensiones extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_maestro_extensiones';
    public $timestamps = false;
}