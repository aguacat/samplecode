<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvMarcadorPoolEstatistica extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_marcador_pool_estatistica';
    public $timestamps = false;
}