<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvParametros extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_parametros';
    public $timestamps = false;
}