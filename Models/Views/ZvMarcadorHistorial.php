<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvMarcadorHistorial extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_marcador_historial';
    public $timestamps = false;
}