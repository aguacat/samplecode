<?php

namespace App\Models\Views;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class Vlan extends Model {
    use ReadOnlyTrait;

    protected $table = 'vlan';
    public $timestamps = false;
}