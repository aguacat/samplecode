<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvTarificador extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_tarificador';
    public $timestamps = false;
}