<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvCdrResumido extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_cdr_resumido';
    public $timestamps = false;
}