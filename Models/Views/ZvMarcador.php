<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvMarcador extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_marcador';
    public $timestamps = false;
}