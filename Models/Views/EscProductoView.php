<?php

namespace App\Models\Views;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class EscProductoView extends Model {
    use ReadOnlyTrait;

    protected $table = 'esc_producto_view';
    public $timestamps = false;
}