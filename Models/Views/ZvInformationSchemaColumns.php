<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvInformationSchemaColumns extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_information_schema_columns';
    public $timestamps = false;
}