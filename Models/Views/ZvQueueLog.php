<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvQueueLog extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_queue_log';
    public $timestamps = false;
}