<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvCanal extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_canal';
    public $timestamps = false;
}