<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvTarificadorReporte extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_tarificador_reporte';
    public $timestamps = false;
}