<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use MichaelAChrisco\ReadOnly\ReadOnlyTrait;

class ZvSistemaIvr extends Model {
    use ReadOnlyTrait;

    protected $table = 'zv_sistema_ivr';
    public $timestamps = false;
}