<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrConcurrentCallInterval extends Model {
    protected $table = 'cdr_concurrent_call_interval';
    protected $primaryKey = 'id_cdr_concurrent_call_interval';
    public $timestamps = false;
}