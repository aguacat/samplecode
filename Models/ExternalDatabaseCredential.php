<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExternalDatabaseCredential extends Model {
    protected $table = 'external_database_credential';
    protected $primaryKey = 'id_external_database_credential';
    public $timestamps = false;
}