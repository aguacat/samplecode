<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MohAudio extends Model {
    public function moh() {
        return $this->belongsTo(Moh::class, 'id_moh', 'id_moh');
    }
    
    protected $table = 'moh_audio';
    protected $primaryKey = 'id_moh_audio';
    public $timestamps = false;
}