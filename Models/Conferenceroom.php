<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conferenceroom extends Model {
    protected $table = 'conferenceroom';
    protected $primaryKey = 'id_conferenceroom';
    public $timestamps = false;
}