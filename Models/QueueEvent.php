<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QueueEvent extends Model {
    protected $table = 'queue_event';
    protected $primaryKey = 'event';
    public $incrementing = false;
    public $timestamps = false;
}