<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccQueueLogWorking extends Model {
    protected $table = 'ccc_queue_log_working';
    public $timestamps = false;
}