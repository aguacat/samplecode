<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CanalProtocoloTemplateOpcion extends Model {
    public function canal_protocolo() {
        return $this->belongsTo(CanalProtocolo::class, 'id_canal_protocolo', 'id_canal_protocolo');
    }

    protected $fillable = ['orden', 'opcion', 'dato', 'id_canal_protocolo_template'];
    protected $table = 'canal_protocolo_template_opcion';
    protected $primaryKey = 'id_canal_protocolo_template_opcion';
    public $timestamps = false;
}