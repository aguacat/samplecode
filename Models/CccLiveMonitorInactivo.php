<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccLiveMonitorInactivo extends Model {
    protected $table = 'ccc_live_monitor_inactivo';
    protected $primaryKey = 'id_ccc_live_monitor_inactivo';
    public $timestamps = false;
}