<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccLiveMonitorAcd extends Model {
    protected $table = 'ccc_live_monitor_acd';
    protected $primaryKey = 'id_ccc_live_monitor_acd';
    public $timestamps = false;
}