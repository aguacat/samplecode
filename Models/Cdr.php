<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cdr extends Model {

    public function scopeDetalle_Llamada($query, $uniqueid) {
        //    SELECT fecha,
        //        CAST(calldate AS time) AS hora,
        //        clid AS callerid,
        //        src AS fuente,
        //        CASE WHEN cdr.hacia_afuera = 'True' THEN cdr.dst
        //            ELSE split_part(split_part(cdr.dstchannel, '-', 1), '/', 2)  END AS destino,
        //        '' AS provincia,
        //        duration AS duracion_segundos,
        //        uniqueid AS id,
        //        linkedid AS lid,
        //        CASE WHEN cdr.disposition = 'ANSWERED' THEN 'Contestado'
        //            WHEN cdr.disposition = 'NO ANSWER' THEN 'No Constestado'
        //            WHEN cdr.disposition = 'BUSY' THEN 'Ocupado'
        //            ELSE cdr.disposition END AS disposition
        //    FROM cdr
        //    WHERE linkedid = '1497958879.12514'
        //    ORDER BY calldate;

        return $query->select('fecha',
            DB::raw('CAST(calldate AS time) AS hora'),
            'clid AS callerid',
            'src AS fuente',
            DB::raw('CASE WHEN cdr.hacia_afuera = \'True\' THEN cdr.dst ELSE split_part(split_part(cdr.dstchannel, \'-\', 1), \'/\', 2)  END AS destino'),
            DB::raw('\'\' AS provincia'),
            'duration AS duracion_segundos',
            'uniqueid AS id',
            'linkedid AS lid',
            DB::raw('CASE WHEN cdr.disposition = \'ANSWERED\' THEN \'Contestado\' WHEN cdr.disposition = \'NO ANSWER\' THEN \'No Constestado\' WHEN cdr.disposition = \'BUSY\' THEN \'Ocupado\' ELSE cdr.disposition END AS disposition'))
            ->where('linkedid', $uniqueid)
            ->orderBy('calldate')
            ->get();
    }

    protected $table = 'cdr';
    public $timestamps = false;
}