<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model {
    public function reporte() {
        return $this->hasMany(Reporte::class, 'id_reporte', 'id_reporte');
    }
    
    public function reporte_usuario() {
        return $this->hasMany(ReporteUsuario::class, 'id_reporte', 'id_reporte');
    }
    
    protected $table = 'reporte';
    protected $primaryKey = 'id_reporte';
    public $timestamps = false;
}