<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecepcionAcceso extends Model {
    public function usuario() {
        return $this->belongsTo(Usuario::class, 'id_usuario', 'id_usuario');
    }
    
    protected $table = 'recepcion_acceso';
    protected $primaryKey = 'id_recepcion_acceso';
    public $timestamps = false;
}