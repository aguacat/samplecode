<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GsGatewayFxsPuerto extends Model {
    public function gs_gateway_fxs() {
        return $this->belongsTo(GsGatewayfxs::class, 'id_gs_gateway_fxs', 'id_gs_gateway_fxs');
    }

    protected $table = 'gs_gateway_fxs_puerto';
    protected $primaryKey = 'id_gs_gateway_fxs_puerto';
    public $timestamps = false;
}