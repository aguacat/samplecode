<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DispositivoModelo extends Model {
    public function ext() {
        return $this->hasMany(Ext::class, 'id_dispositivo_modelo', 'id_dispositivo_modelo');
    }

    protected $table = 'dispositivo_modelo';
    protected $primaryKey = 'id_dispositivo_modelo';
    public $incrementing = false;
    public $timestamps = false;
}