<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrConcurrentCall extends Model {
    protected $table = 'cdr_concurrent_call';
    protected $primaryKey = 'id_cdr_concurrent_call';
    public $timestamps = false;
}