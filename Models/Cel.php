<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cel extends Model {
    protected $table = 'cel';
    public $timestamps = false;
}