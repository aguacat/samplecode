<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccLiveMonitorLlamadasEnEspera extends Model {
    protected $table = 'ccc_live_monitor_llamadas_en_espera';
    protected $primaryKey = 'id_ccc_live_monitor_llamadas_en_espera';
    public $timestamps = false;
}