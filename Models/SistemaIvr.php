<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SistemaIvr extends Model {
    public function audio() {
        return $this->belongsTo(Audio::class, 'id_audio', 'id_audio');
    }
    
    public function sistema_ivr_detalle() {
        return $this->hasMany(SistemaIvrDetalle::class, 'id_sistema_ivr', 'id_sistema_ivr');
    }

    protected $fillable = ['sistema_ivr', 'descripcion', 'segundos_espera', 'accion_no_marcardo', 'accion_no_marcado_parametro', 'tiempo', 'integrar_extensiones', 'id_audio'];
    protected $table = 'sistema_ivr';
    protected $primaryKey = 'id_sistema_ivr';
    public $timestamps = false;
}