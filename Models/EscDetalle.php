<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EscDetalle extends Model {
    public function esc_grupo() {
        return $this->belongsTo(EscGrupo::class, 'id_esc_grupo', 'id_esc_grupo');
    }

    public function esc_producto() {
        return $this->belongsTo(EscProducto::class, 'id_esc_producto', 'id_esc_producto');
    }

    public function sucursal() {
        return $this->belongsTo(Sucursal::class, 'id_sucursal', 'id_sucursal');
    }

    protected $table = 'esc_detalle';
    protected $primaryKey = 'id_esc_detalle';
    public $timestamps = false;
}