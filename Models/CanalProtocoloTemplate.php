<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CanalProtocoloTemplate extends Model {
    public function canal_protocolo() {
        return $this->belongsTo(CanalProtocolo::class, 'id_canal_protocolo', 'id_canal_protocolo');
    }

    public function scopePlantillas($query, $id_canal_protocolo) {
        return $query->select('id_canal_protocolo_template', 'id_canal_protocolo', 'canal_protocolo_template')
            ->from('canal_protocolo_template')
            ->where('id_canal_protocolo', $id_canal_protocolo)
            ->orderBy('canal_protocolo_template')
            ->get();
    }

    protected $table = 'canal_protocolo_template';
    protected $primaryKey = 'id_canal_protocolo_template';
    public $timestamps = false;
}