<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtAcceso extends Model {
    public function ext() {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }

    protected $fillable = ['name', 'ext_destino', 'descripcion'];
    protected $table = 'ext_acceso';
    public $timestamps = false;
}