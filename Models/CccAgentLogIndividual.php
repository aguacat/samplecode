<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccAgentLogIndividual extends Model {
    protected $table = 'ccc_agent_log_individual';
    public $timestamps = false;
}