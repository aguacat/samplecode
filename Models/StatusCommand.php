<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusCommand extends Model {
    protected $table = 'status_command';
    protected $primaryKey = 'status_command';
    public $incrementing = false;
    public $timestamps = false;
}