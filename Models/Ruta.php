<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ruta extends Model {
    public function ruta_respaldo() {
        return $this->hasMany(RutaRespaldo::class, 'id_ruta', 'id_ruta');
    }

    public function scopeNoAsignadas($query, $id_politica) {
        return $query->whereNotIn('id_ruta', function ($query) use ($id_politica) {
            $query->select('id_ruta')
                ->from('politica_ruta')
                ->where('id_politica', '=', $id_politica);
        })->where('activo', '=', 'True')
            ->orderBy('ruta')
            ->get();
    }

    protected $fillable = ['ruta', 'canal', 'descripcion', 'cantidad_digitos_sustraer_inicio', 'cantidad_digitos_sustraer_final', 'callerid', 'costo_minuto', 'prefijo', 'cantidad_digitos_sustraer_para_callerid', 'callerid_activar', 'activo', 'requiere_contrasena', 'enviar_ringback_tone', 'abrir_canal_antes_marcado', 'ruta_automatica', 'sobreescribir_ruta_cid'];
    protected $table = 'ruta';
    protected $primaryKey = 'id_ruta';
    public $timestamps = false;
}