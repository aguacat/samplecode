<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QueueMember extends Model {
    protected $table = 'queue_member';
    protected $primaryKey = 'uniqueid';
    public $timestamps = false;
}