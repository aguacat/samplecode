<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrDetail extends Model {
    protected $table = 'cdr_detail';
    public $timestamps = false;
}