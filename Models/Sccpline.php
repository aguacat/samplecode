<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sccpline extends Model {
    public function ext() {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }
    
    protected $table = 'sccpline';
    protected $primaryKey = 'name';
    public $incrementing = false;
    public $timestamps = false;
}