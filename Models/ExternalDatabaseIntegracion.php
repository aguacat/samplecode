<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExternalDatabaseIntegracion extends Model {
    protected $table = 'external_database_integracion';
    protected $primaryKey = 'id_external_database_integracion';
    public $timestamps = false;
}