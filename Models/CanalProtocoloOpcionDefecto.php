<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CanalProtocoloOpcionDefecto extends Model {
    public function canal_protocolo() {
        return $this->belongsTo(CanalProtocolo::class, 'id_canal_protocolo', 'id_canal_protocolo');
    }

    protected $fillable = ['orden', 'opcion', 'dato', 'id_canal_protocolo'];
    protected $table = 'canal_protocolo_opcion_defecto';
    protected $primaryKey = 'id_canal_protocolo_opcion_defecto';
    public $timestamps = false;
}