<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtProtocolo extends Model {
    public function ext() {
        return $this->hasMany(Ext::class, 'id_ext_protocolo', 'id_ext_protocolo');
    }

    protected $table = 'ext_protocolo';
    protected $primaryKey = 'id_ext_protocolo';
    public $incrementing = false;
    public $timestamps = false;
}