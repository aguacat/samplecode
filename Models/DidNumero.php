<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DidNumero extends Model {
    public function sucursal() {
        return $this->belongsTo(Sucursal::class, 'id_sucursal', 'id_sucursal');
    }

    public function did() {
        return $this->hasMany(Did::class, 'did', 'did_numero');
    }

    public function numero_did() {
        return $this->hasMany(NumeroDid::class, 'did_numero', 'did_numero');
    }

    protected $fillable = ['did_numero', 'descripcion'];
    protected $table = 'did_numero';
    protected $primaryKey = 'id_did_numero';
    public $timestamps = false;
}