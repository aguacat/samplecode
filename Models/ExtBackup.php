<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtBackup extends Model {
    public function ext() {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }

    protected $fillable = ['name', 'ext_destino', 'descripcion'];
    protected $table = 'ext_backup';
    public $timestamps = false;
}