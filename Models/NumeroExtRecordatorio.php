<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NumeroExtRecordatorio extends Model {
    protected $table = 'numero_ext_recordatorio';
    protected $primaryKey = 'id_numero_ext_recordatorio';
    public $timestamps = false;
}