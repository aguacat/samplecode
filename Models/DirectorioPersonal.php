<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DirectorioPersonal extends Model {
    public function departamento() {
        return $this->belongsTo(Departamento::class, 'id_departamento', 'id_departamento');
    }

    protected $table = 'directorio_personal';
    protected $primaryKey = 'id_directorio_personal';
    public $timestamps = false;
}