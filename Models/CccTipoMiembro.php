<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccTipoMiembro extends Model {
    protected $table = 'ccc_tipo_miembro';
    protected $primaryKey = 'id_tipo_miembro';
    public $timestamps = false;
}