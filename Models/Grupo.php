<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model {
    public function grupo_ext() {
        return $this->hasMany(GrupoExt::class, 'id_grupo', 'id_grupo');
    }

    public function reporte_automatico_contacto() {
        return $this->hasMany(ReporteAutomaticoContacto::class, 'id_grupo', 'id_grupo');
    }
    
    public function reporte_grupo() {
        return $this->hasMany(ReporteGrupo::class, 'id_grupo', 'id_grupo');
    }

    protected $fillable = ['grupo', 'pickup_group', 'activo'];
    protected $table = 'grupo';
    protected $primaryKey = 'id_grupo';
    public $timestamps = false;
}