<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MgralSistemaAcceso extends Model {
    public function mgral_sistema_modulo() {
        return $this->belongsTo(MgralSistemaModulo::class, 'id_mgral_sistema_modulo', 'id_mgral_sistema_modulo');
    }
    
    public function mgral_mgral_sistema_acceso_usuario() {
        return $this->hasMany(MgralMgralSistemaAccesoUsuario::class, 'id_mgral_sistema_acceso', 'id_mgral_sistema_acceso');
    }

    public function scopeModulosNoAsignadosRoot($query, $id_usuario) {
        //SQLNO = "SELECT sa.id_mgral_sistema_acceso,
        //                  sm.descripcion AS sistema_modulo,
        //                  sa.sistema_acceso,
        //                  sa.descripcion
        // FROM mgral_sistema_acceso sa,
        //      mgral_sistema_modulo sm
        // WHERE sa.id_mgral_sistema_modulo = sm.id_mgral_sistema_modulo
        // AND sa.id_mgral_sistema_acceso NOT IN (SELECT id_mgral_sistema_acceso FROM mgral_sistema_acceso_usuario WHERE id_usuario = @id_usuario)
        // ORDER BY sa.id_mgral_sistema_acceso;"

        return $query->select('mgral_sistema_acceso.id_mgral_sistema_acceso', 'mgral_sistema_modulo.descripcion AS sistema_modulo', 'mgral_sistema_acceso.sistema_acceso', 'mgral_sistema_acceso.descripcion')
            ->join('mgral_sistema_modulo', 'mgral_sistema_acceso.id_mgral_sistema_modulo', 'mgral_sistema_modulo.id_mgral_sistema_modulo')
            ->whereNotIn('mgral_sistema_acceso.id_mgral_sistema_acceso', function ($query) use ($id_usuario) {
                $query->select('id_mgral_sistema_acceso')
                    ->from('mgral_sistema_acceso_usuario')
                    ->where('id_usuario', $id_usuario);
            })
            ->orderBy('mgral_sistema_acceso.id_mgral_sistema_acceso')
            ->get();
    }

    public function scopeModulosNoAsignadosRootAgrupadosPorModulo($query, $id_usuario, $id_sistema_modulo) {
        //SQLNO = "SELECT sa.id_mgral_sistema_acceso,
        //                  sm.descripcion AS sistema_modulo,
        //                  sa.sistema_acceso,
        //                  sa.descripcion
        // FROM mgral_sistema_acceso sa,
        //      mgral_sistema_modulo sm
        // WHERE sa.id_mgral_sistema_modulo = sm.id_mgral_sistema_modulo
        // AND sa.id_mgral_sistema_acceso NOT IN (SELECT id_mgral_sistema_acceso FROM mgral_sistema_acceso_usuario WHERE id_usuario = @id_usuario)
        // AND sa.id_mgral_sistema_modulo = @id_sistema_modulo
        // ORDER BY sa.id_mgral_sistema_acceso;"

        return $query->select('mgral_sistema_acceso.id_mgral_sistema_acceso', 'mgral_sistema_modulo.descripcion AS sistema_modulo', 'mgral_sistema_acceso.sistema_acceso', 'mgral_sistema_acceso.descripcion')
            ->join('mgral_sistema_modulo', 'mgral_sistema_acceso.id_mgral_sistema_modulo', 'mgral_sistema_modulo.id_mgral_sistema_modulo')
            ->where('mgral_sistema_acceso.id_mgral_sistema_modulo', $id_sistema_modulo)
            ->whereNotIn('mgral_sistema_acceso.id_mgral_sistema_acceso', function ($query) use ($id_usuario) {
                $query->select('id_mgral_sistema_acceso')
                    ->from('mgral_sistema_acceso_usuario')
                    ->where('id_usuario', $id_usuario);
            })
            ->orderBy('mgral_sistema_acceso.id_mgral_sistema_acceso')
            ->get();
    }

    public function scopeModulosAsignadosRoot($query, $id_usuario) {
        //SQLA = SELECT sau.id_mgral_sistema_acceso_usuario,
        //                  sau.acceso,
        //                  sm.descripcion AS sistema_modulo,
        //                  sa.sistema_acceso,
        //                  sa.descripcion
        // FROM mgral_sistema_acceso_usuario sau,
        //      mgral_sistema_acceso sa,
        //      mgral_sistema_modulo sm
        // WHERE sau.id_mgral_sistema_acceso = sa.id_mgral_sistema_acceso
        // AND sa.id_mgral_sistema_modulo = sm.id_mgral_sistema_modulo
        // AND sau.id_usuario = @id_usuario;

        return $query->select('mgral_sistema_acceso_usuario.id_mgral_sistema_acceso_usuario', 'mgral_sistema_acceso_usuario.id_mgral_sistema_acceso', 'mgral_sistema_acceso_usuario.acceso', 'mgral_sistema_modulo.descripcion AS sistema_modulo', 'mgral_sistema_acceso.sistema_acceso', 'mgral_sistema_acceso.descripcion')
            ->join('mgral_sistema_acceso_usuario', 'mgral_sistema_acceso_usuario.id_mgral_sistema_acceso', 'mgral_sistema_acceso.id_mgral_sistema_acceso')
            ->join('mgral_sistema_modulo', 'mgral_sistema_modulo.id_mgral_sistema_modulo', 'mgral_sistema_acceso.id_mgral_sistema_modulo')
            ->where('mgral_sistema_acceso_usuario.id_usuario', $id_usuario)
            ->get();
    }

    public function scopeModulosAsignadosRootAgrupadosPorModulo($query, $id_usuario, $id_sistema_modulo) {
        //SQLA = SELECT sau.id_mgral_sistema_acceso_usuario,
        //                  sau.acceso,
        //                  sm.descripcion AS sistema_modulo,
        //                  sa.sistema_acceso,
        //                  sa.descripcion
        // FROM mgral_sistema_acceso_usuario sau,
        //      mgral_sistema_acceso sa,
        //      mgral_sistema_modulo sm
        // WHERE sau.id_mgral_sistema_acceso = sa.id_mgral_sistema_acceso
        // AND sa.id_mgral_sistema_modulo = sm.id_mgral_sistema_modulo
        // AND sau.id_usuario = @id_usuario
        // AND sa.id_mgral_sistema_modulo = @id_sistema_modulo;

        return $query->select('mgral_sistema_acceso_usuario.id_mgral_sistema_acceso_usuario', 'mgral_sistema_acceso_usuario.id_mgral_sistema_acceso', 'mgral_sistema_acceso_usuario.acceso', 'mgral_sistema_modulo.descripcion AS sistema_modulo', 'mgral_sistema_acceso.sistema_acceso', 'mgral_sistema_acceso.descripcion')
            ->join('mgral_sistema_acceso_usuario', 'mgral_sistema_acceso_usuario.id_mgral_sistema_acceso', 'mgral_sistema_acceso.id_mgral_sistema_acceso')
            ->join('mgral_sistema_modulo', 'mgral_sistema_modulo.id_mgral_sistema_modulo', 'mgral_sistema_acceso.id_mgral_sistema_modulo')
            ->where('mgral_sistema_acceso_usuario.id_usuario', $id_usuario)
            ->where('mgral_sistema_acceso.id_mgral_sistema_modulo', $id_sistema_modulo)
            ->get();
    }

    public function scopeModulosNoAsignadosNoRoot($query, $id_usuario, $id_usuario_registrador) {
        //SQLNO = "SELECT sa.id_mgral_sistema_acceso,
        //                  sm.descripcion AS sistema_modulo,
        //                  sa.sistema_acceso,
        //                  sa.descripcion
        // FROM mgral_sistema_acceso sa,
        //      mgral_sistema_modulo sm
        // WHERE sa.id_mgral_sistema_modulo = sm.id_mgral_sistema_modulo
        // AND sa.id_mgral_sistema_acceso NOT IN (SELECT id_mgral_sistema_acceso FROM mgral_sistema_acceso_usuario WHERE id_usuario = @id_usuario)
        // AND sa.id_mgral_sistema_acceso IN (SELECT id_mgral_sistema_acceso FROM mgral_sistema_acceso_usuario WHERE id_usuario = @id_usuario_registrador)
        // ORDER BY sa.id_mgral_sistema_acceso;"

        return $query->select('mgral_sistema_acceso.id_mgral_sistema_acceso', 'mgral_sistema_modulo.descripcion AS sistema_modulo', 'mgral_sistema_acceso.sistema_acceso', 'mgral_sistema_acceso.descripcion')
            ->join('mgral_sistema_modulo', 'mgral_sistema_acceso.id_mgral_sistema_modulo', 'mgral_sistema_modulo.id_mgral_sistema_modulo')
            ->whereNotIn('mgral_sistema_acceso.id_mgral_sistema_acceso', function ($query) use ($id_usuario) {
                $query->select('id_mgral_sistema_acceso')
                    ->from('mgral_sistema_acceso_usuario')
                    ->where('id_usuario', $id_usuario);
            })
            ->whereIn('mgral_sistema_acceso.id_mgral_sistema_acceso', function ($query) use ($id_usuario_registrador) {
                $query->select('id_mgral_sistema_acceso')
                    ->from('mgral_sistema_acceso_usuario')
                    ->where('id_usuario', $id_usuario_registrador);
            })
            ->orderBy('mgral_sistema_acceso.id_mgral_sistema_acceso')
            ->get();
    }

    public function scopeModulosNoAsignadosNoRootAgrupadosPorModulo($query, $id_usuario, $id_usuario_registrador, $id_sistema_modulo) {
        //SQLNO = "SELECT sa.id_mgral_sistema_acceso,
        //                  sm.descripcion AS sistema_modulo,
        //                  sa.sistema_acceso,
        //                  sa.descripcion
        // FROM mgral_sistema_acceso sa,
        //      mgral_sistema_modulo sm
        // WHERE sa.id_mgral_sistema_modulo = sm.id_mgral_sistema_modulo
        // AND sa.id_mgral_sistema_acceso NOT IN (SELECT id_mgral_sistema_acceso FROM mgral_sistema_acceso_usuario WHERE id_usuario = @id_usuario)
        // AND sa.id_mgral_sistema_acceso IN (SELECT id_mgral_sistema_acceso FROM mgral_sistema_acceso_usuario WHERE id_usuario = @id_usuario_registrador)
        // AND sa.id_mgral_sistema_modulo = @id_sistema_modulo"
        // ORDER BY sa.id_mgral_sistema_acceso;"

        return $query->select('mgral_sistema_acceso.id_mgral_sistema_acceso', 'mgral_sistema_modulo.descripcion AS sistema_modulo', 'mgral_sistema_acceso.sistema_acceso', 'mgral_sistema_acceso.descripcion')
            ->join('mgral_sistema_modulo', 'mgral_sistema_acceso.id_mgral_sistema_modulo', 'mgral_sistema_modulo.id_mgral_sistema_modulo')
            ->whereNotIn('mgral_sistema_acceso.id_mgral_sistema_acceso', function ($query) use ($id_usuario) {
                $query->select('id_mgral_sistema_acceso')
                    ->from('mgral_sistema_acceso_usuario')
                    ->where('id_usuario', $id_usuario);
            })
            ->whereIn('mgral_sistema_acceso.id_mgral_sistema_acceso', function ($query) use ($id_usuario_registrador) {
                $query->select('id_mgral_sistema_acceso')
                    ->from('mgral_sistema_acceso_usuario')
                    ->where('id_usuario', $id_usuario_registrador);
            })
            ->where('mgral_sistema_acceso.id_mgral_sistema_modulo', $id_sistema_modulo)
            ->orderBy('mgral_sistema_acceso.id_mgral_sistema_acceso')
            ->get();
    }

    public function scopeModulosAsignadosNoRoot($query, $id_usuario, $id_usuario_registrador) {
        //SQLA = "SELECT sau.id_mgral_sistema_acceso_usuario,
        //                  sau.acceso,
        //                  sm.descripcion AS sistema_modulo,
        //                  sa.sistema_acceso,
        //                  sa.descripcion
        // FROM mgral_sistema_acceso_usuario sau,
        //      mgral_sistema_acceso sa,
        //      mgral_sistema_modulo sm
        // WHERE sau.id_mgral_sistema_acceso = sa.id_mgral_sistema_acceso
        // AND sa.id_mgral_sistema_modulo = sm.id_mgral_sistema_modulo
        // AND sau.id_usuario = @id_usuario
        // AND sa.id_mgral_sistema_acceso IN (SELECT id_mgral_sistema_acceso FROM mgral_sistema_acceso_usuario WHERE id_usuario = @id_usuario_registrador);"

        return $query->select('mgral_sistema_acceso_usuario.id_mgral_sistema_acceso_usuario', 'mgral_sistema_acceso_usuario.id_mgral_sistema_acceso', 'mgral_sistema_acceso_usuario.acceso', 'mgral_sistema_modulo.descripcion AS sistema_modulo', 'mgral_sistema_acceso.sistema_acceso', 'mgral_sistema_acceso.descripcion')
            ->join('mgral_sistema_acceso_usuario', 'mgral_sistema_acceso_usuario.id_mgral_sistema_acceso', 'mgral_sistema_acceso.id_mgral_sistema_acceso')
            ->join('mgral_sistema_modulo', 'mgral_sistema_modulo.id_mgral_sistema_modulo', 'mgral_sistema_acceso.id_mgral_sistema_modulo')
            ->where('mgral_sistema_acceso_usuario.id_usuario', $id_usuario)
            ->whereIn('mgral_sistema_acceso.id_mgral_sistema_acceso', function ($query) use ($id_usuario_registrador) {
                $query->select('id_mgral_sistema_acceso')
                    ->from('mgral_sistema_acceso_usuario')
                    ->where('id_usuario', $id_usuario_registrador);
            })
            ->get();
    }

    public function scopeModulosAsignadosNoRootAgrupadosPorModulo($query, $id_usuario, $id_usuario_registrador, $id_sistema_modulo) {
        //SQLA = "SELECT sau.id_mgral_sistema_acceso_usuario,
        //                  sau.acceso,
        //                  sm.descripcion AS sistema_modulo,
        //                  sa.sistema_acceso,
        //                  sa.descripcion
        // FROM mgral_sistema_acceso_usuario sau,
        //      mgral_sistema_acceso sa,
        //      mgral_sistema_modulo sm
        // WHERE sau.id_mgral_sistema_acceso = sa.id_mgral_sistema_acceso
        // AND sa.id_mgral_sistema_modulo = sm.id_mgral_sistema_modulo
        // AND sau.id_usuario = @id_usuario
        // AND sa.id_mgral_sistema_acceso IN (SELECT id_mgral_sistema_acceso FROM mgral_sistema_acceso_usuario WHERE id_usuario = @id_usuario_registrador)
        // AND sa.id_mgral_sistema_modulo = @id_sistema_modulo;"

        return $query->select('mgral_sistema_acceso_usuario.id_mgral_sistema_acceso_usuario', 'mgral_sistema_acceso_usuario.id_mgral_sistema_acceso', 'mgral_sistema_acceso_usuario.acceso', 'mgral_sistema_modulo.descripcion AS sistema_modulo', 'mgral_sistema_acceso.sistema_acceso', 'mgral_sistema_acceso.descripcion')
            ->join('mgral_sistema_acceso_usuario', 'mgral_sistema_acceso_usuario.id_mgral_sistema_acceso', 'mgral_sistema_acceso.id_mgral_sistema_acceso')
            ->join('mgral_sistema_modulo', 'mgral_sistema_modulo.id_mgral_sistema_modulo', 'mgral_sistema_acceso.id_mgral_sistema_modulo')
            ->where('mgral_sistema_acceso_usuario.id_usuario', $id_usuario)
            ->where('mgral_sistema_acceso.id_mgral_sistema_modulo', $id_sistema_modulo)
            ->whereIn('mgral_sistema_acceso.id_mgral_sistema_acceso', function ($query) use ($id_usuario_registrador) {
                $query->select('id_mgral_sistema_acceso')
                    ->from('mgral_sistema_acceso_usuario')
                    ->where('id_usuario', $id_usuario_registrador);
            })
            ->get();
    }
    
    protected $table = 'mgral_sistema_acceso';
    protected $primaryKey = 'id_mgral_sistema_acceso';
    public $timestamps = false;
}