<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DirectorioList extends Model {
    protected $table = 'directorio_list';
    protected $primaryKey = 'id_directorio_list';
    public $timestamps = false;
}