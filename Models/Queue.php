<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Queue extends Model {
    protected $table = 'queue';
    protected $primaryKey = 'id_queue';
    public $timestamps = false;
}