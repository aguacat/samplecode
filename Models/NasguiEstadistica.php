<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NasguiEstadistica extends Model {
    public function scopePrimaryKey($query, $name, $fecha) {
        return $query->where('name', $name)->where('fecha', $fecha);
    }

    protected $table = 'nasgui_estadistica';
    public $incrementing = false;
    public $timestamps = false;
}