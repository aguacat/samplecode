<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccReporteGeneralAgente extends Model {
    protected $table = 'ccc_reporte_general_agente';
    protected $primaryKey = 'id_ccc_reporte_general_agente';
    public $timestamps = false;
}