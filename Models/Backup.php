<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Backup extends Model {
    public function usuario() {
        return $this->belongsTo(Usuario::class, 'id_usuario', 'id_usuario');
    }

    protected $table = 'backup';
    protected $primaryKey = 'id_backup';
    public $timestamps = false;
}