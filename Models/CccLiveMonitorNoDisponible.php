<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccLiveMonitorNoDisponible extends Model {
    protected $table = 'ccc_live_monitor_no_disponible';
    protected $primaryKey = 'id_ccc_live_monitor_no_disponible';
    public $timestamps = false;
}