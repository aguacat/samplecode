<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarcadorHorarioDiasIgnorar extends Model {
    protected $table = 'marcador_horario_dias_ignorar';
    protected $primaryKey = 'id_marcador_horario_dias_ignorar';
    public $timestamps = false;
}