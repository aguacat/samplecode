<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IaxUsers extends Model {
    public function ext() {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }

    protected $table = 'iax_users';
    protected $primaryKey = 'name';
    public $incrementing = false;
    public $timestamps = false;
}