<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model {
    public function ext() {
        return $this->hasMany(Ext::class, 'id_language', 'id_language');
    }

    protected $table = 'language';
    protected $primaryKey = 'id_language';
    public $incrementing = false;
    public $timestamps = false;
}