<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buttonconfig extends Model {
    public function sccpdevice() {
        return $this->belongsTo(SccpDevice::class, 'sccpdevice', 'device');
    }

    protected $table = 'buttonconfig';
    public $timestamps = false;
}