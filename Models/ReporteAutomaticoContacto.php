<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReporteAutomaticoContacto extends Model {
    public function grupo() {
        return $this->belongsTo(Grupo::class, 'id_grupo', 'id_grupo');
    }
    
    public function reporte_automatico() {
        return $this->belongsTo(ReporteAutomatico::class, 'id_reporte_automatico', 'id_reporte_automatico');
    }
    
    protected $table = 'reporte_automatico_contacto';
    protected $primaryKey = 'id_reporte_automatico_contacto';
    public $timestamps = false;
}