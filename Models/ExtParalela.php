<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtParalela extends Model {
    public function ext() {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }

    protected $table = 'ext_paralela';
    protected $primaryKey = 'id';
    public $timestamps = false;
}