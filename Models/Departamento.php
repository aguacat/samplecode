<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model {
    public function sucursal() {
        return $this->belongsTo(Sucursal::class, 'id_sucursal', 'id_sucursal');
    }

    public function clave_acceso() {
        return $this->hasMany(ClaveAcceso::class, 'id_departamento', 'id_departamento');
    }

    public function departamento_acceso() {
        return $this->hasMany(DepartamentoAcceso::class, 'id_departamento', 'id_departamento');
    }

    public function directorio_personal() {
        return $this->hasMany(DirectorioPersonal::class, 'id_departamento', 'id_departamento');
    }

    public function ext() {
        return $this->hasMany(Ext::class, 'id_departamento', 'id_departamento');
    }

    protected $fillable = ['departamento', 'sucursal', 'activo'];
    protected $table = 'departamento';
    protected $primaryKey = 'id_departamento';
    public $timestamps = false;
}