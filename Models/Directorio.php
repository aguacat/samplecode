<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Directorio extends Model {
    protected $table = 'directorio';
    protected $primaryKey = 'name';
    public $incrementing = false;
    public $timestamps = false;
}