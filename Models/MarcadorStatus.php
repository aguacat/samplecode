<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarcadorStatus extends Model {
    protected $table = 'marcador_status';
    protected $primaryKey = 'id_marcador_status';
    public $timestamps = false;
}