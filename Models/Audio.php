<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Audio extends Model {
    public function audiotmp() {
        return $this->hasMany(AudioTmp::class, 'id_audio', 'id_audio');
    }

    public function marcador_mensaje_tipo() {
        return $this->hasMany(MarcadorMensajeTipo::class, 'id_audio', 'id_audio');
    }

    protected $table = 'audio';
    protected $primaryKey = 'id_audio';
    public $timestamps = false;
}