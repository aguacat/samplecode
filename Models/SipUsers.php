<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SipUsers extends Model {
    public function ext() {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }
    
    protected $table = 'sip_users';
    public $timestamps = false;
}