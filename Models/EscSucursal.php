<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EscSucursal extends Model {
    protected $table = 'esc_sucursal';
    protected $primaryKey = 'id_esc_sucursal';
    public $timestamps = false;
}