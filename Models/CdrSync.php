<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrSync extends Model {
    protected $table = 'cdr_sync';
    protected $primaryKey = 'id_cdr_sync';
    public $timestamps = false;
}