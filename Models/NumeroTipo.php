<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NumeroTipo extends Model {
    protected $table = 'numero_tipo';
    protected $primaryKey = 'id_numero_tipo';
    public $timestamps = false;
}