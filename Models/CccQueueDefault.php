<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccQueueDefault extends Model {
    protected $table = 'ccc_queue_default';
    protected $primaryKey = 'id_queue_default';
    public $timestamps = false;
}