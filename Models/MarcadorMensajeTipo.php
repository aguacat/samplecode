<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarcadorMensajeTipo extends Model {
    public function audio() {
        return $this->belongsTo(Audio::class, 'id_audio', 'id_audio');
    }
    
    public function marcador_simple() {
        return $this->hasMany(MarcadorSimple::class, 'id_marcador_mensaje_tipo', 'id_marcador_mensaje_tipo');
    }

    protected $table = 'marcador_mensaje_tipo';
    protected $primaryKey = 'id_marcador_mensaje_tipo';
    public $timestamps = false;
}