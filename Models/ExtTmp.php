<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtTmp extends Model {
    protected $table = 'ext_tmp';
    protected $primaryKey = 'id_ext';
    public $timestamps = false;
}