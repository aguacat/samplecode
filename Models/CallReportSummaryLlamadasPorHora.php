<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallReportSummaryLlamadasPorHora extends Model {
    public function call_report_summary() {
        return $this->belongsTo(CallReportSummary::class, 'referencia', 'referencia');
    }

    protected $table = 'call_report_summary_llamadas_por_hora';
    public $timestamps = false;
}