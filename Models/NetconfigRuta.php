<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NetconfigRuta extends Model {
    public function netconfig() {
        return $this->belongsTo(Netconfig::class, 'id_netconfig', 'id_netconfig');
    }
    protected $table = 'netconfig_ruta';
    protected $primaryKey = 'id_netconfig_ruta';
    public $timestamps = false;
}