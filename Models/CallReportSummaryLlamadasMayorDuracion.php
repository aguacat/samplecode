<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallReportSummaryLlamadasMayorDuracion extends Model {
    public function call_report_summary() {
        return $this->belongsTo(CallReportSummary::class, 'referencia', 'referencia');
    }

    protected $table = 'call_report_summary_llamadas_mayor_duracion';
    public $timestamps = false;
}