<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Grabacion extends Model {
    public function grabacion_syslog() {
        return $this->hasMany(GrabacionSyslog::class, 'id_grabacion', 'id_grabacion');
    }

    public function scopeGrabacionesPorFechaRoot($query, $dateFrom, $dateTo) {
        //    SELECT g.id_grabacion,
        //        g.origen,
        //        g.destino,
        //        DATE(g.tiempo) AS fecha,
        //        CAST(g.tiempo AS time) AS hora,
        //        g.archivo,
        //        g.uniqueid,
        //        split_part(split_part(c.dstchannel, '-', 1), '/', 2) AS extension,
        //        ROUND(CAST(c.duration AS numeric) / 60, 2) AS duracion_minutos,
        //        ROUND(CAST(c.billsec AS numeric ) / 60, 2) AS minutos_hablados
        //    FROM grabacion g, cdr c
        //    WHERE g.historico = 'False'
        //      AND g.uniqueid = c.uniqueid
        //      AND DATE(g.tiempo) BETWEEN '2017-06-20' AND '2017-06-20'
        //    ORDER BY g.id_grabacion;

        return $query->select('grabacion.id_grabacion',
            'grabacion.origen',
            'grabacion.destino',
            DB::raw('DATE(grabacion.tiempo) AS fecha'),
            DB::raw('CAST(grabacion.tiempo AS time) AS hora'),
            'grabacion.archivo',
            'grabacion.uniqueid',
            DB::raw('split_part(split_part(cdr.dstchannel, \'-\', 1), \'/\', 2) AS extension'),
            DB::raw('ROUND(CAST(cdr.duration AS numeric) / 60, 2) AS duracion_minutos'),
            DB::raw('ROUND(CAST(cdr.billsec AS numeric ) / 60, 2) AS minutos_hablados'))
            ->join('cdr', 'cdr.uniqueid', 'grabacion.uniqueid')
            ->where('grabacion.historico', 'False')
            ->whereBetween(DB::raw('DATE(grabacion.tiempo)'), [$dateFrom, $dateTo])
            ->orderBy('grabacion.id_grabacion')
            ->get();
    }

    public function scopeGrabacionesPorUniqueIdRoot($query, $uniqueId, $useLike = "=") {
        //    SELECT id_grabacion,
        //        origen,
        //        destino,
        //        DATE(tiempo) AS fecha,
        //        CAST(tiempo AS time) AS hora,
        //        archivo,
        //        uniqueid
        //    FROM grabacion
        //    WHERE uniqueid LIKE @uniqueId
        //    ORDER BY id_grabacion;

        return $query->select('grabacion.id_grabacion',
            'grabacion.origen',
            'grabacion.destino',
            DB::raw('DATE(grabacion.tiempo) AS fecha'),
            DB::raw('CAST(grabacion.tiempo AS time) AS hora'),
            'grabacion.archivo',
            'grabacion.uniqueid')
            ->where('uniqueid', $useLike, $uniqueId)
            ->orderBy('grabacion.id_grabacion')
            ->get();
    }

    public function scopeGrabacionesPorFechaNoRoot($query, $id_usuario, $dateFrom, $dateTo) {
        //    SELECT g.id_grabacion,
        //        g.origen,
        //        g.destino,
        //        g.fecha AS fecha,
        //        CAST(g.tiempo AS time) AS hora,
        //        g.archivo, g.uniqueid,
        //        split_part(split_part(c.dstchannel, '-', 1), '/', 2) AS extension,
        //        ROUND(c.duration / 60, 2) AS duracion_minutos,
        //        ROUND(c.billsec / 60, 2) AS minutos_hablados
        //    FROM grabacion g, cdr c
        //    WHERE g.historico = 'False'
        //            AND g.uniqueid = c.uniqueid
        //            AND g.fecha BETWEEN @fechadesde AND @fechahasta
        //            AND (g.origen IN (SELECT name FROM usuario_ext ue WHERE ue.id_usuario = @id_usuario AND acceso_grabacion = 'True' ) OR g.destino IN (SELECT name FROM usuario_ext ue WHERE ue.id_usuario = @id_usuario AND acceso_grabacion = 'True'))
        //    ORDER BY g.id_grabacion;

        return $query->select('grabacion.id_grabacion',
            'grabacion.origen',
            'grabacion.destino',
            DB::raw('DATE(grabacion.tiempo) AS fecha'),
            DB::raw('CAST(grabacion.tiempo AS time) AS hora'),
            'grabacion.archivo',
            'grabacion.uniqueid',
            DB::raw('split_part(split_part(cdr.dstchannel, \'-\', 1), \'/\', 2) AS extension'),
            DB::raw('ROUND(CAST(cdr.duration AS numeric) / 60, 2) AS duracion_minutos'),
            DB::raw('ROUND(CAST(cdr.billsec AS numeric ) / 60, 2) AS minutos_hablados'))
            ->join('cdr', 'cdr.uniqueid', 'grabacion.uniqueid')
            ->where('grabacion.historico', 'False')
            ->whereBetween(DB::raw('DATE(grabacion.tiempo)'), [$dateFrom, $dateTo])
            ->where(function ($query) use ($id_usuario) {
                $query->whereIn('grabacion.origen', function ($query) use ($id_usuario) {
                    $query->select('name')
                        ->from('usuario_ext')
                        ->where('usuario_ext.id_usuario', $id_usuario)
                        ->where('usuario_ext.acceso_grabacion', 'True');
                })
                    ->orWhereIn('grabacion.destino', function ($query) use ($id_usuario) {
                        $query->select('name')
                            ->from('usuario_ext')
                            ->where('usuario_ext.id_usuario', $id_usuario)
                            ->where('usuario_ext.acceso_grabacion', 'True');
                    });
            })
            ->orderBy('grabacion.id_grabacion')
            ->get();
    }

    public function scopeGrabacionesPorUniqueIdNoRoot($query, $id_usuario, $uniqueId, $useLike = "=") {
        //    SELECT id_grabacion,
        //        origen,
        //        destino,
        //        DATE(tiempo) AS fecha,
        //        CAST(tiempo AS time) AS hora,
        //        archivo,
        //        uniqueid
        //    FROM grabacion
        //    WHERE uniqueid LIKE @uniqueid
        //            AND (origen IN (SELECT ue.name FROM usuario_ext ue WHERE ue.id_usuario = @id_usuario AND acceso_grabacion = 'True')
        //       OR destino IN (SELECT ue.name FROM usuario_ext ue WHERE ue.id_usuario = @id_usuario AND acceso_grabacion = 'True'))
        //    ORDER BY id_grabacion

        return $query->select('grabacion.id_grabacion',
            'grabacion.origen',
            'grabacion.destino',
            DB::raw('DATE(grabacion.tiempo) AS fecha'),
            DB::raw('CAST(grabacion.tiempo AS time) AS hora'),
            'grabacion.archivo',
            'grabacion.uniqueid')
            ->where('uniqueid', $useLike, $uniqueId)
            ->where(function ($query) use ($id_usuario) {
                $query->whereIn('grabacion.origen', function ($query) use ($id_usuario) {
                    $query->select('name')
                        ->from('usuario_ext')
                        ->where('usuario_ext.id_usuario', $id_usuario)
                        ->where('usuario_ext.acceso_grabacion', 'True');
                })
                    ->orWhereIn('grabacion.destino', function ($query) use ($id_usuario) {
                        $query->select('name')
                            ->from('usuario_ext')
                            ->where('usuario_ext.id_usuario', $id_usuario)
                            ->where('usuario_ext.acceso_grabacion', 'True');
                    });
            })
            ->orderBy('grabacion.id_grabacion')
            ->get();
    }

    protected $table = 'grabacion';
    protected $primaryKey = 'id_grabacion';
    public $timestamps = false;
}