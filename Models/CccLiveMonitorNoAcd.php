<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccLiveMonitorNoAcd extends Model {
    protected $table = 'ccc_live_monitor_no_acd';
    protected $primaryKey = 'id_ccc_live_monitor_no_acd';
    public $timestamps = false;
}