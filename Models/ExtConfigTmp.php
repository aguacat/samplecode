<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtConfigTmp extends Model {
    protected $table = 'ext_config_tmp';
    protected $primaryKey = 'id_ext_config_tmp';
    public $timestamps = false;
}