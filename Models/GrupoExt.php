<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GrupoExt extends Model {
    public function ext() {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }
    
    public function grupo() {
        return $this->belongsTo(Grupo::class, 'id_grupo', 'id_grupo');
    }

    protected $table = 'grupo_ext';
    protected $primaryKey = 'id_grupo_ext';
    public $timestamps = false;
}