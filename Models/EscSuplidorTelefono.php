<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EscSuplidorTelefono extends Model {
    protected $table = 'esc_suplidor_telefono';
    protected $primaryKey = 'id_esc_suplidor_telefono';
    public $timestamps = false;
}