<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallReportSummaryExtMinHablados extends Model {
    public function call_report_summary() {
        return $this->belongsTo(CallReportSummary::class, 'referencia', 'referencia');
    }

    protected $table = 'call_report_summary_ext_min_hablados';
    public $timestamps = false;
}