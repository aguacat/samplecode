<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NasguiCdr extends Model {
    protected $table = 'nasgui_cdr';
    public $timestamps = false;
}