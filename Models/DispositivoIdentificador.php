<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DispositivoIdentificador extends Model {
    protected $table = 'dispositivo_identificador';
    protected $primaryKey = 'id_dispositivo_identificador';
    public $timestamps = false;
}