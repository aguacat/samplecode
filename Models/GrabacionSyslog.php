<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GrabacionSyslog extends Model {
    public function grabacion() {
        return $this->belongsTo(Grabacion::class, 'id_grabacion', 'id_grabacion');
    }

    public function usuario() {
        return $this->belongsTo(Usuario::class, 'id_usuario', 'id_usuario');
    }

    protected $fillable = ['id_grabacion', 'id_usuario', 'tipo'];
    protected $table = 'grabacion_syslog';
    protected $primaryKey = 'id_grabacion_syslog';
    public $timestamps = false;
}