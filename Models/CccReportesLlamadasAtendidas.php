<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccReportesLlamadasAtendidas extends Model {
    protected $table = 'ccc_reporte_llamadas_atendidas';
    protected $primaryKey = 'fecha';
    public $incrementing = false;
    public $timestamps = false;
}