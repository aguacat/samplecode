<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DidTipo extends Model {
    public function did() {
        return $this->hasMany(Did::class, 'tipo', 'id_did_tipo');
    }

    protected $table = 'did_tipo';
    protected $primaryKey = 'id_did_tipo';
    public $incrementing = false;
    public $timestamps = false;
}