<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Referencia extends Model {
    protected $table = 'referencia';
    protected $primaryKey = 'id_referencia';
    public $timestamps = false;
}