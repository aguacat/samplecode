<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Patch extends Model {
    protected $table = 'patch';
    protected $primaryKey = 'id_patch';
    public $timestamps = false;
}