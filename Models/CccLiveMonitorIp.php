<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccLiveMonitorIp extends Model {
    protected $table = 'ccc_live_monitor_ip';
    protected $primaryKey = 'id_ccc_live_monitor_ip';
    public $timestamps = false;
}