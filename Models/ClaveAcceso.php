<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClaveAcceso extends Model {
    public function departamento() {
        return $this->belongsTo(Departamento::class, 'id_departamento', 'id_departamento');
    }

    public function usuario() {
        return $this->belongsTo(Usuario::class, 'id_usuario', 'id_usuario');
    }

    protected $fillable = ['nombre', 'id_departamento', 'clave'];
    protected $table = 'clave_acceso';
    protected $primaryKey = 'id_clave_acceso';
    public $timestamps = false;
}