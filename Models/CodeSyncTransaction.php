<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CodeSyncTransaction extends Model {
    protected $table = 'code_sync_transaction';
    protected $primaryKey = 'id_code_sync_transaction';
    public $timestamps = false;
}