<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarcadorHorario extends Model {
    public function marcador_simple() {
        return $this->hasMany(MarcadorSimple::class, 'id_marcador_horario', 'id_marcador_horario');
    }

    protected $table = 'marcador_horario';
    protected $primaryKey = 'id_marcador_horario';
    public $timestamps = false;
}