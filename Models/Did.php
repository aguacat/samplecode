<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Did extends Model {
    public function did_numero() {
        return $this->belongsTo(DidNumero::class, 'did', 'did_numero');
    }

    public function did_tipo() {
        return $this->belongsTo(DidTipo::class, 'tipo', 'id_did_tipo');
    }

    public function scopeGetDid($query) {
        return $query->select('did_numero.id_did_numero', 'did_numero.did_numero', 'did_numero.descripcion', 'sucursal.sucursal')
            ->from('did_numero')
            ->leftJoin('sucursal', 'did_numero.id_sucursal', '=', 'sucursal.id_sucursal')
            ->orderBy('did_numero')
            ->get();
    }

    protected $fillable = ['did',
        'fwd',
        'tipo',
        'duracion_maxima_llamada',
        'callerid',
        'callerid_existente',
        'tocar_audio_unico',
        'f_fecha_inicio',
        'f_fecha_fin',
        'f_hora_inicio',
        'f_hora_fin',
        'f_prioridad',
        'f_dia',
        'grabar_llamadas',
        'nunca_grabar',
        'f_activar_fecha',
        'f_activar_hora'];
    protected $table = 'did';
    protected $primaryKey = 'id_did';
    public $timestamps = false;
}