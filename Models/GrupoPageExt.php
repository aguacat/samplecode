<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GrupoPageExt extends Model {
    public function ext() {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }
    
    public function grupo_page() {
        return $this->belongsTo(GrupoPage::class, 'id_grupo_page', 'id_grupo_page');
    }

    protected $table = 'grupo_page_ext';
    protected $primaryKey = 'id_grupo_page_ext';
    public $timestamps = false;
}