<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SistemaIvrDetalle extends Model {
    public function sistema_ivr() {
        return $this->belongsTo(SistemaIvr::class, 'id_sistema_ivr', 'id_sistema_ivr');
    }

    public function accion_detalle() {
        return $this->belongsTo(AccionNoMarcado::class, 'accion', 'accion_no_marcardo');
    }

    protected $table = 'sistema_ivr_detalle';
    protected $primaryKey = 'id_sistema_ivr_detalle';
    public $timestamps = false;
}