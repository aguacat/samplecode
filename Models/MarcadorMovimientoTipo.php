<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarcadorMovimientoTipo extends Model {
    public function marcador_movimiento() {
        return $this->hasMany(MarcadorMovimiento::class, 'id_marcador_movimiento_tipo', 'id_marcador_movimiento_tipo');
    }
    
    protected $table = 'marcador_movimiento_tipo';
    protected $primaryKey = 'id_marcador_movimiento_tipo';
    public $timestamps = false;
}