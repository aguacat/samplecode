<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marcador extends Model {
    public function marcador_movimiento() {
        return $this->hasMany(MarcadorMovimiento::class, 'id_marcador', 'id_marcador');
    }
    protected $table = 'marcador';
    protected $primaryKey = 'id_marcador';
    public $timestamps = false;
}