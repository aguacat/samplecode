<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtFwdChange extends Model {
    protected $table = 'ext_fwd_change';
    protected $primaryKey = 'id_ext_fwd_change';
    public $timestamps = false;
}