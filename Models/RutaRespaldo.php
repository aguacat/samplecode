<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RutaRespaldo extends Model {
    public function ruta() {
        return $this->belongsTo(Ruta::class, 'id_ruta', 'id_ruta');
    }

    protected $fillable = ['id_ruta', 'canal', 'prefijo', 'cantidad_digito_sustraer_inicio'];
    protected $table = 'ruta_respaldo';
    protected $primaryKey = 'id_ruta_respaldo';
    public $timestamps = false;
}