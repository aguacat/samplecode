<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NumeroDid extends Model {
    public function did_numero() {
        return $this->belongsTo(DidNumero::class, 'did_numero', 'did_numero');
    }

    public function numero() {
        return $this->belongsTo(Numero::class, 'numero', 'numero');
    }


    public function scopePrimaryKey($query, $numero, $did) {
        return $query->where('numero', $numero)->where('did', $did);
    }

    protected $fillable = ['numero', 'did'];
    protected $table = 'numero_did';
    public $incrementing = false;
    public $timestamps = false;
}