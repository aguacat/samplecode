<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstadoCentral extends Model
{
    protected $fillable = ['servicio', 'comando_verificacion', 'comando_encendido', 'comando_apagado', 'comando_apagado_forzado', 'comando_reinicio', 'estado', 'descripcion', 'activo', 'result', 'es_servicio', 'comando_fix'];
    protected $table = 'estado_central';
    protected $primaryKey = 'id_estado_central';
    public $timestamps = false;
}
