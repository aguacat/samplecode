<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtNumeroAsignado extends Model
{
    protected $table = 'ext_numeroasignado';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'ext_destino',
        'descripcion',
    ];

    public function ext()
    {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }
}
