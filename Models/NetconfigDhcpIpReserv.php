<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NetconfigDhcpIpReserv extends Model {
    public function netconfig_dhcp() {
        return $this->belongsTo(NetconfigDhcp::class, 'id_netconfig_dhcp', 'id_netconfig_dhcp');
    }


    protected $fillable = ['id', 'id_netconfig_dhcp', 'mac_address', 'ip', 'activo'];
    protected $table = 'netconfig_dhcp_ip_reserv';
    public $incrementing = false;
    public $timestamps = false;
}