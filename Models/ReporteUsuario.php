<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReporteUsuario extends Model {
    public function reporte() {
        return $this->belongsTo(Reporte::class, 'id_reporte', 'id_reporte');
    }

    public function usuario() {
        return $this->belongsTo(Usuario::class, 'id_usuario', 'id_usuario');
    }

    public function usuario_asignador() {
        return $this->belongsTo(Usuario::class, 'id_usuario_asignador', 'id_usuario');
    }

    protected $table = 'reporte_usuario';
    protected $primaryKey = 'id_reporte_usuario';
    public $timestamps = false;
}