<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChannelStatus extends Model {
    protected $table = 'channel_status';
    protected $primaryKey = 'canal';
    public $timestamps = false;
}