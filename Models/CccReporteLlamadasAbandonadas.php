<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccReporteLlamadasAbandonadas extends Model {
    protected $table = 'ccc_reporte_llamadas_abandonadas';
    protected $primaryKey = 'fecha';
    public $incrementing = false;
    public $timestamps = false;
}