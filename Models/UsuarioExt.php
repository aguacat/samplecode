<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioExt extends Model {
    public function usuario() {
        return $this->belongsTo(Usuario::class, 'id_usuario', 'id_usuario');
    }
    
    protected $table = 'usuario_ext';
    protected $primaryKey = 'id_usuario_ext';
    public $timestamps = false;
}