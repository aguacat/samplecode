<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccEventLog extends Model {
    protected $table = 'ccc_event_log';
    protected $primaryKey = 'ccc_event_log';
    public $incrementing = false;
    public $timestamps = false;
}