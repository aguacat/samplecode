<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RutaInterna extends Model {
    public function canal() {
        return $this->belongsTo(Canal::class, 'id_canal', 'id_canal');
    }

    protected $fillable = ['id_canal', 'ruta_interna', 'cantidad_digitos_sustraer_inicio', 'cantidad_digitos_sustraer_final', 'prefijo', 'enviar_ringback_tone'];
    protected $table = 'ruta_interna';
    protected $primaryKey = 'id_ruta_interna';
    public $timestamps = false;
}