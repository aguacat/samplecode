<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Syslog extends Model {
    public function usuario() {
        return $this->belongsTo(Usuario::class, 'id_usuario', 'id_usuario');
    }
    
    protected $table = 'syslog';
    protected $primaryKey = 'id_syslog';
    public $timestamps = false;
}