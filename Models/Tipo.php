<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model {
    public function firewall_simple() {
        return $this->hasMany(FirewallSimple::class, 'id_tipo', 'id_tipo');
    }

    protected $table = 'tipo';
    protected $primaryKey = 'id_tipo';
    public $timestamps = false;
}