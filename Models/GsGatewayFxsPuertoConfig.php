<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GsGatewayFxsPuertoConfig extends Model {
    protected $table = 'gs_gateway_fxs_puerto_config';
    protected $primaryKey = 'id_gs_gateway_fxs_puerto_config';
    public $timestamps = false;
}