<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Politica extends Model {
    public function politica_ruta() {
        return $this->hasMany(PoliticaRuta::class, 'id_politica', 'id_politica');
    }

    public function ext() {
        return $this->hasMany(Ext::class, 'id_politica', 'id_politica');
    }

    protected $fillable = ['politica'];
    protected $table = 'politica';
    protected $primaryKey = 'id_politica';
    public $timestamps = false;
}