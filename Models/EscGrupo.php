<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EscGrupo extends Model {
    public function esc_detalle() {
        return $this->hasMany(EscDetalle::class, 'id_esc_grupo', 'id_esc_grupo');
    }

    protected $table = 'esc_grupo';
    protected $primaryKey = 'id_esc_grupo';
    public $timestamps = false;
}