<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepartamentoAcceso extends Model {
    public function departamento() {
        return $this->belongsTo(Departamento::class, 'id_departamento', 'id_departamento');
    }

    protected $fillable = ['ext_destino', 'descripcion'];
    protected $table = 'departamento_acceso';
    public $timestamps = false;
}