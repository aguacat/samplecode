<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccReporteGeneral extends Model {
    protected $table = 'ccc_reporte_general';
    protected $primaryKey = 'id_ccc_reporte_general';
    public $timestamps = false;
}