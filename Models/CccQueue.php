<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccQueue extends Model {
    public function usuario_ccc_Queue() {
        return $this->hasMany(UsuarioCccQueue::class, 'queue', 'queue');
    }
    protected $table = 'ccc_queue';
    protected $primaryKey = 'id_queue';
    public $timestamps = false;
}