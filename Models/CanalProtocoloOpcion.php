<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CanalProtocoloOpcion extends Model {
    public function canal_protocolo() {
        return $this->belongsTo(CanalProtocolo::class, 'id_canal_protocolo', 'id_canal_protocolo');
    }

    protected $table = 'canal_protocolo_opcion';
    protected $primaryKey = 'id_canal_protocolo_opcion';
    public $timestamps = false;
}