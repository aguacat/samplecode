<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccReporteLlamadasAtendidasPorAgentes extends Model {
    protected $table = 'ccc_reporte_llamadas_atendidas_por_agentes';
    protected $primaryKey = 'agent';
    public $incrementing = false;
    public $timestamps = false;
}