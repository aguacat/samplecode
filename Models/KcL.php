<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KcL extends Model {
    protected $table = 'kc_l';
    protected $primaryKey = 'id_kc_l';
    public $timestamps = false;
}