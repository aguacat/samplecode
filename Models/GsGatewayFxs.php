<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GsGatewayFxs extends Model {
    protected $table = 'gs_gateway_fxs';
    protected $primaryKey = 'id_gs_gateway_fxs';
    public $timestamps = false;
}