<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccAgentDefault extends Model {
    protected $table = 'ccc_agent_default';
    protected $primaryKey = 'id_agent_default';
    public $timestamps = false;
}