<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarcadorPool extends Model {
    protected $table = 'marcador_pool';
    protected $primaryKey = 'id_marcador_pool';
    public $timestamps = false;
}