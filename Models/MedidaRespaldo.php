<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedidaRespaldo extends Model {
    public function medida_perdida() {
        return $this->hasMany(MedidaPerdida::class, 'medida_respaldo', 'medida_respaldo');
    }

    public function ext() {
        return $this->hasMany(Ext::class, 'medida_respaldo', 'medida_respaldo');
    }

    protected $table = 'medida_respaldo';
    protected $primaryKey = 'medida_respaldo';
    public $incrementing = false;
    public $timestamps = false;
}