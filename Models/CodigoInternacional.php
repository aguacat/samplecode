<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CodigoInternacional extends Model {
    protected $table = 'codigo_internacional';
    protected $primaryKey = 'id_codigo_internacional';
    public $timestamps = false;
}