<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoliticaRuta extends Model {
    public function politica() {
        return $this->belongsTo(Politica::class, 'id_politica', 'id_politica');
    }
    
    public function ruta() {
        return $this->belongsTo(Ruta::class, 'id_ruta', 'id_ruta');
    }

    protected $table = 'politica_ruta';
    protected $primaryKey = 'id_politica_ruta';
    public $timestamps = false;
}