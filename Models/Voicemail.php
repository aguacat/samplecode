<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voicemail extends Model {

    protected $fillable = ['mailbox', 'fullname', 'email', 'password', 'attach', 'saycid'];
    protected $table = 'voicemail';
    protected $primaryKey = 'uniqueid';
    public $timestamps = false;
}