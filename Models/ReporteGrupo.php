<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReporteGrupo extends Model {
    public function grupo() {
        return $this->belongsTo(Grupo::class, 'id_grupo', 'id_grupo');
    }
    
    public function reporte() {
        return $this->belongsTo(Reporte::class, 'id_reporte', 'id_reporte');
    }
    
    protected $table = 'reporte_grupo';
    protected $primaryKey = 'id_reporte_grupo';
    public $timestamps = false;
}