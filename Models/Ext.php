<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ext extends Model {
    public function ext_acceso() {
        return $this->hasMany(ExtAcceso::class, 'name', 'name');
    }

    public function ext_backup() {
        return $this->hasMany(ExtBackup::class, 'name', 'name');
    }

    public function ext_bloqueo() {
        return $this->hasMany(ExtBloqueo::class, 'name', 'name');
    }

    public function ext_monitoreo() {
        return $this->hasMany(ExtMonitoreo::class, 'name', 'name');
    }

    public function ext_paralela() {
        return $this->hasMany(ExtParalela::class, 'name', 'name');
    }

    public function ext_ruta() {
        return $this->hasMany(ExtRuta::class, 'name', 'name');
    }

    public function ext_whitelist() {
        return $this->hasMany(ExtWhitelist::class, 'name', 'name');
    }

    public function grupo_ext() {
        return $this->hasMany(GrupoExt::class, 'name', 'name');
    }

    public function iax_users() {
        return $this->hasMany(IaxUsers::class, 'name', 'name');
    }

    public function sccpline() {
        return $this->hasMany(Sccpline::class, 'name', 'name');
    }

    public function sip_users() {
        return $this->hasMany(SipUsers::class, 'name', 'name');
    }

    public function departamento() {
        return $this->belongsTo(Departamento::class, 'id_departamento', 'id_departamento');
    }

    public function dispositivo_modelo() {
        return $this->belongsTo(DispositivoModelo::class, 'id_dispositivo_modelo', 'id_dispositivo_modelo');
    }

    public function politica() {
        return $this->belongsTo(Politica::class, 'id_politica', 'id_politica');
    }

    public function ext_protocolo() {
        return $this->belongsTo(ExtProtocolo::Class, 'id_ext_protocolo', 'id_ext_protocolo');
    }

    public function ext_tipo() {
        return $this->belongsTo(ExtTipo::class, 'id_ext_tipo', 'id_ext_tipo');
    }

    public function language() {
        return $this->belongsTo(Language::class, 'id_language', 'id_language');
    }

    public function medida_respaldo() {
        return $this->$this->belongsTo(MedidaRespaldo::class, 'medida_respaldo', 'medida_respaldo');
    }

    public function scopeNoAsignadosGruposExt($query, $grupo) {
        return $query->whereNotIn('name', function ($query) use ($grupo) {
            $query->select('name')
                ->from('grupo_ext')
                ->where('id_grupo', '=', $grupo);
        })->where('name', '<>', '-')
            ->orderBy('name')
            ->get();
    }

    public function scopeNoAsignadosGruposPage($query, $grupo_page) {
        return $query->whereNotIn('name', function ($query) use ($grupo_page) {
            $query->select('name')
                ->from('grupo_page_ext')
                ->where('id_grupo_page', '=', $grupo_page);
        })->where('name', '<>', '-')
            ->orderBy('name')
            ->get();
    }

    public function scopeExtensionSipUsers($query) {
        return $query->select('ext.*', DB::raw('to_timestamp(sip_users.regseconds) AS fecha'), DB::raw("TO_CHAR(to_timestamp(sip_users.regseconds), 'HH12:MI:SS AM') AS hora"), 'sip_users.useragent')
            ->where('ext.name', '<>', '-')
            ->leftJoin('sip_users', 'sip_users.name', '=', 'ext.name')
            ->get();
    }

    public function scopeExtensionSipUsersAll($query) {
        //    SELECT ext.id_ext,
        //        ext.name,
        //        ext.display_name,
        //        departamento.departamento,
        //        politica.politica,
        //        ext.mac_address,
        //        ext_tipo.ext_tipo,
        //        dispositivo_modelo.dispositivo_modelo,
        //        ext.email,
        //        ext.nasvox_gui_ipaddress,
        //        ext.sistema_monitoreo_activado,
        //        ext.encryption,
        //        ext.transport,
        //        to_timestamp(sip_users.regseconds) AS fecha, 
        //        to_char(to_timestamp(sip_users.regseconds), 'HH12:MI:SS AM') AS hora, 
        //        sip_users.useragent
        //    FROM ext, sip_users, departamento, politica, ext_tipo, dispositivo_modelo
        //    WHERE ext.name <> '-'
        //    AND sip_users.name = ext.name
        //    AND departamento.id_departamento = ext.id_departamento
        //    AND politica.id_politica = ext.id_politica
        //    AND ext_tipo.id_ext_tipo = ext.id_ext_tipo
        //    AND dispositivo_modelo.id_dispositivo_modelo = ext.id_dispositivo_modelo

        return $query->select(
            'ext.id_ext',
            'ext.name',
            'ext.display_name',
            'departamento.departamento',
            'politica.politica',
            'ext.mac_address',
            'ext_tipo.ext_tipo',
            'dispositivo_modelo.dispositivo_modelo',
            'ext.email',
            'ext.nasvox_gui_ipaddress',
            'ext.sistema_monitoreo_activado',
            'ext.encryption',
            'ext.transport',
            DB::raw('to_timestamp(sip_users.regseconds) AS fecha'), 
            DB::raw("to_char(to_timestamp(sip_users.regseconds), 'HH12:MI:SS AM') AS hora"), 
            'sip_users.useragent')
            ->where('ext.name', '<>', '-')
            ->join('sip_users', 'sip_users.name', 'ext.name')
            ->join('departamento', 'departamento.id_departamento', 'ext.id_departamento')
            ->join('politica', 'politica.id_politica', 'ext.id_politica')
            ->join('ext_tipo', 'ext_tipo.id_ext_tipo', 'ext.id_ext_tipo')
            ->join('dispositivo_modelo', 'dispositivo_modelo.id_dispositivo_modelo', 'ext.id_dispositivo_modelo')
            ->get();
    }

    public function scopePermisosGrabacionLlamadas($query) {
        //    SELECT e.id_ext,
        //        e.name,
        //        e.display_name,
        //        d.departamento,
        //        su.useragent AS dispositivo_modelo,
        //        e.grabar_llamadas,
        //        e.nunca_grabar
        //    FROM ext e
        //    LEFT OUTER JOIN sip_users su ON e.name = su.name
        //    LEFT OUTER JOIN departamento d ON e.id_departamento = d.id_departamento
        //    WHERE e.name <> '-'
        //    ORDER BY e.name;

        return $query->select('ext.name', 'ext.display_name', 'departamento.departamento', 'sip_users.useragent AS dispositivo_modelo', 'ext.grabar_llamadas', 'ext.nunca_grabar')
            ->where('ext.name', '<>', '-')
            ->leftJoin('sip_users', 'sip_users.name', '=', 'ext.name')
            ->leftJoin('departamento', 'departamento.id_departamento', '=', 'ext.id_departamento')
            ->orderBy('ext.name')
            ->get();
    }

    protected $fillable = [
        'name', 
        'id_ext_tipo', 
        'display_name', 
        'operador', 
        'email', 
        'segundos_timbre', 
        'medida_respaldo_llamada', 
        'medida_respaldo_llamada_dato', 
        'medida_perdida_llamada', 
        'medida_perdida_llamada_dato', 
        'id_dispositivo_modelo', 
        'id_ext_protocolo', 
        'mac_address', 
        'id_departamento', 
        'id_politica', 
        'bloqueo_llamadas_fuera', 
        'permitir_fax', 
        'permitir_fwd', 
        'permitir_hacerpage', 
        'permitir_recibirpage', 
        'duracion_maxima_llamada_entrante', 
        'grabar_llamadas', 
        'puede_editar_display_name', 
        'puede_editar_email', 
        'puede_editar_pincode', 
        'language', 
        'duracion_maxima_llamada_saliente', 
        'fwd', 
        'nunca_grabar', 
        'vlan', 
        'permitir_levantarextension', 
        'permitir_levantamientoextension', 
        'fwd_numero', 
        'email_llamada_perdida', 
        'pin_global', 
        'contrasena', 
        'forward_automatico', 
        'forward_simultaneo', 
        'segundos_timbre_en_ocupado', 
        'mostrar_extension_en_directorio', 
        'allow', 
        'nat', 
        'transport', 
        'encryption', 
        'readonly', 
        'videosupport', 
        'puede_administrar_lista_blanca', 
        'puede_administrar_bloqueos', 
        'tarificador_activo', 
        'nasvox_gui_activo', 
        'nunca_pedir_contrasena_en_llamada', 
        'puede_modificar_directorio_departamental', 
        'permitir_modificar_email_llamada_perdida', 
        'puede_activar_desactivar_politica', 
        'activar_desactivar_politica_en_cola'
    ];
    
    protected $table = 'ext';
    protected $primaryKey = 'id_ext';
    public $timestamps = false;
}