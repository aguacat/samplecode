<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Features extends Model {
    protected $table = 'features';
    protected $primaryKey = 'feature';
    public $incrementing = false;
    public $timestamps = false;
}