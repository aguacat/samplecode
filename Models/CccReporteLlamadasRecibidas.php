<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccReporteLlamadasRecibidas extends Model {
    protected $table = 'ccc_reporte_llamadas_recibidas';
    protected $primaryKey = 'fecha';
    public $incrementing = false;
    public $timestamps = false;
}