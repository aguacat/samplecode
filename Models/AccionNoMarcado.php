<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccionNoMarcado extends Model {

    public function sistema_ivr_detalle() {
        return $this->hasMany(SistemaIvrDetalle::class, 'accion', 'accion_no_marcardo');
    }

    protected $table = 'accion_no_marcardo';
    protected $primaryKey = 'accion_no_marcardo';
    public $timestamps = false;
    public $incrementing = false;
}