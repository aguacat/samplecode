<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Netconfig extends Model {
    public function netconfig_dhcp() {
        return $this->hasMany(NetconfigDhcp::class, 'id_netconfig', 'id_netconfig');
    }

    public function netconfig_ruta() {
        return $this->hasMany(NetconfigRuta::class, 'id_netconfig', 'id_netconfig');
    }

    public function scopeGetInterfaces($query) {
        return $query->whereNotIn('id_netconfig', function ($query) {
            $query->select('id_netconfig')
                ->from('netconfig_dhcp');
        })->orderBy('interface')
            ->get();
    }

    public function scopeGetAllConfig($query) {
        return $query->select('netconfig_dhcp.*', DB::raw("CASE WHEN netconfig.vlanid > 0 THEN netconfig.interface || '.' || netconfig.vlanid ELSE netconfig.interface END AS id"))
            ->from('netconfig')
            ->join('netconfig_dhcp', 'netconfig.id_netconfig', '=', 'netconfig_dhcp.id_netconfig')
            ->get();
    }

    protected $table = 'netconfig';
    protected $primaryKey = 'id_netconfig';
    public $timestamps = false;
}