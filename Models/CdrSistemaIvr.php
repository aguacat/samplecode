<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CdrSistemaIvr extends Model {
    protected $table = 'cdr_sistema_ivr';
    protected $primaryKey = 'id_cdr_sistema_ivr';
    public $timestamps = false;
}