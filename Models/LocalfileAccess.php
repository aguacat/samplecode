<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocalfileAccess extends Model {
    protected $table = 'localfile_access';
    protected $primaryKey = 'id_localfile_access';
    public $timestamps = false;
}