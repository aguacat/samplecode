<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtRuta extends Model {
    public function ext() {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }

    public function ruta() {
        return $this->belongsTo(Ruta::class, 'id_ruta', 'id_ruta');
    }

    protected $fillable = ['name', 'id_ruta', 'canal', 'requiere_contrasena'];
    protected $table = 'ext_ruta';
    protected $primaryKey = 'id_ext_ruta';
    public $timestamps = false;
}