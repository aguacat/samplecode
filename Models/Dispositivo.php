<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dispositivo extends Model {
    public function dispositivo_modelo() {
        return $this->belongsTo(DispositivoModelo::class, 'id_dispositivo_modelo', 'id_dispositivo_modelo');
    }

    public function ext() {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }

    public function ext_protocolo() {
        return $this->belongsTo(ExtProtocolo::class, 'id_ext_protocolo', 'id_ext_protocolo');
    }

    protected $fillable = ['name', 'mac_address', 'id_dispositivo_modelo', 'id_ext_protocolo'];
    protected $table = 'dispositivo';
    protected $primaryKey = 'id_dispositivo';
    public $timestamps = false;
}