<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Numero extends Model {
    public function numero_accion() {
        return $this->belongsTo(NumeroAccion::class, 'numero_accion', 'numero_accion');
    }
    
    public function numero_did() {
        return $this->hasMany(NumeroDid::class, 'numero', 'numero');
    }
    
    protected $table = 'numero';
    protected $primaryKey = 'id_numero';
    public $timestamps = false;
}