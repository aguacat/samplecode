<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtWhitelist extends Model {
    public function ext() {
        return $this->belongsTo(Ext::class, 'name', 'name');
    }

    protected $fillable = ['name', 'ext_destino', 'descripcion'];
    protected $table = 'ext_whitelist';
    public $timestamps = false;
}