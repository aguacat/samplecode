<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallReportSummary extends Model {
    protected $table = 'call_report_summary';
    protected $primaryKey = 'referencia';
    public $incrementing = false; //This is necessary because the primary key is string.
    public $timestamps = false;
}