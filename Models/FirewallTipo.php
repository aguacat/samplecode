<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FirewallTipo extends Model {
    public function tipo() {
        return $this->belongsTo(Tipo::class, 'id_tipo', 'id_tipo');
    }

    protected $table = 'firewall_tipo';
    protected $primaryKey = 'id_firewall_tipo';
    public $incrementing = false;
    public $timestamps = false;
}