<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarcadorSimple extends Model {
    public function marcador_horario() {
        return $this->belongsTo(MarcadorHorario::class, 'id_marcador_horario', 'id_marcador_horario');
    }
    
    public function marcador_mensaje_Tipo() {
        return $this->belongsTo(MarcadorMensajeTipo::class, 'id_marcador_mensaje_tipo', 'id_marcador_mensaje_tipo');
    }
    protected $table = 'marcador_simple';
    protected $primaryKey = 'id_marcador_simple';
    public $timestamps = false;
}