<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReporteAutomatico extends Model {
    public function reporte_automatico_contacto() {
        return $this->hasMany(ReporteAutomaticoContacto::class, 'id_reporte_automatico', 'id_reporte_automatico');
    }

    protected $table = 'reporte_automatico';
    protected $primaryKey = 'id_reporte_automatico';
    public $timestamps = false;
}