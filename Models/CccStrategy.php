<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CccStrategy extends Model {
    protected $table = 'ccc_strategy';
    protected $primaryKey = 'strategy';
    public $incrementing = false;
    public $timestamps = false;
}