<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Musiconhold extends Model {
    protected $table = 'musiconhold';
    protected $primaryKey = 'name';
    public $incrementing = false;
    public $timestamps = false;
}