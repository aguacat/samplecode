<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sccpdevice extends Model {
    protected $table = 'sccpdevice';
    protected $primaryKey = 'name';
    public $incrementing = false;
    public $timestamps = false;
}