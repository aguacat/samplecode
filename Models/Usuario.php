<?php

namespace App\Models;

use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UsuarioPasswordResetNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable {

    use Notifiable;

    public function clave_acceso() {
        return $this->hasMany(ClaveAcceso::class, 'id_usuario', 'id_usuario');
    }
    
    public function mgral_sistema_acceso_usuario() {
        return $this->hasMany(MgralSistemaAccesoUsuario::class, 'id_usuario', 'id_usuario');
    }

    public function recepcion_usuario() {
        return $this->hasMany(RecepcionUsuario::class, 'id_usuario', 'id_usuario');
    }

    public function reporte_usuario() {
        return $this->hasMany(ReporteUsuario::class, 'id_usuario', 'id_usuario');
    }

    public function reporte_usuario_asignador() {
        return $this->hasMany(ReporteUsuario::class, 'id_usuario_asignador', 'id_usuario');
    }
    
    public function syslog() {
        return $this->hasMany(Syslog::class, 'id_usuario', 'id_usuario');
    }

    public function usuario_ccc_queue() {
        return $this->hasMany(UsuarioCccQueue::class, 'id_usuario', 'id_usuario');
    }
    
    public function usuario_ext() {
        return $this->hasMany(UsuarioExt::class, 'id_usuario', 'id_usuario');
    }

    public static $rules = array(
        'password' => 'required',
        'email'      => 'required'
    );

    public function username() {
        return $this->usuario;
    }

    public function getAuthIdentifier() {
        return $this->getKey();
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public function getReminderEmail() {
        return $this->email;
    }

    public static function validate($data) {
        return Validator::make($data, static::$rules);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token) {
        //$this->notify(new PasswordReset($token));
        $this->notify(new UsuarioPasswordResetNotification($token));
    }

    public function changePassword($currentPassword, $newPassword) {
        if (Hash::check($currentPassword, $this->password)) {
            $this->password = Hash::make($newPassword);
            $this->save();

            return ["result" => "ok"];
        } else {
            return ["error" => "Old password does not match our records."];
        }
    }

    protected $fillable = ['usuario', 'contrasena', 'activo', 'nota', 'email', 'password'];
    protected $hidden = ['contrasena', 'remember_token'];
    protected $table = 'usuario';
    protected $primaryKey = 'id_usuario';
    public $timestamps = false;
}