<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model {
    public function departamento() {
        return $this->hasMany(Departamento::class, 'id_sucursal', 'id_sucursal');
    }

    protected $fillable = ['sucursal', 'activo'];
    protected $table = 'sucursal';
    protected $primaryKey = 'id_sucursal';
    public $timestamps = false;
}
