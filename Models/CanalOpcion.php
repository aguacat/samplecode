<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CanalOpcion extends Model {
    public function canal() {
        return $this->belongsTo(Canal::class, 'id_canal', 'id_canal');
    }

    protected $table = 'canal_opcion';
    protected $primaryKey = 'id_canal_opcion';
    public $timestamps = false;
}