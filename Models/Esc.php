<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Esc extends Model {
    protected $table = 'esc';
    protected $primaryKey = 'id_esc';
    public $timestamps = false;
}