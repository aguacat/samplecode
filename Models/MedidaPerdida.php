<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedidaPerdida extends Model {
    public function medida_respaldo() {
        return $this->belongsTo(MedidaRespaldo::class, 'medida_respaldo', 'medida_respaldo');
    }
    
    protected $table = 'medida_perdida';
    protected $primaryKey = 'medida_perdida';
    public $incrementing = false;
    public $timestamps = false;
}