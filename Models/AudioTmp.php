<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AudioTmp extends Model {
    public function audio() {
        return $this->belongsTo(Audio::class, 'id_audio', 'id_audio');
    }

    //PENDING
    public function ext() {
        return $this->belongsTo(Ext::class, '', '');
    }

    public function scopePrimaryKey($query, $id_audio, $ext) {
        return $query->where('id_audio', $id_audio)->where('ext', $ext);
    }

    protected $table = 'audio_tmp';
    protected $primaryKey = 'id_audio';
    public $timestamps = false;
}