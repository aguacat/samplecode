<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EscProducto extends Model {
    public function esc_detalle() {
        return $this->hasMany(EscDetalle::class, 'id_esc_producto', 'id_esc_producto');
    }

    protected $table = 'esc_producto';
    protected $primaryKey = 'id_esc_producto';
    public $timestamps = false;
}