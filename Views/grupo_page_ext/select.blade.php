@extends('layouts.master')

@section('pagelevelcss')
    <!-- BEGIN SELECT2 -->
    <link href="{{ asset('/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <!-- END SELECT2 -->
    <link href="{{ asset('/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('layouts.pageheader')

        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row containerDiv">
                <div class="col-md-4">
                    <div class="portlet portlet-sortable light bordered">
                        <div class="portlet-title ui-sortable-handle">
                            <div class="caption font-blue-madison">
                                <i class="icon-wrench font-blue-madison"></i>
                                <span class="caption-subject bold uppercase">{{ __('page.bloque.izquierdo.titulo') }}</span>
                                <span class="caption-helper">
                                    {{ __('page.assign_grupo_page_ext.bloque.izquierdo.subtitulo') }}
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form method="post" action="#">

                                {{ csrf_field() }}

                                @include('layouts.errors')

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label">
                                            {{ __('page.assign_grupo_page_ext.campo.grupo_page') }}
                                        </label>
                                        <select class="form-control select2" id="grupo_page" name="grupo_page">
                                            <option value="" disabled selected>
                                                {{ __('assign_grupo_page_ext.placeholder.grupo_page') }}
                                            </option>
                                            @foreach($grupos_page as $grupo_page)
                                                <option value="{{ $grupo_page->id_grupo_page }}">{{ $grupo_page->grupo_page }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group hidden">
                                        <button type="button" class="btn red" disabled>
                                            {{ __('page.boton.desasignar_todo') }}
                                        </button>
                                        <button type="button" class="btn blue pull-right" disabled>
                                            {{ __('page.boton.asignar_todo') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="portlet portlet-sortable light bordered">
                        <div class="portlet-title ui-sortable-handle">
                            <div class="caption font-blue-madison">
                                <i class="icon-list font-blue-madison"></i>
                                <span class="caption-subject bold uppercase">{{ __('page.bloque.no_asignados') }}</span>
                                <span class="caption-helper">
                                    {{ __('page.assign_grupo_page_ext.bloque.no_asignados.subtitulo') }}
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <div id="no_asignar" class="list-group">
                                <div class="text-center non-draggable"><b>{{ __('page.seccion.arrastre_aqui') }}</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="portlet portlet-sortable light bordered">
                        <div class="portlet-title ui-sortable-handle">
                            <div class="caption font-blue-madison">
                                <i class="icon-list font-blue-madison"></i>
                                <span class="caption-subject bold uppercase">{{ __('page.bloque.asignados') }}</span>
                                <span class="caption-helper">
                                    {{ __('page.assign_grupo_page_ext.bloque.asignados.subtitulo') }}
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <div id="asignar" class="list-group">
                                <div class="text-center non-draggable"><b>{{ __('page.seccion.arrastre_aqui') }}</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->

    </div>
    <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('pagelevelplugins')
    <script src="{{ asset('/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
            type="text/javascript"></script>
@endsection

@section('pagelevelscripts')
    <script src="{{ asset('/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('pages/scripts/table-datatables-editable.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('pages/scripts/dragula.min.js') }}" type="text/javascript"></script>
@endsection

@section('customjs')
    <script>
        $('#grupo_page').on('change', function () {
            location.assign("{{ asset('assign_grupo_page_ext') }}/" + $(this).val() + '/edit');
        });

        $(document).ready(function () {
            $('.select2').select2({
                'placeholder': '{{ __('page.assign_grupo_page_ext.placeholder.grupo_page') }}',
            })
        });
    </script>
@endsection
