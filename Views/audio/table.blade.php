<div class="portlet portlet-sortable light bordered">
        <div class="portlet-title ui-sortable-handle">
            <div class="caption font-blue-madison">
                <i class="icon-list font-blue-madison"></i>
                <span class="caption-subject bold uppercase">
                    {{ __('page.bloque.derecho.titulo') }}
                </span>
                <span class="caption-helper">
                {{ __('page.audio.tabla.subtitulo') }}
                </span>
            </div>
            <div>
                <div class="col-md-4 pull-right">
                    <div class="input-group">
                        <input id="audioSearchBox" type="text" class="form-control" placeholder="{{ __('page.audio.tabla.placeholder.buscar_audio') }}" aria-describedby="sizing-addon1">
                        <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search" style="color: #2C3E50 !important"></i></span>
                    </div>
                </div>
                <div class="col-md-4 pull-right">
                    <input id="numeroExtension" type="text" class="form-control" placeholder="{{ __('page.audio.tabla.placeholder.extension') }}" aria-describedby="sizing-addon1">
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-hover table-light" id="datatable">
                <thead>
                <tr class="uppercase">
                    <th class="text-center">
                    {{ __('page.audio.tabla.campo.audio') }}
                    </th>
                    <th class="text-center">
                    {{ __('page.audio.tabla.campo.descripcion') }}
                    </th>
                    <th class="text-center">
                    {{ __('page.audio.tabla.campo.acciones') }}
                    </th>
                    <th class="text-center">
                        {{ 'Acciones' }}
                    </th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    
    @push('table_scripts')
        <script src="{{ asset('/js/page.jumpToData().js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/jquery.fileDownload.js') }}" type="text/javascript"></script>

        <script>
            var table;
            var item_actual = "{{ !empty($audio_actual) ? $audio_actual->id_audio : '0' }}";
            var audio = "{{ !empty($audio_actual) ? $audio_actual->audio : '0' }}";
    
            function gotoPage() {
                if (item_actual != 0) {
                    table.page.jumpToData(audio, 0);
                }
            }

            function isLoading(button) {
                button.find('span').removeClass('font-green-jungle');
                button.find('span').removeClass('icon-link');
                button.find('span').removeClass('icon-arrow-down');
                button.find('span').addClass('font-yellow-gold');
                button.find('span').html('<i class="fa fa-spinner fa-spin"></i>');
            }

            function resetDownloadButton() {
                var button = $('.current_download');
                button.find('span>i').removeClass('fa fa-spinner fa-spin');
                button.find('span').removeClass('font-yellow-gold');
                button.find('span').addClass('font-green-jungle');
                button.find('span').addClass('icon-arrow-down');
                button.removeClass('current_download');
            }

            $('#datatable').on('click', '.download', function(e){
                var audio_id = $(this).attr('id').replace('download_', '');

                e.preventDefault();
                $(this).addClass('current_download')
                isLoading($(this));

                let downloadPromise = new Promise((resolve, reject) => {
                    $.fileDownload('{{ asset('audio/download') }}' + '/' + audio_id, {
                        httpMethod: 'GET',
                        successCallback: function(url) {
                            alert('Jevi!')
                        },
                        failCallback: function(responseHtml, url) {
                            alert('{{ __('alerts.js.descarga_incompleta') }}');
                        }
                    });

                    setTimeout(function(){
                        resolve();
                    }, 250);
                });

                downloadPromise.then(() => {
                    resetDownloadButton();
                });
                
                return false;
            });

            $('#datatable').on('click', '.play', function(e) {
                var audio_id = $(this).attr('id').replace('play_', '');
                var numeroExtension = $('#numeroExtension').val();

                if(numeroExtension.length < 1) {
                    alert('{{ __('alerts.js.extension_requerida') }}');
                } else {
                    $.post('{{ asset('/audio/listen') }}/' + audio_id + '/' + numeroExtension, {
                        _token: "{{ csrf_token() }}",
                    })
                    .done(function (data) {
                        console.log(data);
                    });
                }
                
            });

            $('#datatable').on('click', '.upload', function(e) {
                var audio_id = $(this).attr('id').replace('upload_', '');

                // Creating the form: 
                var form = document.createElement('form');
                form.setAttribute('method', 'post');
                form.setAttribute('action', '{{ asset('/audio/upload') }}');
                form.setAttribute('enctype', 'multipart/form-data');

                var tInput = document.createElement('input');
                tInput.setAttribute('type', 'hidden');
                tInput.setAttribute('name', '_token');
                tInput.setAttribute('value', $('meta[name="csrf-token"]').attr('content'));

                var aInput = document.createElement('input');
                aInput.setAttribute('type', 'hidden');
                aInput.setAttribute('name', 'audio_id');
                aInput.setAttribute('value', audio_id);

                var fInput = document.createElement('input');
                fInput.setAttribute('type', 'file');
                fInput.setAttribute('name', 'audioFile');
                fInput.setAttribute('accept', '.wav, .ogg, .mp3, .gsm');

                var sInput = document.createElement('input');
                sInput.setAttribute('type', 'submit');
                sInput.setAttribute('value', 'Submit');

                form.appendChild(tInput);
                form.appendChild(aInput);
                form.appendChild(fInput);
                form.appendChild(sInput);

                document.getElementsByTagName('body')[0].appendChild(form);
                fInput.click();
                
                fInput.onchange = function() {
                    form.submit();
                };
            });

            $('#datatable').on('click', '.record', function(e) {
                var audio_id = $(this).attr('id').replace('record_', '');
                var numeroExtension = $('#numeroExtension').val();

                if(numeroExtension.length < 1) {
                    alert('{{ __('alerts.js.extension_requerida') }}');
                } else {
                    $.post('{{ asset('/audio/record') }}/' + audio_id + '/' + numeroExtension, {
                        _token: "{{ csrf_token() }}",
                    })
                    .done(function (data) {
                        console.log(data);
                    });
                }
            });

            $('#datatable').on('click', '.delete', function(e) {
                var audio_id = $(this).attr('id').replace('delete_', '');
                
                swal({
                    title: '{{ __('alerts.js.confirmacion.title') }}',
                    text: '{{ __('alerts.js.confirmacion.text') }}',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    cancelButtonText: '{{ __('alerts.js.confirmation.boton.no') }}',
                    confirmButtonText: '{{ __('alerts.js.confirmation.boton.yes') }}',
                    closeOnConfirm: false,
                    closeOnCancel: false,
                    allowEnterKey: false,
                    allowEscapeKey: false,
                    },
                    function(isConfirm) {
                        if (!isConfirm) {
                            $.post('{{ asset('/audio/remove') }}/' + audio_id, {
                                _token: "{{ csrf_token() }}",
                            })
                            .fail(function (xhr, status, error) {
                                var errors = JSON.parse(xhr.responseText);

                                $('#errors-block').removeClass('hidden');
                                $('#form-errors').empty();

                                for (var error in errors) {
                                    $('#form-errors').append('<li>' + errors[error] + '</li>');
                                }
                            })
                            .success(function(data) {
                                table.ajax.reload();
                                swal('{{ __('alerts.js.confirmation.deleted') }}', '{{ __('alerts.js.confirmation.deleted.text') }}', "success");
                            });
                            
                        } else {
                            swal('{{ __('alerts.js.confirmation.cancel') }}', '{{ __('alerts.js.confirmation.cancel.text') }}', "error");
                        }
                    }
                );
            });

            $(document).ready(function () {
                $("#datatable").on('click', 'tr td:not(:last-child)', function () {
                    window.location = "{{ asset('/audio') }}/" + $(this).closest('tr').attr('id') + '/edit';
                });
    
                table = $('#datatable').DataTable({
                    autoWidth: false,
                    ajax: '{{ asset('/audio/lista') }}',
                    dataSource: 'lista',
                    columns: [
                        {"data": "audio", "class": "editable"},
                        {"data": "descripcion", "class": "editable"},
                        {"data": "activo", "class": "text-center editable" },
                        {"data": "actions", "class" : "text-center" }
                    ],
                    info: false,
                    searching: true,
                    paging: true,
                    responsive: true,
                    pageLength: 8,
                    lengthChange: false,
                    ordering: false,
                    createdRow: function (row, data) {
                        row.id = data['id_audio']

                        if (row.id == item_actual) {
                            $(row.childNodes[0]).css('border-left', '0.3em solid #3598dc');
                            $(row.childNodes[3]).css('border-right', '0.3em solid #3598dc');
                        }
                    },
                    initComplete: function (settings, json) {
                        gotoPage();
                    },
                    columnDefs: [
                        { width: "15%", targets: 0 },
                        { width: "40%", targets: 1 },
                        { width: "5%", targets: 2 },
                        { width: "40%", targets: 3 },
                    ],
                });

                $("#datatable_filter").addClass("hidden");

                var search = $.fn.dataTable.util.throttle(
                    function (val) {
                        table.search(val).draw();
                    },
                    400
                );
                
                $('#audioSearchBox').on( 'keyup', function () {
                    search( this.value );
                });
            });
        </script>
    @endpush