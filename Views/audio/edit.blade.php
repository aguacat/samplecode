@extends('layouts.master')

@section('pagelevelcss')
    <link href="{{ asset('/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('layouts.pageheader')

        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-4">
                    <div class="portlet portlet-sortable light bordered">
                        <div class="portlet-title ui-sortable-handle">
                            <div class="caption font-blue-madison">
                                <i class="icon-wrench font-blue-madison"></i>
                                <span class="caption-subject bold uppercase">{{ __('page.bloque.izquierdo.titulo') }}</span>
                            <span class="caption-helper">{{ __('page.audio.bloque.izquierdo.subtitulo.edit') }}</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form id="audioForm" method="post" action="{{ asset('/audio/' . $audio_actual->id_audio) }}">

                                {{ csrf_field() }}

                                @include('layouts.errors')

                                <div id="errors-block" class="form-group hidden">
                                    <div class="alert alert-danger">
                                        <ul id="form-errors">
                                        </ul>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                    <label class="control-label">{{ __('page.audio.campo.audio') }}</label>
                                        <div class="input-icon right">
                                            <input type="text" class="form-control" name="audio" value="{{ $audio_actual->audio }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                    <label class="control-label">{{ __('page.audio.campo.descripcion') }}</label>
                                        <div class="input-icon right">
                                            <input type="text" class="form-control" name="descripcion" value="{{ $audio_actual->descripcion }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="mt-checkbox" style="position: relative">
                                            <input type="checkbox" name="activo" checked="{{ $audio_actual->activo ? 'checked' : '' }}" onclick="return false;">
                                                {{ __('page.audio.campo.activo') }}
                                            <span></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button id="patchForm" type="button" class="btn blue">
                                                {{ __('page.boton.modificar') }}
                                            </button>
                                            <a href="{{ asset('/audio') }}" class="btn default">
                                                {{ __('page.boton.cancel') }}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    @include('audio.table')
                </div>    
            </div>
            <!-- END PAGE BASE CONTENT -->

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('pagelevelplugins')
    <script src="{{ asset('/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endsection

@section('pagelevelscripts')
    <script src="{{ asset('pages/scripts/table-datatables-editable.min.js') }}" type="text/javascript"></script>
@endsection

@section('customjs')
    <script>
        var form = document.getElementById('audioForm');
        var input = document.createElement("input");

        input.type = "hidden";
        input.name = "_method";

        form.onsubmit = function () {
            form.target = '_self';
        };

        document.getElementById('patchForm').onclick = function () {
            input.value = "PUT";
            form.appendChild(input);
            form.submit();
        };

        // document.getElementById('destroyForm').onclick = function () {
        //     input.value = "DELETE";
        //     form.appendChild(input);
        //     form.submit();
        // }

    </script>

    @stack('table_scripts');
@endsection
