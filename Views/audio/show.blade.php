@extends('layouts.master')

@section('pagelevelcss')
    <link href="{{ asset('/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('layouts.pageheader')

        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-4">
                    <div class="portlet portlet-sortable light bordered">
                        <div class="portlet-title ui-sortable-handle">
                            <div class="caption font-blue-madison">
                                <i class="icon-wrench font-blue-madison"></i>
                                <span class="caption-subject bold uppercase">{{ __('page.bloque.izquierdo.titulo') }}</span>
                            <span class="caption-helper">{{ __('page.audio.bloque.izquierdo.subtitulo') }}</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form method="post" action="{{ asset('/audio') }}">

                                {{ csrf_field() }}

                                @include('layouts.errors')

                                <div id="errors-block" class="form-group hidden">
                                    <div class="alert alert-danger">
                                        <ul id="form-errors">
                                        </ul>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                    <label class="control-label">{{ __('page.audio.campo.audio') }}</label>
                                        <div class="input-icon right">
                                            <input type="text" class="form-control" name="audio" value="{{ old('audio') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                    <label class="control-label">{{ __('page.audio.campo.descripcion') }}</label>
                                        <div class="input-icon right">
                                            <input type="text" class="form-control" name="descripcion" value="{{ old('descripcion') }}">
                                        </div>
                                    </div>

                                    {{-- <div class="form-group">
                                        <label class="mt-checkbox" style="position: relative">
                                            <input type="checkbox" name="activo" checked="checked">
                                        {{ 'Activo'/*__('page.usuario.campo.activo')*/ }}
                                            <span></span>
                                        </label>
                                    </div> --}}
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit"
                                                    class="btn blue">{{ __('page.boton.guardar') }}</button>
                                            <button type="reset"
                                                    class="btn default">{{ __('page.boton.cancel') }}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    @include('audio.table')
                </div>    
            </div>
            <!-- END PAGE BASE CONTENT -->

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('pagelevelplugins')
    <script src="{{ asset('/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endsection

@section('pagelevelscripts')
    <script src="{{ asset('pages/scripts/table-datatables-editable.min.js') }}" type="text/javascript"></script>
@endsection

@section('customjs')
    @stack('table_scripts');
@endsection
