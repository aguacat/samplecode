<div class="row">
    <div class="col-md-4">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-wrench font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.izquierdo.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.bloqueo.bloque.izquierdo.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <form method="post">

                    {{ csrf_field() }}

                    <div id="bloqueo-errors-block" class="form-group hidden">
                        <div class="alert alert-danger">
                            <ul id="bloqueo-form-errors">
                            </ul>
                        </div>
                    </div>

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.bloqueo.campo.extension_destino') }}
                            </label>
                            <div class="input-icon right">
                                <input type="text" class="form-control" id="bloqueo_ext_destino" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.bloqueo.campo.descripcion') }}
                            </label>
                            <div class="input-icon right">
                                <input type="text" class="form-control" id="bloqueo_descripcion" value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div id="bloqueo_action_section" class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn blue" onclick="bloqueoPrepareForm(requestType.POST)">
                                    {{ __('page.boton.guardar') }}
                                </button>
                                <button type="reset" class="btn default">
                                    {{ __('page.boton.cancel') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-list font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.derecho.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.bloqueo.bloque.derecho.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-hover table-light">
                    <thead>
                    <tr class="uppercase">
                        <th width="30%">
                            {{ __('page.extension.bloqueo.tabla.campo.extension_destino') }}
                        </th>
                        <th width="70%">
                            {{ __('page.extension.bloqueo.tabla.campo.descripcion') }}
                        </th>
                    </tr>
                    </thead>
                    <tbody id="bloqueos">
                    @forelse($extensiones_bloqueo as $extension_bloqueo)
                        <tr id="{{ $extension_bloqueo->id }}" onclick="bloqueoSelectData($(this))">
                            <td>{{ $extension_bloqueo->ext_destino }}</td>
                            <td>{{ $extension_bloqueo->descripcion }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">
                                <h4 class="text-center">
                                    {{ __('page.extension.bloqueo.tabla.empty') }}
                                </h4>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function bloqueoRefreshTable() {
            $('#bloqueos').empty();

            var extension_actual_name = $('#extension_actual_name').html();

            $.get('{{ asset('/extension_opcion_data') }}' + "/bloqueo/" + extension_actual_name, function (data) {
                if (data.length > 0) {
                    $.each(data, function (index, value) {
                        var columna1 = '<td>' + value.ext_destino + '</td>';
                        var columna2 = '<td>' + value.descripcion + '</td>';
                        var fila = '<tr id="' + value.id + '" onclick="bloqueoSelectData($(this))">' + columna1 + columna2 + '</tr>';

                        $('#bloqueos').append(fila);
                    });
                }

                if ($('#bloqueos tr').length == 0) {
                    $('#bloqueos').append('<tr><td colspan="2"><h4 class="text-center">{{ __('page.extension.bloqueo.tabla.empty')}}</h4></td></tr>');
                }
            })
                .fail(function () {
                    alert("{{ __('page.extension.alert.error') }}");
                })
        }

        function bloqueoResetAction() {
            $('#bloqueo_ext_destino').val('');
            $('#bloqueo_descripcion').val('');
            $('#bloqueo').find('tr').removeAttr('style');
            bloqueoActionBlock('create');
        }

        function bloqueoActionBlock(status) {
            $('#bloqueo_action_section').empty();

            if (status == 'update_delete') {
                var btnModificar = '<button id="patchForm" type="button" class="btn blue" onclick="bloqueoPrepareForm(requestType.PUT)">{{ __('page.boton.modificar') }}</button>';
                var btnEliminar = '<button id="destroyForm" type="button" class="btn red pull-right" onclick="bloqueoPrepareForm(requestType.DELETE)">{{ __('page.boton.eliminar') }}</button>';
                var btnCancelar = '<a class="btn default" onclick="bloqueoResetAction()">{{ __('page.boton.cancel') }}</a>';
                var divAccion = '<div class="col-md-8 pull-left">' + btnModificar + '&nbsp;' + btnCancelar + '</div><div class="col-md-4 pull-right">' + btnEliminar + '</div>';

                $('#bloqueo_action_section').append(divAccion);

            } else {
                var btnGuardar = '<button id="postForm" type="button" class="btn blue" onclick="bloqueoPrepareForm(requestType.POST)">{{ __('page.boton.guardar') }}</button> ';
                var btnCancelar = '<button type="reset" class="btn default">{{ __('page.boton.cancel') }}</button>';
                var divAccion = '<div class="col-md-offset-3 col-md-9">' + btnGuardar + btnCancelar + '</div>';

                $('#bloqueo_action_section').append(divAccion);
            }
        }

        function bloqueoPrepareForm(currentRequestType) {
            $('#bloqueo-errors-block').addClass('hidden');

            var extension_actual_name = $('#extension_actual_name').html();
            var numero = $('#bloqueo_ext_destino').val();
            var descripcion = $('#bloqueo_descripcion').val();

            switch (currentRequestType) {
                case requestType.POST:
                    $.post('{{ asset('extension_opcion/bloqueo') }}',
                        {
                            _token: "{{ csrf_token() }}",
                            _method: "POST",
                            name: extension_actual_name,
                            numero: numero,
                            descripcion: descripcion
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#bloqueo-errors-block').removeClass('hidden');
                            $('#bloqueo-form-errors').empty();

                            for (var error in errors) {
                                $('#bloqueo-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            bloqueoRefreshTable();
                            bloqueoResetAction();
                        });
                    break;
                case requestType.PUT:
                    $.put('{{ asset('extension_opcion/bloqueo') }}/' + $('.bloqueo_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            //_method: "PUT",
                            name: extension_actual_name,
                            numero: numero,
                            descripcion: descripcion
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#bloqueo-errors-block').removeClass('hidden');
                            $('#bloqueo-form-errors').empty();

                            for (var error in errors) {
                                $('#bloqueo-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            bloqueoRefreshTable();
                            bloqueoResetAction();
                        });
                    break;
                case requestType.DELETE:
                    $.delete('{{ asset('extension_opcion/bloqueo') }}/' + $('.bloqueo_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            _method: "DELETE"
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#bloqueo-errors-block').removeClass('hidden');
                            $('#bloqueo-form-errors').empty();

                            for (var error in errors) {
                                $('#bloqueo-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            bloqueoRefreshTable();
                            bloqueoResetAction();
                        });
                    break;
                default:
                    console.log('Nope... Failed.');
                    //Invalid Request.
                    break;
            }
        }

        function bloqueoSelectData(element) {
            $('#bloqueos').find('tr').removeAttr('style');

            element.addClass('bloqueo_selected_row');
            element.css('border-left', '0.3em solid #3598dc');
            element.css('border-right', '0.3em solid #3598dc');

            $.get('{{ asset('extension_opcion') }}/bloqueo/' + element.prop('id'))
                .done(function (data) {
                    $('#bloqueo_ext_destino').val(data.field1);
                    $('#bloqueo_descripcion').val(data.field2);

                    bloqueoActionBlock('update_delete');
                })
        }
    </script>
@endpush