<div class="row">
    <div class="col-md-4">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-wrench font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.izquierdo.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.listablanca.bloque.izquierdo.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <form method="post">

                    {{ csrf_field() }}

                    <div id="listablanca-errors-block" class="form-group hidden">
                        <div class="alert alert-danger">
                            <ul id="listablanca-form-errors">
                            </ul>
                        </div>
                    </div>

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.listablanca.campo.extension_destino') }}
                            </label>
                            <div class="input-icon right">
                                <input type="text" class="form-control" id="listablanca_ext_destino" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.listablanca.campo.descripcion') }}
                            </label>
                            <div class="input-icon right">
                                <input type="text" class="form-control" id="listablanca_descripcion" value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div id="listablanca_action_section" class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button"
                                        class="btn blue"
                                        onclick="listablancaPrepareForm(requestType.POST)">
                                    {{ __('page.boton.guardar') }}
                                </button>
                                <button type="reset" class="btn default">
                                    {{ __('page.boton.cancel') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-list font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.derecho.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.listablanca.bloque.derecho.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-hover table-light">
                    <thead>
                    <tr class="uppercase">
                        <th width="30%">
                            {{ __('page.extension.listablanca.tabla.campo.extension_destino') }}
                        </th>
                        <th width="70%">
                            {{ __('page.extension.listablanca.tabla.campo.descripcion') }}
                        </th>
                    </tr>
                    </thead>
                    <tbody id="tablalistablanca">
                    @forelse($extensiones_lista_blanca as $extension_lista_blanca)
                        <tr id="{{ $extension_lista_blanca->id }}" onclick="listablancaSelectData($(this))">
                            <td>{{ $extension_lista_blanca->ext_destino }}</td>
                            <td>{{ $extension_lista_blanca->descripcion }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">
                                <h4 class="text-center">
                                    {{ __('page.extension.listablanca.tabla.empty') }}
                                </h4>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function listablancaRefreshTable() {
            $('#tablalistablanca').empty();

            var extension_actual_name = $('#extension_actual_name').html();

            $.get('{{ asset('/extension_opcion_data') }}' + "/listablanca/" + extension_actual_name, function (data) {
                if (data.length > 0) {
                    $.each(data, function (index, value) {
                        var columna1 = '<td>' + value.ext_destino + '</td>';
                        var columna2 = '<td>' + value.descripcion + '</td>';
                        var fila = '<tr id="' + value.id + '" onclick="listablancaSelectData($(this))">' + columna1 + columna2 + '</tr>';

                        $('#tablalistablanca').append(fila);
                    });
                }

                if ($('#tablalistablanca tr').length == 0) {
                    $('#tablalistablanca').append('<tr><td colspan="2"><h4>{{ __('page.extension.listablanca.tabla.empty') }}</h4></td></tr>');
                }
            })
                .fail(function () {
                    alert("{{ __('page.extension.alert.error') }}");
                })
        }

        function listablancaResetAction() {
            $('#listablanca_ext_destino').val('');
            $('#listablanca_descripcion').val('');
            $('#tablalistablanca').find('tr').removeAttr('style');
            listablancaActionBlock('create');
        }

        function listablancaActionBlock(status) {
            $('#listablanca_action_section').empty();

            if (status == 'update_delete') {
                var btnModificar = '<button id="patchForm" type="button" class="btn blue" onclick="listablancaPrepareForm(requestType.PUT)">{{ __('page.boton.modificar') }}</button>';
                var btnEliminar = '<button id="destroyForm" type="button" class="btn red pull-right" onclick="listablancaPrepareForm(requestType.DELETE)">{{ __('page.boton.eliminar') }}</button>';
                var btnCancelar = '<a class="btn default" onclick="listablancaResetAction()">{{ __('page.boton.cancel') }}</a>';
                var divAccion = '<div class="col-md-8 pull-left">' + btnModificar + '&nbsp;' + btnCancelar + '</div><div class="col-md-4 pull-right">' + btnEliminar + '</div>';

                $('#listablanca_action_section').append(divAccion);

            } else {
                var btnGuardar = '<button id="postForm" type="button" class="btn blue" onclick="listablancaPrepareForm(requestType.POST)">{{ __('page.boton.guardar') }}</button> ';
                var btnCancelar = '<button type="reset" class="btn default">{{ __('page.boton.cancel') }}</button>';
                var divAccion = '<div class="col-md-offset-3 col-md-9">' + btnGuardar + btnCancelar + '</div>';

                $('#listablanca_action_section').append(divAccion);
            }
        }

        function listablancaPrepareForm(currentRequestType) {
            $('#listablanca-errors-block').addClass('hidden');

            var extension_actual_name = $('#extension_actual_name').html();
            var numero = $('#listablanca_ext_destino').val();
            var descripcion = $('#listablanca_descripcion').val();

            switch (currentRequestType) {
                case requestType.POST:
                    $.post('{{ asset('extension_opcion/listablanca') }}',
                        {
                            _token: "{{ csrf_token() }}",
                            _method: "POST",
                            name: extension_actual_name,
                            numero: numero,
                            descripcion: descripcion
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#listablanca-errors-block').removeClass('hidden');
                            $('#listablanca-form-errors').empty();

                            for (var error in errors) {
                                $('#listablanca-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            listablancaRefreshTable();
                            listablancaResetAction();
                        });
                    break;
                case requestType.PUT:
                    $.put('{{ asset('extension_opcion/listablanca') }}/' + $('.listablanca_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            name: extension_actual_name,
                            numero: numero,
                            descripcion: descripcion
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#listablanca-errors-block').removeClass('hidden');
                            $('#listablanca-form-errors').empty();

                            for (var error in errors) {
                                $('#listablanca-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            listablancaRefreshTable();
                            listablancaResetAction();
                        });
                    break;
                case requestType.DELETE:
                    $.delete('{{ asset('extension_opcion/listablanca') }}/' + $('.listablanca_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            _method: "DELETE"
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#listablanca-errors-block').removeClass('hidden');
                            $('#listablanca-form-errors').empty();

                            for (var error in errors) {
                                $('#listablanca-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            listablancaRefreshTable();
                            listablancaResetAction();
                        });
                    break;
                default:
                    console.log('Nope... Failed.');
                    //Invalid Request.
                    break;
            }
        }

        function listablancaSelectData(element) {
            $('#tablalistablanca').find('tr').removeAttr('style');

            element.addClass('listablanca_selected_row');
            element.css('border-left', '0.3em solid #3598dc');
            element.css('border-right', '0.3em solid #3598dc');

            $.get('{{ asset('extension_opcion') }}/listablanca/' + element.prop('id'))
                .done(function (data) {
                    $('#listablanca_ext_destino').val(data.field1);
                    $('#listablanca_descripcion').val(data.field2);

                    listablancaActionBlock('update_delete');
                })
        }
    </script>
@endpush