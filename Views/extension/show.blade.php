@extends('layouts.master')

@section('pagelevelcss')
    <link href="{{ asset('/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('layouts.pageheader')

        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet portlet-sortable light bordered">
                        <div class="portlet-title ui-sortable-handle">
                            <div class="caption font-blue-madison">
                                <i class="icon-list font-blue-madison"></i>
                                <span class="caption-subject bold uppercase">
                                    {{ __('page.extension_lista.bloque.derecho.titulo') }}
                                </span>
                                <span class="caption-helper">
                                    {{ __('page.extension_lista.bloque.derecho.subtitulo') }}
                                </span>
                            </div>
                            <div>
                                <div class="col-md-3 pull-right">
                                    <div class="input-group">
                                    <input id="extSearchBox" type="text" class="form-control" placeholder="{{ __('page.extension_lista.tabla.buscar') }}" aria-describedby="sizing-addon1">
                                        <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-search" style="color: #2C3E50 !important"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table table-borderless">
                                <table id="datatable" class="table table-hover table-light">
                                    <thead>
                                    <tr class="uppercase">
                                        <th class="all text-center table-grey-title" width="5%">
                                            {{ __('page.extension_lista.tabla.extension') }}
                                        </th>
                                        <th class="all text-center table-grey-title" width="5%" style="">
                                            {{ __('page.extension_lista.tabla.nombre') }}
                                        </th>
                                        <th class="all text-center table-grey-title" width="10%">
                                            {{ __('page.extension_lista.tabla.departamento') }}
                                        </th>
                                        <th class="all text-center table-grey-title" width="10%">
                                            {{ __('page.extension_lista.tabla.politica') }}
                                        </th>
                                        <th class="all text-center table-grey-title" width="10%">
                                            {{ __('page.extension_lista.tabla.mac_address') }}
                                        </th>
                                        <th class="all text-center table-grey-title" width="10%">
                                            {{ __('page.extension_lista.tabla.tipo') }}
                                        </th>
                                        <th class="all text-center table-grey-title" width="10">
                                            {{ __('page.extension_lista.tabla.modelo') }}
                                        </th>
                                        <th class="all text-center table-grey-title" width="20%">
                                            {{ __('page.extension_lista.tabla.id_fabricante') }}
                                        </th>
                                        <th class="none text-center" width="">
                                            {{ __('page.extension_lista.tabla.email') }}
                                        </th>
                                        <th class="none text-center" width="">
                                            {{ __('page.extension_lista.tabla.direccion_ip') }}
                                        </th>
                                        <th class="none text-center" width="">
                                            {{ __('page.extension_lista.tabla.mon') }}
                                        </th>
                                        <th class="none text-center" width="">
                                            {{ __('page.extension_lista.tabla.cript') }}
                                        </th>
                                        <th class="none text-center" width="">
                                            {{ __('page.extension_lista.tabla.trans') }}
                                        </th>
                                        <th class="none text-center" width="">
                                            {{ __('page.extension_lista.tabla.fecha') }}
                                        </th>
                                        <th class="none text-center" width="">
                                            {{ __('page.extension_lista.tabla.hora') }}
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{-- @forelse($extensiones as $extension)
                                        <tr id="{{ $extension->id_ext }}">
                                            <td class="text-center editable" width="">{{ $extension->name }}</td>
                                            <td class="text-center editable"
                                                width="">{{ $extension->display_name }}</td>
                                            <td class="text-center editable"
                                                width="">{{ $extension->departamento->departamento }}</td>
                                            <td class="text-center editable"
                                                width="">{{ $extension->politica->politica }}</td>
                                            <td class="text-center editable" width="">{{ $extension->mac_address }}</td>
                                            <td class="text-center editable"
                                                width="">{{ $extension->ext_tipo->ext_tipo }}</td>
                                            <td class="text-center editable"
                                                width="">{{ $extension->dispositivo_modelo->dispositivo_modelo }}</td>
                                            <td class="text-center editable" width="">{{ $extension->useragent }}</td>
                                            <td class="text-center" width="">{{ $extension->email }}</td>
                                            <td class="text-center" width="">{{ $extension->nasvox_gui_ipaddress }}</td>
                                            <td class="text-center" width="">
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox"
                                                           @if($extension->sistema_monitoreo_activado) checked="checked"
                                                           @endif disabled>
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td class="text-center" width="">{{ ucfirst($extension->encryption) }}</td>
                                            <td class="text-center"
                                                width="">{{ strtoupper($extension->transport) }}</td>
                                            <td class="text-center"
                                                width="">@if($extension->fecha)  {{ date('d/m/Y', strtotime($extension->fecha)) }} @endif</td>
                                            <td class="text-center"
                                                width="">@if($extension->hora) {{ $extension->hora }} @endif</td>
                                        </tr>
                                    @empty
                                        <tr><td colspan="11" class="text-center">No users</td></tr>
                                    @endforelse --}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('pagelevelplugins')
    <script src="{{ asset('/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/dataTables.buttons.min.js') }}" type="text/javascript"></script>

@endsection

@section('pagelevelscripts')
    <script src="{{ asset('pages/scripts/table-datatables-editable.min.js') }}" type="text/javascript"></script>
@endsection

@section('customjs')
    <script>
        var table = '';

        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr td:not(:first-child, :last-child)', function () {
                window.location = '{{ asset('/extension') }}/' + $(this).closest('tr').prop('id') + '/edit';
            });

            table = $('#datatable').DataTable({
                ajax: '{{ asset('/extension/lista') }}',
                dataSource: 'lista',
                columns : [
                    {"data": 'name', "sClass": "font-blue-madison text-center"},
                    {"data": 'display_name', "sClass": "font-blue-dark text-center editable"},
                    {"data": 'departamento', "sClass": "font-blue-dark text-center editable"},
                    {"data": 'politica', "sClass": "font-blue-dark text-center editable"},
                    {"data": 'mac_address', "sClass": "font-blue-dark text-center editable"},
                    {"data": 'ext_tipo', "sClass": "font-blue-dark text-center editable"},
                    {"data": 'dispositivo_modelo', "sClass": "font-blue-dark text-center editable"},
                    {"data": 'useragent', "sClass": "font-blue-dark text-center editable"},
                    {"data": 'email', "sClass": "font-grey-mint"},
                    {"data": 'nasvox_gui_ipaddress', "sClass": "font-grey-mint"},
                    {"data": 'sistema_monitoreo_activado', "sClass": "font-grey-mint"},
                    {"data": 'encryption', "sClass": "font-grey-mint"},
                    {"data": 'transport', "sClass": "font-grey-mint"},
                    {"data": 'fecha', "sClass": "font-grey-mint"},
                    {"data": 'hora', "sClass": "font-grey-mint"},
                ],
                info: true,
                searching: true,
                paging: true,
                lengthChange: false,
                pageLength: 7,
                responsive: true,
                createdRow: function (row, data) {
                    row.id = data['id']

                    // if (row.id == item_actual) {
                    //     $(row.childNodes[0]).css('border-left', '0.3em solid #3C8DBC');
                    //     $(row.childNodes[1]).css('border-right', '0.3em solid #3C8DBC');
                    // }
                },
                //dom: 'Bfrtip',
                // buttons: [
                //     {
                //         text: 'Nueva Extensión',
                //         action: function (e, dt, node, config) {
                //             alert('Button activated');
                //         }
                //     }
                // ]
            });

            $("#datatable_filter").addClass("hidden");

            var search = $.fn.dataTable.util.throttle(
                function ( val ) {
                    table.search( val ).draw();
                },
                400
            );
            
            $('#extSearchBox').on( 'keyup', function () {
                search( this.value );
            });
        });
    </script>
@endsection