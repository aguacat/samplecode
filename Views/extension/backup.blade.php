<div class="row">
    <div class="col-md-4">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-wrench font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.izquierdo.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.backup.bloque.izquierdo.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <form method="post">

                    {{ csrf_field() }}

                    <div id="backup-errors-block" class="form-group hidden">
                        <div class="alert alert-danger">
                            <ul id="backup-form-errors">
                            </ul>
                        </div>
                    </div>

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.backup.campo.extension_destino') }}
                            </label>
                            <select class="form-control select2" data-placeholder="Escoja Extensión Destino"
                                    style="width:100%" id="backup_ext_destino">
                                <option></option>
                                @foreach($extensiones_destino as $extension_destino)
                                    <option value="{{ $extension_destino->name }}">{{ $extension_destino->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.backup.campo.descripcion') }}
                            </label>
                            <div class="input-icon right">
                                <input type="text" class="form-control" id="backup_descripcion">
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div id="backup_action_section" class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button" class="btn blue" onclick="backupPrepareForm(requestType.POST)">
                                    {{ __('page.boton.guardar') }}
                                </button>
                                <button type="reset" class="btn default">
                                    {{ __('page.boton.cancel') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-list font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.derecho.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.backup.bloque.derecho.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-hover table-light">
                    <thead>
                    <tr class="uppercase">
                        <th width="30%">
                            {{ __('page.extension.backup.tabla.campo.extension_destino') }}
                        </th>
                        <th width="70%">
                            {{ __('page.extension.backup.tabla.campo.descripcion') }}
                        </th>
                    </tr>
                    </thead>
                    <tbody id="backups">
                    @forelse($extensiones_backup as $extension_backup)
                        <tr id="{{ $extension_backup->id }}" onclick="backupSelectData($(this))">
                            <td>{{ $extension_backup->ext_destino }}</td>
                            <td>{{ $extension_backup->descripcion }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">
                                <h4 class="text-center">
                                    {{ __('page.extension.backup.tabla.empty') }}
                                </h4>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function backupRefreshTable() {
            $('#backups').empty();

            var extension_actual_name = $('#extension_actual_name').html();

            $.get('{{ asset('/extension_opcion_data') }}' + "/backup/" + extension_actual_name, function (data) {
                if (data.length > 0) {
                    $.each(data, function (index, value) {
                        var columna1 = '<td>' + value.ext_destino + '</td>';
                        var columna2 = '<td>' + value.descripcion + '</td>';
                        var fila = '<tr id="' + value.id + '" onclick="backupSelectData($(this))">' + columna1 + columna2 + '</tr>';

                        $('#backups').append(fila);
                    });
                }

                if ($('#backups tr').length == 0) {
                    $('#backups').append('<tr><td colspan="2"><h4 class="text-center">{{ __('page.extension.backup.tabla.empty') }}</h4></td></tr>');
                }
            }).fail(function () {
                alert("{{ __('page.extension.alert.error') }}");
            })
        }

        function backupResetAcceso() {
            $('#backup_ext_destino').val('').trigger('change');
            $('#backup_descripcion').val('');
            $('#backups').find('tr').removeAttr('style');
            backupActionBlock('create');
        }

        function backupActionBlock(status) {
            $('#backup_action_section').empty();

            if (status == 'update_delete') {
                var btnModificar = '<button id="patchForm" type="button" class="btn blue" onclick="backupPrepareForm(requestType.PUT)">{{ __('page.boton.modificar') }}</button>';
                var btnEliminar = '<button id="destroyForm" type="button" class="btn red pull-right" onclick="backupPrepareForm(requestType.DELETE)">{{ __('page.boton.eliminar') }}</button>';
                var btnCancelar = '<a class="btn default" onclick="backupResetAcceso()">{{ __('page.boton.cancel') }}</a>';
                var divAccion = '<div class="col-md-8 pull-left">' + btnModificar + '&nbsp;' + btnCancelar + '</div><div class="col-md-4 pull-right">' + btnEliminar + '</div>';

                $('#backup_action_section').append(divAccion);

            } else {
                var btnGuardar = '<button id="postForm" type="button" class="btn blue" onclick="backupPrepareForm(requestType.POST)">{{ __('page.boton.guardar') }}</button> ';
                var btnCancelar = '<button type="reset" class="btn default">{{ __('page.boton.cancel') }}</button>';
                var divAccion = '<div class="col-md-offset-3 col-md-9">' + btnGuardar + btnCancelar + '</div>';

                $('#backup_action_section').append(divAccion);
            }
        }

        function backupPrepareForm(currentRequestType) {
            $('#backup-errors-block').addClass('hidden');

            var extension_actual_name = $('#extension_actual_name').html();
            var numero = $('#backup_ext_destino').val();
            var descripcion = $('#backup_descripcion').val();

            switch (currentRequestType) {
                case requestType.POST:
                    $.post('{{ asset('extension_opcion/backup') }}',
                        {
                            _token: "{{ csrf_token() }}",
                            name: extension_actual_name,
                            numero: numero,
                            descripcion: descripcion
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#backup-errors-block').removeClass('hidden');
                            $('#backup-form-errors').empty();

                            for (var error in errors) {
                                $('#backup-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            backupRefreshTable();
                            backupResetAcceso();
                        });
                    break;
                case requestType.PUT:
                    $.put('{{ asset('extension_opcion/backup') }}/' + $('.backup_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            name: extension_actual_name,
                            numero: numero,
                            descripcion: descripcion
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#backup-errors-block').removeClass('hidden');
                            $('#backup-form-errors').empty();

                            for (var error in errors) {
                                $('#backup-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            backupRefreshTable();
                            backupResetAcceso();
                        });
                    break;
                case requestType.DELETE:
                    $.delete('{{ asset('extension_opcion/backup') }}/' + $('.backup_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            _method: "DELETE"
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#backup-errors-block').removeClass('hidden');
                            $('#backup-form-errors').empty();

                            for (var error in errors) {
                                $('#backup-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            backupRefreshTable();
                            backupResetAcceso();
                        });
                    break;
                default:
                    console.log('Nope... Failed.');
                    //Invalid Request.
                    break;
            }
        }

        function backupSelectData(element) {
            $('#backups').find('tr').removeAttr('style');

            element.addClass('backup_selected_row');
            element.css('border-left', '0.3em solid #3598dc');
            element.css('border-right', '0.3em solid #3598dc');

            $.get('{{ asset('extension_opcion') }}/backup/' + element.prop('id'))
                .done(function (data) {
                    console.log(data);

                    $('#backup_ext_destino').val(data.field1).trigger('change');
                    $('#backup_descripcion').val(data.field2);

                    backupActionBlock('update_delete');
                })
        }
    </script>
@endpush