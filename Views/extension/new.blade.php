@extends('layouts.master')

@section('pagelevelcss')
    <!-- BEGIN SELECT2 -->
    <link href="{{ asset('/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <!-- END SELECT2 -->
    <link href="{{ asset('/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('layouts.pageheader')

        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-4">
                    <div class="portlet portlet-sortable light bordered">
                        <div class="portlet-title ui-sortable-handle">
                            <div class="caption font-blue-madison">
                                <i class="icon-list font-blue-madison"></i>
                                <span class="caption-subject bold uppercase">
                                    {{ __('page.extension.bloque.izquierdo.titulo') }}
                                </span>
                                <span class="caption-helper">
                                    {{ __('page.extension.bloque.izquierdo.subtitulo') }}
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <!-- BEGIN FORM-->
                            <form method="post" action="{{ asset('/extension_nueva') }}" class="form-horizontal">

                                {{ csrf_field() }}

                                @include('layouts.errors')

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.extension') }}
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.extension') }}"
                                                   id="ext_name"
                                                   name="name"
                                                   value="{{ old('name') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.contrasena') }}"
                                                   name="contrasena"
                                                   value="{{ old('contrasena') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.tipo') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="id_ext_tipo"
                                                    id="id_ext_tipo"
                                                    data-placeholder="{{ __('page.extension.placeholder.tipo') }}">
                                                <option value=""></option>
                                                @foreach($tipos as $tipo)
                                                    <option value="{{$tipo->id_ext_tipo}}"
                                                            @if($tipo->id_ext_tipo == old('id_ext_tipo')) selected="selected" @endif>{{ $tipo->ext_tipo }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.nombre') }}
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.nombre') }}"
                                                   name="display_name"
                                                   value="{{ old('display_name') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.email') }}
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.email') }}"
                                                   name="email"
                                                   value="{{ old('email') }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.dept') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="departamento"
                                                    id="departamento"
                                                    data-placeholder="{{ __('page.extension.placeholder.dept') }}">
                                                <option value=""></option>
                                                @foreach($departamentos as $departamento)
                                                    <option value="{{ $departamento->id_departamento }}"
                                                            @if($departamento->id_departamento == old('departamento')) selected="selected" @endif>{{ $departamento->sucursal->sucursal }}
                                                        | {{ $departamento->departamento }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.politica') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="politica"
                                                    id="politica"
                                                    data-placeholder="{{ __('page.extension.placeholder.politica') }}">
                                                <option value=""></option>
                                                @foreach($politicas as $politica)
                                                    <option value="{{ $politica->id_politica }}"
                                                            @if($politica->id_politica == old('id_politica')) selected="selected" @endif>{{ $politica->politica }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-bottom: 0">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.medida_respaldo') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="medida_respaldo_llamada"
                                                    id="medida_respaldo_llamada"
                                                    data-placeholder="{{ __('page.extension.placeholder.medida_respaldo') }}"
                                                    onchange="loadMedidaRespaldo($(this))">
                                                <option value=""></option>
                                                @foreach($medidas_respaldo as $medida_respaldo)
                                                    <option value="{{$medida_respaldo->medida_respaldo}}"
                                                            @if($medida_respaldo->medida_respaldo == old('medida_respaldo_llamada')) selected="selected" @endif>{{ $medida_respaldo->descripcion }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div id="contenedor_medida_respaldo_llamada_dato" class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.medida_respaldo_dato') }}"
                                                   name="medida_respaldo_llamada_dato"
                                                   value="{{ old('medida_respaldo_llamada_dato') }}"
                                                   @if(empty(old('medida_respaldo_llamada_dato'))) readonly="readonly" @endif>
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-bottom: 0">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.medida_perdida') }}
                                        </label>
                                        <div id="contenedor_medida_perdida_llamada" class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.medida_perdida') }}"
                                                   name="medida_perdida_llamada_dato"
                                                   readonly="readonly">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div id="contenedor_medida_perdida_llamada_dato" class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.medida_perdida_dato') }}"
                                                   name="medida_perdida_llamada_dato"
                                                   readonly="readonly">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.modelo_dispositivo') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="id_dispositivo_modelo"
                                                    id="id_dispositivo_modelo"
                                                    data-placeholder="{{ __('page.extension.placeholder.modelo_dispositivo') }}">
                                                <option value=""></option>
                                                @foreach($dispositivos_modelo as $dispositivo_modelo)
                                                    <option value="{{$dispositivo_modelo->id_dispositivo_modelo}}"
                                                            @if($dispositivo_modelo->id_dispositivo_modelo == old('id_dispositivo_modelo')) selected="selected" @endif>{{ $dispositivo_modelo->dispositivo_modelo }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.protocolo') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="id_ext_protocolo"
                                                    id="id_ext_protocolo"
                                                    data-placeholder="{{ __('page.extension.placeholder.protocolo') }}">
                                                <option value=""></option>
                                                @foreach($ext_protocolos as $ext_protocolo)
                                                    <option value="{{$ext_protocolo->id_ext_protocolo}}"
                                                            @if($ext_protocolo->id_ext_protocolo == old('id_ext_protocolo')) selected="selected" @endif>{{ $ext_protocolo->ext_protocolo }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.mac_address') }}
                                        </label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <input type="text"
                                                       class="form-control"
                                                       placeholder="{{ __('page.extension.placeholder.mac_address') }}"
                                                       id="ext_mac_address"
                                                       name="mac_address"
                                                       value="{{ old('mac_address') }}">
                                                <span class="input-group-btn">
                                                    <button class="btn blue" type="button"
                                                            onclick="generateMacAddress()"><i
                                                                class="fa fa-gears"></i>
                                                    </button>
                                                </span>
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.forward_activado') }}
                                        </label>
                                        <div class="col-md-2">
                                            <label class="mt-checkbox" style="position: relative">
                                                <input type="checkbox" name="fwd"
                                                       @if(old('fwd')) checked="checked" @endif>
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-md-7">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.forward_activado') }}"
                                                   name="fwd_numero"
                                                   value="{{ old('fwd_numero') }}">
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn blue">
                                                    {{ __('page.boton.guardar') }}
                                                </button>
                                                <button type="reset" class="btn default">
                                                    {{ __('page.boton.cancel') }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="portlet portlet-sortable light bordered">
                        <div class="portlet-title ui-sortable-handle">
                            <div class="caption font-blue-madison">
                                <i class="icon-list font-blue-madison"></i>
                                <span class="caption-subject bold uppercase">
                                    {{ __('page.extension.bloque.derecho.titulo') }}
                                </span>
                                <span class="caption-helper">
                                    {{ __('page.extension.bloque.derecho.subtitulo') }}
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.opcion_de_llamada.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.opcion_de_llamada.1') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="bloqueo_llamadas_fuera"
                                                           @if($extension_nueva->bloqueo_llamadas_fuera) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.2') }}</td>
                                            <td class="text-center">
                                                <input type="text" class="form-control text-center"
                                                       name="duracion_maxima_llamada_entrante"
                                                       id="duracion_maxima_llamada_entrante"
                                                       value="{{ $extension_nueva->duracion_maxima_llamada_entrante }}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.3') }}</td>
                                            <td class="text-center">
                                                <input type="text" class="form-control text-center"
                                                       name="duracion_maxima_llamada_saliente"
                                                       id="duracion_maxima_llamada_saliente"
                                                       value="{{ $extension_nueva->duracion_maxima_llamada_saliente }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.4') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="email_llamada_perdida"
                                                           @if($extension_nueva->email_llamada_perdida) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.5') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="forward_automatico"
                                                           @if($extension_nueva->forward_automatico) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.6') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="forward_simultaneo"
                                                           @if($extension_nueva->forward_simultaneo) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.7') }}</td>
                                            <td class="text-center">
                                                <select class="form-control input-sm select2" 
                                                        name="language"
                                                        id="language">
                                                    @foreach($languages as $language)
                                                        <option value="{{ $language->id_language }}"
                                                                @if($language->id_language == $extension_nueva->id_language) selected="selected" @endif>{{ substr($language->language, 0, 3) }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.8') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="whitelist_habilitado"
                                                           @if($extension_nueva->whitelist_habilitado) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.9') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox"
                                                           name="nunca_pedir_contrasena_en_llamada"
                                                           @if($extension_nueva->nunca_pedir_contrasena_en_llamada) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.10') }}</td>
                                            <td class="text-center">
                                                <input type="text" class="form-control text-center"
                                                       name="operador"
                                                       id="operador" value="{{ $extension_nueva->operador }}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.11') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="pin_global"
                                                           @if($extension_nueva->pin_global) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.12') }}</td>
                                            <td class="text-center">
                                                <input type="text" class="form-control text-center"
                                                       name="segundos_timbre"
                                                       id="segundos_timbre"
                                                       value="{{ $extension_nueva->segundos_timbre }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.13') }}</td>
                                            <td class="text-center">
                                                <input type="text" class="form-control text-center"
                                                       name="segundos_timbre_en_ocupado"
                                                       id="segundos_timbre_en_ocupado"
                                                       value="{{ $extension_nueva->segundos_timbre_en_ocupado }}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.14') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="videosupport"
                                                        id="videosupport"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    <option value="yes"
                                                            @if($extension_nueva->videosupport == 'yes') selected="selected" @endif>
                                                        Si
                                                    </option>
                                                    <option value="no"
                                                            @if($extension_nueva->videosupport == 'no') selected="selected" @endif>
                                                        No
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.nasvox_gui.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.1') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox"
                                                           name="permitir_modificar_email_llamada_perdida"
                                                           @if($extension_nueva->permitir_modificar_email_llamada_perdida) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.2') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox"
                                                           name="puede_modificar_directorio_departamental"
                                                           @if($extension_nueva->puede_modificar_directorio_departamental) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.3') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox"
                                                           name="mostrar_extension_en_directorio"
                                                           @if($extension_nueva->mostrar_extension_en_directorio) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.4') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="nasvox_gui_activo"
                                                           @if($extension_nueva->nasvox_gui_activo) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.nasvox_gui.5') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_administrar_bloqueos"
                                                           @if($extension_nueva->puede_administrar_bloqueos) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.6') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_administrar_lista_blanca"
                                                           @if($extension_nueva->puede_administrar_lista_blanca) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.7') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_editar_display_name"
                                                           @if($extension_nueva->puede_editar_display_name) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.8') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_editar_pincode"
                                                           @if($extension_nueva->puede_editar_pincode) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.9') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_editar_email"
                                                           @if($extension_nueva->puede_editar_email) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.permisos_extension.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ __('page.extension.permisos_extension.1') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="permitir_fwd"
                                                           @if($extension_nueva->permitir_fwd) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.permisos_extension.2') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox"
                                                           name="permitir_levantamientoextension"
                                                           @if($extension_nueva->permitir_levantamientoextension) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.permisos_extension.3') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="permitir_levantarextension"
                                                           @if($extension_nueva->permitir_levantarextension) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.permisos_extension.4') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="permitir_hacerpage"
                                                           @if($extension_nueva->permitir_hacerpage) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.permisos_extension.5') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="permitir_recibirpage"
                                                           @if($extension_nueva->permitir_recibirpage) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.permisos_extension.6') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_activar_desactivar_politica"
                                                           @if($extension_nueva->puede_activar_desactivar_politica) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.avanzado.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ __('page.extension.avanzado.1') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="encryption"
                                                        id="encryption"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    <option value="yes"
                                                            @if($extension_nueva->encryption == 'yes') selected="selected" @endif>
                                                        Si
                                                    </option>
                                                    <option value="no"
                                                            @if($extension_nueva->encryption == 'no') selected="selected" @endif>
                                                        No
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.avanzado.2') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="grabar_llamadas"
                                                           @if($extension_nueva->grabar_llamadas) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.avanzado.3') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="tarificador_activo"
                                                           @if($extension_nueva->tarificador_activo) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.avanzado.4') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="nunca_grabar"
                                                           @if($extension_nueva->nunca_grabar) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.avanzado.5') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="permitir_fax"
                                                           @if($extension_nueva->permitir_fax) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.avanzado.6') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="readonly"
                                                           @if($extension_nueva->readonly) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.contact_center.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ __('page.extension.contact_center.1') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="nasvox_gui_queue"
                                                        id="nasvox_gui_queue"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    @foreach($ccc_queues as $ccc_queue)
                                                        <option value="{{ $ccc_queue->id_queue }}">{{ $ccc_queue->queue }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.contact_center.2') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="nasvox_gui_es_agente"
                                                           @if($extension_nueva->nasvox_gui_es_agente) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.contact_center.3') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="activar_desactivar_politica_en_cola"
                                                           @if($extension_nueva->activar_desactivar_politica_en_cola) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.opciones_de_red.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ __('page.extension.opciones_de_red.1') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="allow[]" id="allow"
                                                        multiple
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    <option value="h263"
                                                            @if(in_array('h263', explode(';', $extension_nueva->allow))) selected="selected" @endif>
                                                        h263
                                                    </option>
                                                    <option value="h264"
                                                            @if(in_array('h264', explode(';', $extension_nueva->allow))) selected="selected" @endif>
                                                        h264
                                                    </option>
                                                    <option value="g722"
                                                            @if(in_array('g722', explode(';', $extension_nueva->allow))) selected="selected" @endif>
                                                        g722
                                                    </option>
                                                    <option value="ulaw"
                                                            @if(in_array('ulaw', explode(';', $extension_nueva->allow))) selected="selected" @endif>
                                                        ulaw
                                                    </option>
                                                    <option value="alaw"
                                                            @if(in_array('alaw', explode(';', $extension_nueva->allow))) selected="selected" @endif>
                                                        alaw
                                                    </option>
                                                    <option value="g729"
                                                            @if(in_array('g729', explode(';', $extension_nueva->allow))) selected="selected" @endif>
                                                        g729
                                                    </option>
                                                    <option value="gsm"
                                                            @if(in_array('gsm', explode(';', $extension_nueva->allow))) selected="selected" @endif>
                                                        gsm
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opciones_de_red.2') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="nat" id="nat"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    <option value="yes"
                                                            @if($extension_nueva->nat == 'yes') selected="selected" @endif>
                                                        Si
                                                    </option>
                                                    <option value="no"
                                                            @if($extension_nueva->nat == 'no') selected="selected" @endif>
                                                        No
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.opciones_de_red.3') }}</td>
                                            <td width="30%" class="text-center">
                                                <select class="form-control select2" name="vlan" id="vlan"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    @foreach($vlans as $vlan)
                                                        <option value="{{ $vlan->vlan }}"
                                                                @if($extension_nueva->vlan) selected="selected" @endif>{{ $vlan->descripcion }}
                                                            : {{ $vlan->ip }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opciones_de_red.4') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="transport"
                                                        id="transport"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    <option value="udp"
                                                            @if($extension_nueva->transport == 'udp') selected="selected" @endif>
                                                        UDP
                                                    </option>
                                                    <option value="tls"
                                                            @if($extension_nueva->transport == 'tls') selected="selected" @endif>
                                                        TLS
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <!-- END FORM-->
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('pagelevelplugins')
    <script src="{{ asset('/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
            type="text/javascript"></script>
@endsection

@section('pagelevelscripts')
    <script src="{{ asset('pages/scripts/table-datatables-editable.min.js') }}" type="text/javascript"></script>
@endsection

@section('customjs')
    <script>
        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }

        function fnConvertMacAddress(string, chr, nth) {
            var output = '';
            for (var i = 0; i < string.length; i++) {
                if (i < 0 && i % nth == 0)
                    output += chr;
                output += string.charAt(i);
            }

            return output;
        }

        function generateMacAddress() {
            var macAddressStructure = /^[A-Fa-f0-9]{1,2}\:[A-Fa-f0-9]{1,2}\:[A-Fa-f0-9]{1,2}\:[A-Fa-f0-9]{1,2}\:[A-Fa-f0-9]{1,2}\:[A-Fa-f0-9]{1,2}$/;
            var extName = $('#ext_name');
            var extMacAddress = $('#ext_mac_address');

            if (extName.val().length == 0) {
                extMacAddress.val('00:00:00:00:00:00');
            } else {
                //Removing all letters from extention.
                extName.val(extName.val().replace(/\D/g,''));
                
                //Adding ceros ahead of extension number.
                var macAddress = pad(extName.val(), 12);

                //Adding : every 2 places.
                macAddress = macAddress.match(/.{1,2}/g).join(':');
                extMacAddress.val(macAddress);
            }
        }

        function loadMedidaRespaldo(element) {
            var medida_respaldo = element.find(':selected').val();

            $.get('{{ asset('extension_nueva') }}' + '/' + medida_respaldo + '/medida_respaldo')
                .done(function (data) {
                    $('#contenedor_medida_respaldo_llamada_dato').empty();
                    $('#contenedor_medida_perdida_llamada').empty();
                    $('#contenedor_medida_perdida_llamada_dato').empty();

                    //console.log(data.tipo_input);
                    //console.log(data.bloqueado);
                    //console.log(data.dato_obligatorio);

                    switch (data.tipo_input) {
                        case 'select':
                            var select_respaldo = $('<select>')
                                .prop('name', 'medida_respaldo_llamada_dato')
                                .prop('class', 'form-control')
                                .appendTo('#contenedor_medida_respaldo_llamada_dato');

                            select_respaldo.append($("<option>").attr('value', '').text(''));

                            $(data.opciones_select_medida_respaldo_dato).each(function (index, element) {
                                select_respaldo.append($("<option>").attr('value', element[Object.keys(element)[0]]).text(element[Object.keys(element)[1]]));
                            });

                            select_respaldo.select2({
                                placeholder: 'Escoja Medida Respaldo'
                            });
                            break;
                        default:
                            var input_medida_respaldo_dato = $('<input>').prop('class', 'form-control')
                                .prop('name', 'medida_respaldo_llamada_dato')
                                .prop('placeholder', 'Medida Respaldo')
                                .appendTo('#contenedor_medida_respaldo_llamada_dato');

                            if (data.bloqueado) {
                                input_medida_respaldo_dato.prop('readonly', true);
                            }
                            break;
                    }

                    if (data.opciones_select_medida_perdida.length > 0) {
                        var select_perdida = $('<select>')
                            .prop('id', 'select_medida_perdida_llamada')
                            .prop('name', 'contenedor_medida_perdida_llamada')
                            .prop('class', 'form-control')
                            .prop('placeholder', 'Escoja Medida Perdida')
                            .appendTo('#contenedor_medida_perdida_llamada');

                        select_perdida.append($("<option>").attr('value', '').text(''));

                        $(data.opciones_select_medida_perdida).each(function (index, element) {
                            select_perdida.append($("<option>").attr('value', element[Object.keys(element)[0]]).text(element[Object.keys(element)[1]]));
                        });

                        select_perdida.select2({
                            placeholder: 'Escoja Medida Perdida'
                        });
                    } else {
                        var input_medida_perdida = $('<input>').prop('class', 'form-control')
                            .prop('name', 'medida_perdida_llamada')
                            .prop('placeholder', 'Medida Perdida')
                            .prop('readonly', true)
                            .appendTo('#contenedor_medida_perdida_llamada');
                    }

                    var input_medida_perdida_dato = $('<input>').prop('class', 'form-control')
                        .prop('name', 'medida_perdida_llamada_dato')
                        .prop('placeholder', 'Medida Perdida')
                        .prop('readonly', true)
                        .appendTo('#contenedor_medida_perdida_llamada_dato');
                });
        }

        $(document).on('change', '#select_medida_perdida_llamada', function () {
            var medida_respaldo = $('#medida_respaldo_llamada').find(':selected').val();
            var medida_perdida = $('#select_medida_perdida_llamada').find(':selected').val();

            console.log(medida_respaldo);
            console.log(medida_perdida);

            $.get('{{ asset('extension_nueva') }}' + '/' + medida_respaldo + '/' + medida_perdida + '/medida_perdida')
                .done(function (data) {
                    $('#contenedor_medida_perdida_llamada_dato').empty();

                    switch (data.tipo_input) {
                        case 'select':
                            var select_perdida_dato = $('<select>')
                                .prop('name', 'medida_respaldo_llamada_dato')
                                .prop('class', 'form-control')
                                .appendTo('#contenedor_medida_perdida_llamada_dato');

                            select_perdida_dato.append($("<option>").attr('value', '').text(''));

                            $(data.opciones_select_medida_perdida_dato).each(function (index, element) {
                                select_perdida_dato.append($("<option>").attr('value', element[Object.keys(element)[0]]).text(element[Object.keys(element)[1]]));
                            });

                            select_perdida_dato.select2({
                                placeholder: 'Escoja Medida Respaldo'
                            });
                            break;
                        default:
                            var input_medida_perdida_dato = $('<input>').prop('class', 'form-control')
                                .prop('name', 'medida_perdida_llamada_dato')
                                .prop('placeholder', '{{ __('page.extension.campo.medida_perdida') }}')
                                .appendTo('#contenedor_medida_perdida_llamada_dato');

                            if (data.bloqueado) {
                                input_medida_perdida_dato.prop('readonly', true);
                            }
                            break;
                    }

                });
        });

        //        $('#ext_mac_address').keyup(function (e) {
        //            var r = /([a-f0-9]{2})/i;
        //            var str = e.target.value.replace(/[^a-f0-9:]/ig, "");
        //            if (e.keyCode != 8 && r.test(str.slice(-2))) {
        //                str = str.concat(':')
        //            }
        //            e.target.value = str.slice(0, 17);
        //        });

        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>

    @stack('scripts')
@endsection