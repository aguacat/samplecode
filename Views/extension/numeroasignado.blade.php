<div class="row">
    <div class="col-md-4">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-wrench font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.izquierdo.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.numero_asignado.bloque.izquierdo.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <form method="post">

                    <div id="numeroasignado-errors-block" class="form-group hidden">
                        <div class="alert alert-danger">
                            <ul id="numeroasignado-form-errors">
                            </ul>
                        </div>
                    </div>

                    <div id="accesoForm" class="form-body">
                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.numero_asignado.campo.numero') }}
                            </label>
                            <div class="input-icon right">
                                <input type="text" class="form-control" id="numeroasignado_ext_destino" name="ext_destino"
                                       value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.numero_asignado.campo.descripcion') }}
                            </label>
                            <div class="input-icon right">
                                <input type="text" class="form-control" id="numeroasignado_descripcion" name="descripcion"
                                       value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div id="numeroasignado_action_section" class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button"
                                        class="btn blue"
                                        onclick="numeroAsignadoPrepareForm(requestType.POST)">
                                    {{ __('page.boton.guardar') }}
                                </button>
                                <button type="reset" class="btn default">
                                    {{ __('page.boton.cancel') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-list font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.derecho.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.numero_asignado.bloque.derecho.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-hover table-light">
                    <thead>
                    <tr class="uppercase">
                        <th width="30%">
                            {{ __('page.extension.numero_asignado.tabla.campo.numero') }}
                        </th>
                        <th width="70%">
                            {{ __('page.extension.numero_asignado.tabla.campo.descripcion') }}
                        </th>
                    </tr>
                    </thead>
                    <tbody id="numeroAsignado">
                    @forelse($extensiones_acceso as $extension_acceso)
                        <tr id="{{ $extension_acceso->id }}" onclick="numeroAsignadoSelectData($(this))">
                            <td>{{ $extension_acceso->ext_destino }}</td>
                            <td>{{ $extension_acceso->descripcion }}</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">
                                <h4 class="text-center">
                                    {{ __('page.extension.numero_asignado.tabla.empty') }}
                                </h4>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function numeroAsignadoRefreshTable() {
            $('#numeroAsignado').empty();

            var extension_actual_name = $('#extension_actual_name').html();

            $.get('{{ asset('/extension_opcion_data') }}' + "/numeroasignado/" + extension_actual_name, function (data) {
                if (data.length > 0) {
                    $.each(data, function (index, value) {
                        var columna1 = '<td>' + value.ext_destino + '</td>';
                        var columna2 = '<td>' + value.descripcion + '</td>';
                        var fila = '<tr id="' + value.id + '" onclick="numeroAsignadoSelectData($(this))">' + columna1 + columna2 + '</tr>';

                        $('#numeroAsignado').append(fila);
                    });
                }

                if ($('#numeroAsignado tr').length == 0) {
                    $('#numeroAsignado').append('<tr><td colspan="2"><h4 class="text-center">{{ __('page.extension.numero_asignado.tabla.empty') }}</h4></td></tr>');
                }
            })
                .fail(function () {
                    alert("Ha ocurrido un error inesperado al cargar la data.");
                })
        }

        function numeroAsignadoResetAction() {
            $('#numeroasignado_ext_destino').val('');
            $('#numeroasignado_descripcion').val('');
            $('#numeroAsignado').find('tr').removeAttr('style');
            numeroAsignadoActionBlock('create');
        }

        function numeroAsignadoActionBlock(status) {
            $('#numeroasignado_action_section').empty();

            if (status == 'update_delete') {
                var btnModificar = '<button id="patchForm" type="button" class="btn blue" onclick="numeroAsignadoPrepareForm(requestType.PUT)">{{ __('page.boton.modificar') }}</button>';
                var btnEliminar = '<button id="destroyForm" type="button" class="btn red pull-right" onclick="numeroAsignadoPrepareForm(requestType.DELETE)">{{ __('page.boton.eliminar') }}</button>';
                var btnCancelar = '<a class="btn default" onclick="accesoResetAction()">{{ __('page.boton.cancel') }}</a>';
                var divAccion = '<div class="col-md-8 pull-left">' + btnModificar + '&nbsp;' + btnCancelar + '</div><div class="col-md-4 pull-right">' + btnEliminar + '</div>';

                $('#numeroasignado_action_section').append(divAccion);

            } else {
                var btnGuardar = '<button id="postForm" type="button" class="btn blue" onclick="numeroAsignadoPrepareForm(requestType.POST)">{{ __('page.boton.guardar') }}</button> ';
                var btnCancelar = '<button type="reset" class="btn default">{{ __('page.boton.cancel') }}</button>';
                var divAccion = '<div class="col-md-offset-3 col-md-9">' + btnGuardar + btnCancelar + '</div>';

                $('#numeroasignado_action_section').append(divAccion);
            }
        }

        function numeroAsignadoPrepareForm(currentRequestType) {
            $('#numeroasignado-errors-block').addClass('hidden');

            var extension_actual_name = $('#extension_actual_name').html();
            var numero = $('#numeroasignado_ext_destino').val();
            var descripcion = $('#numeroasignado_descripcion').val();

            switch (currentRequestType) {
                case requestType.POST:
                    $.post('{{ asset('extension_opcion/numeroasignado') }}',
                        {
                            _token: "{{ csrf_token() }}",
                            name: extension_actual_name,
                            numero: numero,
                            descripcion: descripcion
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#numeroasignado-errors-block').removeClass('hidden');
                            $('#numeroasignado-form-errors').empty();

                            for (var error in errors) {
                                $('#numeroasignado-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            numeroAsignadoRefreshTable();
                            numeroAsignadoResetAction();
                        });
                    break;
                case requestType.PUT:
                    $.put('{{ asset('extension_opcion/numeroasignado') }}/' + $('.numeroasignado_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            name: extension_actual_name,
                            numero: numero,
                            descripcion: descripcion
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#numeroasignado-errors-block').removeClass('hidden');
                            $('#numeroasignado-form-errors').empty();

                            for (var error in errors) {
                                $('#numeroasignado-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            numeroAsignadoRefreshTable();
                            numeroAsignadoResetAction();
                        });
                    break;
                case requestType.DELETE:
                    $.delete('{{ asset('extension_opcion/numeroasignado') }}/' + $('.numeroasignado_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            _method: "DELETE"
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#numeroasignado-errors-block').removeClass('hidden');
                            $('#numeroasignado-form-errors').empty();

                            for (var error in errors) {
                                $('#numeroasignado-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            numeroAsignadoRefreshTable();
                            numeroAsignadoResetAction();
                        });
                    break;
                default:
                    console.log('Nope... Failed.');
                    //Invalid Request.
                    break;
            }
        }

        function numeroAsignadoSelectData(element) {
            $('#accesos').find('tr').removeAttr('style');

            element.addClass('numeroasignado_selected_row');
            element.css('border-left', '0.3em solid #3598dc');
            element.css('border-right', '0.3em solid #3598dc');

            $.get('{{ asset('extension_opcion') }}/numeroasignado/' + element.prop('id'))
                .done(function (data) {
                    $('#numeroasignado_ext_destino').val(data.field1);
                    $('#numeroasignado_descripcion').val(data.field2);

                    numeroAsignadoActionBlock('update_delete');
                })
        }
    </script>
@endpush
