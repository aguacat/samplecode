<div class="row">
    <div class="col-md-4">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-wrench font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.izquierdo.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.rutas.bloque.izquierdo.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <form method="post">

                    {{ csrf_field() }}

                    <div id="rutas-errors-block" class="form-group hidden">
                        <div class="alert alert-danger">
                            <ul id="rutas-form-errors">
                            </ul>
                        </div>
                    </div>

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.rutas.campo.ruta') }}
                            </label>
                            <div class="input-icon right">
                                <select class="form-control select2"
                                        data-placeholder="{{ __('page.extension.rutas.placeholder.ruta') }}"
                                        style="width: 100%"
                                        id="rutas-select">
                                    <option></option>
                                    @foreach($rutas as $ruta)
                                        <option value="{{ $ruta->id_ruta }}">{{ $ruta->ruta . '|' . $ruta->descripcion }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.rutas.campo.canal') }}
                            </label>
                            <div class="input-icon right">
                                <select class="form-control select2"
                                        data-placeholder="{{ __('page.extension.rutas.placeholder.canal') }}"
                                        style="width: 100%"
                                        id="canal-select">
                                    <option></option>
                                    @foreach($canales as $canal)
                                        <option value="{{ $canal->canal }}">{{ $canal->canal }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="mt-checkbox" style="position: relative">
                                <input type="checkbox" id="rc-checkbox" checked="checked">
                                {{ __('page.extension.rutas.campo.requiere_contrasena') }}
                                <span></span>
                            </label>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div id="rutas_action_section" class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button"
                                        class="btn blue"
                                        onclick="rutasPrepareForm(requestType.POST)">
                                    {{ __('page.boton.guardar') }}
                                </button>
                                <button type="reset" class="btn default">
                                    {{ __('page.boton.cancel') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-list font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.izquierdo.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.rutas.bloque.derecho.subtitulo') }}
                    </span>
                </div>

                <div class="pull-right">
                    <button class="btn btn-info btn-sm" onclick="toogleRequiereContrasenaAll(true)">
                        {{ __('page.extension.rutas.boton.marcar_requiere_contrasena') }}
                    </button>
                    <button class="btn btn-danger btn-sm" onclick="toogleRequiereContrasenaAll(false)">
                        {{ __('page.extension.rutas.boton.marcar_no_requiere_contrasena') }}
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-hover table-light">
                    <thead>
                    <tr class="uppercase">
                        <th width="30%">
                            {{ __('page.extension.rutas.tabla.campo.canal') }}
                        </th>
                        <th width="60%">
                            {{ __('page.extension.rutas.tabla.campo.descripcion') }}
                        </th>
                        <th width="10%">
                            {{ __('page.extension.rutas.tabla.campo.requiere_contrasena') }}
                        </th>
                    </tr>
                    </thead>
                    <tbody id="tablarutas">
                    @forelse($extensiones_rutas as $extension_ruta)
                        <tr id="{{ $extension_ruta->id_ext_ruta }}" onclick="rutasSelectData($(this))">
                            <td>{{ $extension_ruta->canal }}</td>
                            <td>{{ $extension_ruta->ruta->descripcion }}</td>
                            <td>
                                <label class="mt-checkbox mt-checkbox-outline">
                                    <input type="checkbox"
                                           @if($extension_ruta->requiere_contrasena) checked="checked" @endif
                                           disabled>
                                    <span></span>
                                </label>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="2">
                                <h4 class="text-center">
                                    {{ __('page.extension.rutas.tabla.empty') }}
                                </h4>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function rutasRefreshTable() {
            $('#tablarutas').empty();

            var extension_actual_name = $('#extension_actual_name').html();

            $.get('{{ asset('/extension_opcion_data') }}' + "/ruta/" + extension_actual_name, function (data) {
                if (data.length > 0) {
                    $.each(data, function (index, value) {

                        if (value.requiere_contrasena) {
                            var checked = 'checked="checked"';
                        } else {
                            var checked = '';
                        }

                        var columna1 = '<td>' + value.canal + '</td>';
                        var columna2 = '<td>' + value.ruta.descripcion + '</td>';
                        var columna3 = '<td><label class="mt-checkbox mt-checkbox-outline" style="position: relative"><input type="checkbox" id="rc-checkbox" ' + checked + ' disabled"><span></span></label></td>';
                        var fila = '<tr id="' + value.id_ext_ruta + '" onclick="rutasSelectData($(this))">' + columna1 + columna2 + columna3 + '</tr>';

                        $('#tablarutas').append(fila);
                    });
                }

                if ($('#tablarutas tr').length == 0) {
                    $('#tablarutas').append('<tr><td colspan="2"><h4 class="text-center">{{ __('page.extension.rutas.tabla.empty') }}</h4></td></tr>');
                }
            }).fail(function () {
                alert("{{ __('page.extension.alert.error') }}");
            })
        }

        function rutasResetAcceso() {
            $('#rutas-select').val('').trigger('change');
            $('#canal-select').val('').trigger('change');
            $('#rc-checkbox').prop('checked', true);
            $('#tablarutas').find('tr').removeAttr('style');
            rutasActionBlock('create');
        }

        function rutasActionBlock(status) {
            $('#rutas_action_section').empty();

            if (status == 'update_delete') {
                var btnModificar = '<button id="patchForm" type="button" class="btn blue" onclick="rutasPrepareForm(requestType.PUT)">{{ __('page.boton.modificar') }}</button>';
                var btnEliminar = '<button id="destroyForm" type="button" class="btn red pull-right" onclick="rutasPrepareForm(requestType.DELETE)">{{ __('page.boton.eliminar') }}</button>';
                var btnCancelar = '<a class="btn default" onclick="rutasResetAcceso()">{{ __('page.boton.cancel')}}</a>';
                var divAccion = '<div class="col-md-8 pull-left">' + btnModificar + '&nbsp;' + btnCancelar + '</div><div class="col-md-4 pull-right">' + btnEliminar + '</div>';

                $('#rutas_action_section').append(divAccion);

            } else {
                var btnGuardar = '<button id="postForm" type="button" class="btn blue" onclick="rutasPrepareForm(requestType.POST)">{{ __('page.boton.guardar') }}</button> ';
                var btnCancelar = '<button type="reset" class="btn default">{{ __('page.boton.cancel')}}</button>';
                var divAccion = '<div class="col-md-offset-3 col-md-9">' + btnGuardar + btnCancelar + '</div>';

                $('#rutas_action_section').append(divAccion);
            }
        }

        function rutasPrepareForm(currentRequestType) {
            $('#rutas-errors-block').addClass('hidden');

            var extension_actual_name = $('#extension_actual_name').html();
            var ruta = $('#rutas-select').val();
            var canal = $('#canal-select').val();
            var requiere_contrasena = $('#rc-checkbox').prop('checked');

            var numero = $('#rutas_ext_destino').val();
            var descripcion = $('#rutas_descripcion').val();

            switch (currentRequestType) {
                case requestType.POST:
                    $.post('{{ asset('extension_opcion/ruta') }}',
                        {
                            _token: "{{ csrf_token() }}",
                            name: extension_actual_name,
                            ruta: ruta,
                            canal: canal,
                            requiere_contrasena: requiere_contrasena
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#rutas-errors-block').removeClass('hidden');
                            $('#rutas-form-errors').empty();

                            for (var error in errors) {
                                $('#rutas-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            rutasRefreshTable();
                            rutasResetAcceso();
                        });
                    break;
                case requestType.PUT:
                    $.put('{{ asset('extension_opcion/ruta') }}/' + $('.rutas_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            name: extension_actual_name,
                            ruta: ruta,
                            canal: canal,
                            requiere_contrasena: requiere_contrasena
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#rutas-errors-block').removeClass('hidden');
                            $('#rutas-form-errors').empty();

                            for (var error in errors) {
                                $('#rutas-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            rutasRefreshTable();
                            rutasResetAcceso();
                        });
                    break;
                case requestType.DELETE:
                    $.delete('{{ asset('extension_opcion/ruta') }}/' + $('.rutas_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            _method: "DELETE"
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#rutas-errors-block').removeClass('hidden');
                            $('#rutas-form-errors').empty();

                            for (var error in errors) {
                                $('#rutas-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            rutasRefreshTable();
                            rutasResetAcceso();
                        });
                    break;
                default:
                    console.log('Nope... Failed.');
                    //Invalid Request.
                    break;
            }
        }

        function rutasSelectData(element) {
            $('#tablarutas').find('tr').removeAttr('style');

            element.addClass('rutas_selected_row');
            element.css('border-left', '0.3em solid #3598dc');
            element.css('border-right', '0.3em solid #3598dc');

            $.get('{{ asset('extension_opcion') }}/ruta/' + element.prop('id'))
                .done(function (data) {
                    $('#rutas-select').val(data.id_ruta).trigger('change');
                    $('#canal-select').val(data.canal).trigger('change');

                    if (data.requiere_contrasena) {
                        $('#rc-checkbox').prop('checked', true);
                    } else {
                        $('#rc-checkbox').prop('checked', false);
                    }

                    rutasActionBlock('update_delete');
                })
        }

        function toogleRequiereContrasenaAll(status) {
            var extension_actual_name = $('#extension_actual_name').html();

            $.post('{{ asset('extension_rutas/') }}/' + status,
                {
                    _token: "{{ csrf_token() }}",
                    name: extension_actual_name,
                })
                .fail(function (xhr, status, error) {
                    var errors = JSON.parse(xhr.responseText);

                    $('#rutas-errors-block').removeClass('hidden');
                    $('#rutas-form-errors').empty();

                    for (var error in errors) {
                        $('#rutas-form-errors').append('<li>' + errors[error] + '</li>');
                    }
                })
                .done(function () {
                    rutasRefreshTable();
                    rutasResetAcceso();
                });
        }
    </script>
@endpush