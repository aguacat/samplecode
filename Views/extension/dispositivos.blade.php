<div class="row">
    <div class="col-md-4">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-wrench font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.izquierdo.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.dispositivos.bloque.izquierdo.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <form method="post">

                    {{ csrf_field() }}

                    <div id="dispositivo-errors-block" class="form-group hidden">
                        <div class="alert alert-danger">
                            <ul id="dispositivo-form-errors">
                            </ul>
                        </div>
                    </div>

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.dispositivos.campo.mac_address') }}
                            </label>
                            <div class="input-icon right">
                                <input type="text" class="form-control" id="mac_address" value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div id="dispositivo_action_section" class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button"
                                        class="btn blue"
                                        onclick="dispositivoPrepareForm(requestType.POST)">
                                    {{ __('page.boton.guardar') }}
                                </button>
                                <button type="reset" class="btn default">
                                    {{ __("page.boton.cancel") }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-list font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.derecho.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.dispositivos.bloque.derecho.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                    <table class="table table-hover table-light">
                        <thead>
                        <tr class="uppercase">
                            <th width="30%">
                                {{ __('page.extension.dispositivos.tabla.campo.mac_address') }}
                            </th>
                            <th width="70%">
                                {{ __('page.extension.dispositivos.tabla.campo.ultima_actualizacion') }}
                            </th>
                        </tr>
                        </thead>
                        <tbody id="tabladispositivos">
                        @forelse($extensiones_dispositivo as $extension_dispositivo)
                            <tr id="{{ $extension_dispositivo->id_dispositivo }}"
                                onclick="dispositivoSelectData($(this))">
                                <td>{{ $extension_dispositivo->mac_address }}</td>
                                <td>{{ $extension_dispositivo->tiempo }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2">
                                    <h4 class="text-center">
                                        {{ __('page.extension.dispositivos.tabla.empty') }}
                                    </h4>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function dispositivoRefreshTable() {
            $('#tabladispositivos').empty();

            var extension_actual_name = $('#extension_actual_name').html();

            $.get('{{ asset('/extension_opcion_data') }}' + "/dispositivo/" + extension_actual_name, function (data) {
                if (data.length > 0) {
                    $.each(data, function (index, value) {
                        var columna1 = '<td>' + value.mac_address + '</td>';
                        var columna2 = '<td>' + value.tiempo + '</td>';
                        var fila = '<tr id="' + value.id_dispositivo + '" onclick="dispositivoSelectData($(this))">' + columna1 + columna2 + '</tr>';

                        $('#tabladispositivos').append(fila);
                    });
                }

                if ($('#tabladispositivos tr').length == 0) {
                    $('#tabladispositivos').append('<tr><td colspan="2"><h4>{{ __('page.extension.dispositivos.tabla.empty') }}</h4></td></tr>');
                }
            })
                .fail(function () {
                    alert("{{ __('page.extension.alert.error') }}");
                })
        }

        function dispositivoResetAction() {
            $('#mac_address').val('');
            $('#tabladispositivos').find('tr').removeAttr('style');
            dispositivoActionBlock('create');
        }

        function dispositivoActionBlock(status) {
            $('#dispositivo_action_section').empty();

            if (status == 'update_delete') {
                var btnModificar = '<button id="patchForm" type="button" class="btn blue" onclick="dispositivoPrepareForm(requestType.PUT)">{{ __('page.boton.modificar') }}</button>';
                var btnEliminar = '<button id="destroyForm" type="button" class="btn red pull-right" onclick="dispositivoPrepareForm(requestType.DELETE)">{{ __('page.boton.eliminar') }}</button>';
                var btnCancelar = '<a class="btn default" onclick="dispositivoResetAction()">{{ __('page.boton.cancel') }}</a>';
                var divAccion = '<div class="col-md-8 pull-left">' + btnModificar + '&nbsp;' + btnCancelar + '</div><div class="col-md-4 pull-right">' + btnEliminar + '</div>';

                $('#dispositivo_action_section').append(divAccion);

            } else {
                var btnGuardar = '<button id="postForm" type="button" class="btn blue" onclick="dispositivoPrepareForm(requestType.POST)">{{ __('page.boton.guardar') }}</button> ';
                var btnCancelar = '<button type="reset" class="btn default">{{ __('page.boton.cancel') }}</button>';
                var divAccion = '<div class="col-md-offset-3 col-md-9">' + btnGuardar + btnCancelar + '</div>';

                $('#dispositivo_action_section').append(divAccion);
            }
        }

        function dispositivoPrepareForm(currentRequestType) {
            $('#dispositivo-errors-block').addClass('hidden');

            var extension_actual_name = $('#extension_actual_name').html();
            var mac_address = $('#mac_address').val();
            var dispositivo_modelo = $('#id_dispositivo_modelo').val();
            var protocolo = $('#id_ext_protocolo').val();

            switch (currentRequestType) {
                case requestType.POST:
                    $.post('{{ asset('extension_opcion/dispositivo') }}',
                        {
                            _token: "{{ csrf_token() }}",
                            name: extension_actual_name,
                            mac_address: mac_address,
                            dispositivo_modelo: dispositivo_modelo,
                            protocolo: protocolo
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#dispositivo-errors-block').removeClass('hidden');
                            $('#dispositivo-form-errors').empty();

                            for (var error in errors) {
                                $('#dispositivo-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            dispositivoRefreshTable();
                            dispositivoResetAction();
                        });
                    break;
                case requestType.PUT:
                    $.put('{{ asset('extension_opcion/dispositivo') }}/' + $('.dispositivo_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            name: extension_actual_name,
                            mac_address: mac_address,
                            dispositivo_modelo: dispositivo_modelo,
                            protocolo: protocolo
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#dispositivo-errors-block').removeClass('hidden');
                            $('#dispositivo-form-errors').empty();

                            for (var error in errors) {
                                $('#dispositivo-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            dispositivoRefreshTable();
                            dispositivoResetAction();
                        });
                    break;
                case requestType.DELETE:
                    $.delete('{{ asset('extension_opcion/dispositivo') }}/' + $('.dispositivo_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            _method: "DELETE"
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#dispositivo-errors-block').removeClass('hidden');
                            $('#dispositivo-form-errors').empty();

                            for (var error in errors) {
                                $('#dispositivo-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            dispositivoRefreshTable();
                            dispositivoResetAction();
                        });
                    break;
                default:
                    console.log('Nope... Failed.');
                    //Invalid Request.
                    break;
            }
        }

        function dispositivoSelectData(element) {
            $('#tabladispositivos').find('tr').removeAttr('style');

            element.addClass('dispositivo_selected_row');
            element.css('border-left', '0.3em solid #3598dc');
            element.css('border-right', '0.3em solid #3598dc');

            $.get('{{ asset('extension_opcion') }}/dispositivo/' + element.prop('id'))
                .done(function (data) {
                    $('#mac_address').val(data.mac_address);

                    dispositivoActionBlock('update_delete');
                })
        }
    </script>
@endpush