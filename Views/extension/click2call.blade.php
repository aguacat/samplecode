<div class="row">
    <div class="col-md-12">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-wrench font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.izquierdo.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.click2call.bloque.izquierdo.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <form method="post" class="horizontal-form">

                        {{ csrf_field() }}

                        <div class="col-md-6">
                            <div id="click2call-errors-block" class="form-group hidden">
                                <div class="alert alert-danger">
                                    <ul id="click2call-form-errors">
                                    </ul>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label">
                                        {{ __('page.extension.click2call.campo.ip_permitido') }}
                                    </label>
                                    <div class="input-icon right">
                                        <input type="text" class="form-control" id="c2c_ipaddress"
                                               value="{{ $click2call->c2c_ipaddress }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">
                                        {{ __('page.extension.click2call.campo.contrasena') }}
                                    </label>
                                    <div class="input-icon right">
                                        <input type="password" class="form-control" id="c2c_contrasena"
                                               value="{{ $click2call->c2c_contrasena }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">
                                        {{ __('page.extension.click2call.campo.prefijo') }}
                                    </label>
                                    <div class="input-icon right">
                                        <input type="text" class="form-control" id="c2c_prefix"
                                               value="{{ $click2call->c2c_prefix }}">
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="button" class="btn blue"
                                                    onclick="click2callPrepareForm(requestType.PUT)">
                                                {{ __('page.boton.guardar') }}
                                            </button>
                                            <button type="reset" class="btn default">
                                                {{ __('page.boton.cancel') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div id="click2call-test-call-errors-block" class="form-group hidden">
                                <div class="alert alert-danger">
                                    <ul id="click2call-test-call-form-errors">
                                    </ul>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">
                                    {{ __('page.extension.click2call.campo.numero_a_llamar') }}
                                </label>
                                <div class="input-icon right">
                                    <input type="text" class="form-control" id="numero_a_llamar" value="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="mt-checkbox" style="position: relative">
                                    <input type="checkbox" id="c2c_activo"
                                           @if($click2call->c2c_activo) checked="checked" @endif>
                                    {{ __('page.extension.click2call.campo.ip_permitido') }}
                                    <span></span>
                                </label>
                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" class="btn blue" onclick="testCall(requestType.PUT)">
                                            {{ __('page.extension.click2call.boton.probar') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function click2callPrepareForm(currentRequestType) {
            $('#backup-errors-block').addClass('hidden');

            var url = $(location).attr('href').split("/");
            var c2c_ipaddress = $('#c2c_ipaddress').val();
            var c2c_contrasena = $('#c2c_contrasena').val();
            var c2c_prefix = $('#c2c_prefix').val();
            var c2c_activo = $('#c2c_activo').val();
            var extension_actual_name = $('#extension_actual_name').html();

            switch (currentRequestType) {
                case requestType.PUT:
                    $.put('{{ asset('extension_opcion/click2call') }}/' + url[4],
                        {
                            _token: "{{ csrf_token() }}",
                            id_ext: url[4],
                            c2c_ipaddress: c2c_ipaddress,
                            c2c_contrasena: c2c_contrasena,
                            c2c_prefix: c2c_prefix,
                            c2c_activo: c2c_activo
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#click2call-errors-block').removeClass('hidden');
                            $('#click2call-form-errors').empty();

                            for (var error in errors) {
                                $('#click2call-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        });
                    break;
                default:
                    console.log('Nope... Failed.');
                    //Invalid Request.
                    break;
            }
        }

        function testCall() {
            var phone = $('#numero_a_llamar').val();
            var ext = $('#extension_actual_name').html();
            var pass = $('#c2c_contrasena').val();

            if (phone.length <= 0 || ext.length <= 0 || pass.length <= 0) {
                $('#click2call-test-call-errors-block').removeClass('hidden');
                $('#click2call-test-call-form-errors').empty();

                $('#click2call-test-call-form-errors').append('{{ __('page.extension.click2call.validation') }}');
            } else {
                $('#click2call-test-call-errors-block').addClass('hidden');
                $('#click2call-test-call-form-errors').empty();

                $.get('http://127.0.0.1/call.php?phone=' + phone + '&exten=' + ext + '&pass=' + pass);

                $('#master_modal_title').html('{{ __('page.extension.click2call.testing_call') }}' + ext);
                $('#master_modal_body').html('{{ __('page.extension.click2call.calling_to') }}' + phone + '.');

                $("#master_modal").modal("show");
            }
        }
    </script>
@endpush