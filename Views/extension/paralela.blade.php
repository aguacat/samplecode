<div class="row">
    <div class="col-md-4">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-wrench font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.izquierdo.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.paralela.bloque.izquierdo.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <form method="post">

                    {{ csrf_field() }}

                    <div id="paralela-errors-block" class="form-group hidden">
                        <div class="alert alert-danger">
                            <ul id="paralela-form-errors">
                            </ul>
                        </div>
                    </div>

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">{{ __('page.extension.paralela.campo.extension_destino') }}</label>
                            <div class="input-icon right">
                                <select class="form-control select2"
                                        data-placeholder="{{ __('page.extension.paralela.placeholder.extension_destino') }}"
                                        style="width:100%"
                                        id="paralela_ext_destino">
                                    <option></option>
                                    @foreach($extensiones_destino as $extension_destino)
                                        <option value="{{ $extension_destino->id }}">{{ $extension_destino->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">
                                {{ __('page.extension.paralela.campo.descripcion') }}
                            </label>
                            <div class="input-icon right">
                                <input type="text" class="form-control" id="paralela_descripcion" value="">
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <div id="paralela_action_section" class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="button"
                                        class="btn blue"
                                        onclick="paralelaPrepareForm(requestType.POST)">
                                    {{ __('page.boton.guardar') }}
                                </button>
                                <button type="reset" class="btn default">
                                    {{ __('page.boton.cancel') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="portlet portlet-sortable light bordered">
            <div class="portlet-title ui-sortable-handle">
                <div class="caption font-blue-madison">
                    <i class="icon-list font-blue-madison"></i>
                    <span class="caption-subject bold uppercase">
                        {{ __('page.bloque.derecho.titulo') }}
                    </span>
                    <span class="caption-helper">
                        {{ __('page.extension.paralela.bloque.derecho.subtitulo') }}
                    </span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable table-scrollable-borderless">
                    <table class="table table-hover table-light">
                        <thead>
                        <tr class="uppercase">
                            <th width="30%">
                                {{ __('page.extension.paralela.tabla.campo.extension_destino') }}
                            </th>
                            <th width="70%">
                                {{ __('page.extension.paralela.tabla.campo.descripcion') }}
                            </th>
                        </tr>
                        </thead>
                        <tbody id="paralelas">
                        @forelse($extensiones_paralelas as $extension_paralela)
                            <tr id="{{ $extension_paralela->id }}" onclick="paralelaSelectData($(this))">
                                <td>{{ $extension_paralela->ext_destino }}</td>
                                <td>{{ $extension_paralela->descripcion }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2">
                                    <h4 class="text-center">
                                        {{ __('page.extension.paralela.tabla.empty') }}
                                    </h4>
                                </td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function paralelaRefreshTable() {
            $('#paralelas').empty();

            var extension_actual_name = $('#extension_actual_name').html();

            $.get('{{ asset('/extension_opcion_data') }}' + "/paralela/" + extension_actual_name, function (data) {
                if (data.length > 0) {
                    $.each(data, function (index, value) {
                        var columna1 = '<td>' + value.ext_destino + '</td>';
                        var columna2 = '<td>' + value.descripcion + '</td>';
                        var fila = '<tr id="' + value.id + '" onclick="paralelaSelectData($(this))">' + columna1 + columna2 + '</tr>';

                        $('#paralelas').append(fila);
                    });
                }

                if ($('#paralelas tr').length == 0) {
                    $('#paralelas').append('<tr><td colspan="2"><h4 class="text-center">{{ __('page.extension.paralela.tabla.empty') }}</h4></td></tr>');
                }
            }).fail(function () {
                alert("{{ __('page.extension.alert.error') }}");
            })
        }

        function paralelaResetAcceso() {
            $('#paralela_ext_destino').val('').trigger('change');
            $('#paralela_descripcion').val('');
            $('#paralelas').find('tr').removeAttr('style');
            paralelaActionBlock('create');
        }

        function paralelaActionBlock(status) {
            $('#paralela_action_section').empty();

            if (status == 'update_delete') {
                var btnModificar = '<button id="patchForm" type="button" class="btn blue" onclick="paralelaPrepareForm(requestType.PUT)">{{ __('page.boton.modificar') }}</button>';
                var btnEliminar = '<button id="destroyForm" type="button" class="btn red pull-right" onclick="paralelaPrepareForm(requestType.DELETE)">{{ __('page.boton.eliminar') }}</button>';
                var btnCancelar = '<a class="btn default" onclick="paralelaResetAcceso()">{{ __('page.boton.cancel') }}</a>';
                var divAccion = '<div class="col-md-8 pull-left">' + btnModificar + '&nbsp;' + btnCancelar + '</div><div class="col-md-4 pull-right">' + btnEliminar + '</div>';

                $('#backup_action_section').append(divAccion);

            } else {
                var btnGuardar = '<button id="postForm" type="button" class="btn blue" onclick="paralelaPrepareForm(requestType.POST)">{{ __('page.boton.guardar') }}</button> ';
                var btnCancelar = '<button type="reset" class="btn default">{{ __('page.boton.cancel') }}</button>';
                var divAccion = '<div class="col-md-offset-3 col-md-9">' + btnGuardar + btnCancelar + '</div>';

                $('#paralela_action_section').append(divAccion);
            }
        }

        function paralelaPrepareForm(currentRequestType) {
            $('#paralela-errors-block').addClass('hidden');

            var extension_actual_name = $('#extension_actual_name').html();
            var numero = $('#paralela_ext_destino').val();
            var descripcion = $('#paralela_descripcion').val();

            switch (currentRequestType) {
                case requestType.POST:
                    $.post('{{ asset('extension_opcion/backup') }}',
                        {
                            _token: "{{ csrf_token() }}",
                            name: extension_actual_name,
                            numero: numero,
                            descripcion: descripcion
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#paralela-errors-block').removeClass('hidden');
                            $('#paralela-form-errors').empty();

                            for (var error in errors) {
                                $('#paralela-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            paralelaRefreshTable();
                            paralelaResetAcceso();
                        });
                    break;
                case requestType.PUT:
                    $.put('{{ asset('extension_opcion/paralela') }}/' + $('.paralela_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            name: extension_actual_name,
                            numero: numero,
                            descripcion: descripcion
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#paralela-errors-block').removeClass('hidden');
                            $('#paralela-form-errors').empty();

                            for (var error in errors) {
                                $('#paralela-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            paralelaRefreshTable();
                            paralelaResetAcceso();
                        });
                    break;
                case requestType.DELETE:
                    $.delete('{{ asset('extension_opcion/paralela') }}/' + $('.paralela_selected_row').prop('id'),
                        {
                            _token: "{{ csrf_token() }}",
                            _method: "DELETE"
                        })
                        .fail(function (xhr, status, error) {
                            var errors = JSON.parse(xhr.responseText);

                            $('#paralela-errors-block').removeClass('hidden');
                            $('#paralela-form-errors').empty();

                            for (var error in errors) {
                                $('#paralela-form-errors').append('<li>' + errors[error] + '</li>');
                            }
                        })
                        .done(function () {
                            paralelaRefreshTable();
                            paralelaResetAcceso();
                        });
                    break;
                default:
                    console.log('Nope... Failed.');
                    //Invalid Request.
                    break;
            }
        }

        function paralelaSelectData(element) {
            $('#paralela').find('tr').removeAttr('style');

            element.addClass('backup_selected_row');
            element.css('border-left', '0.3em solid #3598dc');
            element.css('border-right', '0.3em solid #3598dc');

            $.get('{{ asset('extension_opcion') }}/paralela/' + element.prop('id'))
                .done(function (data) {
                    console.log(data);

                    $('#paralela_ext_destino').val(data.field1).trigger('change');
                    $('#paraela_descripcion').val(data.field2);

                    paralelaActionBlock('update_delete');
                })
        }
    </script>
@endpush