@extends('layouts.master')

@section('pagelevelcss')
    <!-- BEGIN SELECT2 -->
    <link href="{{ asset('/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <!-- END SELECT2 -->
    <link href="{{ asset('/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('layouts.pageheader')

        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-4">
                    <div class="portlet portlet-sortable light bordered">
                        <div class="portlet-title ui-sortable-handle">
                            <div class="caption font-blue-madison">
                                <i class="icon-list font-blue-madison"></i>
                                <span class="caption-subject bold uppercase">
                                    {{ __('page.extension.bloque.izquierdo.titulo') }}
                                </span>
                                <span class="caption-helper">
                                    {{ __('page.extension.bloque.izquierdo.subtitulo') }}
                                    <span id="extension_actual_name">{{ $extension_actual->name }}</span>
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <!-- BEGIN FORM-->
                            <form id="extensionForm" class="form-horizontal" method="post"
                                  action="{{ asset('/extension') . '/' . $extension_actual->id_ext }}">

                            {{ csrf_field() }}

                            @include('layouts.errors')

                            <!-- fake fields are a workaround for chrome autofill getting the wrong fields -->
                                <input style="display:none" type="text" name="fakeusernameremembered"/>
                                <input style="display:none" type="password" name="fakepasswordremembered"/>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.extension') }}
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.extension') }}"
                                                   name="name"
                                                   value="{{ $extension_actual->name }}" autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.contrasena') }}"
                                                   name="contrasena"
                                                   value="{{ $extension_actual->contrasena }}"
                                                   autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.tipo') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="id_ext_tipo"
                                                    id="id_ext_tipo"
                                                    data-placeholder="{{ __('page.extension.placeholder.tipo') }}">
                                                <option value=""></option>
                                                @foreach($tipos as $tipo)
                                                    <option value="{{$tipo->id_ext_tipo}}"
                                                            @if($tipo->id_ext_tipo == $extension_actual->id_ext_tipo) selected="selected" @endif>{{ $tipo->ext_tipo }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.nombre') }}
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.nombre') }}"
                                                   name="display_name"
                                                   value="{{ $extension_actual->display_name }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.email') }}
                                        </label>
                                        <div class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.email') }}"
                                                   name="email"
                                                   value="{{ $extension_actual->email }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.dept') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="departamento"
                                                    id="departamento"
                                                    data-placeholder="{{ __('page.extension.placeholder.dept') }}">
                                                <option value=""></option>
                                                @foreach($departamentos as $departamento)
                                                    <option value="{{ $departamento->id_departamento }}"
                                                            @if($departamento->id_departamento == $extension_actual->id_departamento) selected="selected" @endif>{{ $departamento->sucursal->sucursal }}
                                                        | {{ $departamento->departamento }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.politica') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="politica"
                                                    id="politica"
                                                    data-placeholder="{{ __('page.extension.placeholder.politica') }}">
                                                <option value=""></option>
                                                @foreach($politicas as $politica)
                                                    <option value="{{ $politica->id_politica }}"
                                                            @if($politica->id_politica == $extension_actual->id_politica) selected="selected" @endif>{{ $politica->politica }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-bottom: 0">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.medida_respaldo') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="medida_respaldo_llamada"
                                                    id="medida_respaldo_llamada"
                                                    data-placeholder="{{ __('page.extension.placeholder.medida_respaldo') }}"
                                                    onchange="loadMedidaRespaldo($(this))">
                                                <option value=""></option>
                                                @foreach($medidas_respaldo as $medida_respaldo)
                                                    <option value="{{$medida_respaldo->medida_respaldo}}"
                                                            @if($medida_respaldo->medida_respaldo == $extension_actual->medida_respaldo_llamada) selected="selected" @endif>{{ $medida_respaldo->descripcion }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div id="contenedor_medida_respaldo_llamada_dato" class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.medida_respaldo_dato') }}"
                                                   name="medida_respaldo_llamada_dato"
                                                   value="{{ $extension_actual->medida_respaldo_llamada_dato }}"
                                                   @if(empty($extension_actual->medida_respaldo_llamada_dato)) readonly="readonly" @endif>
                                        </div>
                                    </div>

                                    <div class="form-group" style="margin-bottom: 0">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.medida_perdida') }}
                                        </label>
                                        <div id="contenedor_medida_perdida_llamada" class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.medida_perdida') }}"
                                                   name="medida_perdida_llamada_dato"
                                                   readonly="readonly">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"></label>
                                        <div id="contenedor_medida_perdida_llamada_dato" class="col-md-9">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.medida_perdida_dato') }}"
                                                   name="medida_perdida_llamada_dato"
                                                   readonly="readonly">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.modelo_dispositivo') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="id_dispositivo_modelo"
                                                    id="id_dispositivo_modelo"
                                                    data-placeholder="{{ __('page.extension.placeholder.modelo_dispositivo') }}">
                                                <option value=""></option>
                                                @foreach($dispositivos_modelo as $dispositivo_modelo)
                                                    <option value="{{$dispositivo_modelo->id_dispositivo_modelo}}"
                                                            @if($dispositivo_modelo->id_dispositivo_modelo == $extension_actual->id_dispositivo_modelo) selected="selected" @endif>{{ $dispositivo_modelo->dispositivo_modelo }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.protocolo') }}
                                        </label>
                                        <div class="col-md-9">
                                            <select class="form-control select2"
                                                    name="id_ext_protocolo"
                                                    id="id_ext_protocolo"
                                                    data-placeholder="{{ __('page.extension.placeholder.protocolo') }}">
                                                <option value=""></option>
                                                @foreach($ext_protocolos as $ext_protocolo)
                                                    <option value="{{$ext_protocolo->id_ext_protocolo}}"
                                                            @if($ext_protocolo->id_ext_protocolo == $extension_actual->id_ext_protocolo) selected="selected" @endif>{{ $ext_protocolo->ext_protocolo }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.mac_address') }}
                                        </label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <input type="text"
                                                       class="form-control"
                                                       placeholder="{{ __('page.extension.placeholder.mac_address') }}"
                                                       id="ext_mac_address"
                                                       name="mac_address"
                                                       value="{{ $extension_actual->mac_address }}" autocomplete="off">
                                                <span class="input-group-btn">
                                                    <button class="btn blue" type="button"
                                                            onclick="generateMacAddress()"><i class="fa fa-gears"></i>
                                                    </button>
                                                </span>
                                            </div>
                                            <!-- /input-group -->
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">
                                            {{ __('page.extension.campo.forward_activado') }}
                                        </label>
                                        <div class="col-md-2">
                                            <label class="mt-checkbox" style="position: relative">
                                                <input type="checkbox" name="fwd"
                                                       @if($extension_actual->fwd) checked="checked" @endif>
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="col-md-7">
                                            <input type="text"
                                                   class="form-control"
                                                   placeholder="{{ __('page.extension.placeholder.forward_activado') }}"
                                                   name="fwd_numero" value="{{ $extension_actual->fwd_numero }}">
                                        </div>
                                    </div>

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-8 pull-left">
                                                <button id="patchForm" type="button" class="btn blue">
                                                    {{ __('page.boton.modificar') }}
                                                </button>
                                                <a href="/extension" class="btn default">
                                                    {{ __('page.boton.cancel') }}
                                                </a>
                                            </div>
                                            <div class="col-md-4 pull-right">
                                                <button id="destroyForm" type="button" class="btn red pull-right">
                                                    {{ __('page.boton.eliminar') }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="portlet portlet-sortable light bordered">
                        <div class="portlet-title ui-sortable-handle">
                            <div class="caption font-blue-madison">
                                <i class="icon-list font-blue-madison"></i>
                                <span class="caption-subject bold uppercase">
                                    {{ __('page.extension.bloque.derecho.titulo') }}
                                </span>
                                <span class="caption-helper">
                                    {{ __('page.extension.bloque.derecho.subtitulo') }}
                                </span>
                            </div>
                            <div class="pull-right">
                                <button type="button" class="btn blue btn-small"
                                        onclick="resetPinCode({{ $extension_actual->id_ext }});">
                                    {{ __('page.boton.reset_pin_code') }}
                                </button>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.opcion_de_llamada.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.opcion_de_llamada.1') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="bloqueo_llamadas_fuera"
                                                           @if($extension_actual->bloqueo_llamadas_fuera) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.2') }}</td>
                                            <td class="text-center">
                                                <input type="text" class="form-control text-center"
                                                       name="duracion_maxima_llamada_entrante"
                                                       id="duracion_maxima_llamada_entrante"
                                                       value="{{ $extension_actual->duracion_maxima_llamada_entrante }}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.3') }}</td>
                                            <td class="text-center">
                                                <input type="text" class="form-control text-center"
                                                       name="duracion_maxima_llamada_saliente"
                                                       id="duracion_maxima_llamada_saliente"
                                                       value="{{ $extension_actual->duracion_maxima_llamada_saliente }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.4') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="email_llamada_perdida"
                                                           @if($extension_actual->email_llamada_perdida) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.5') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="forward_automatico"
                                                           @if($extension_actual->forward_automatico) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.6') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="forward_simultaneo"
                                                           @if($extension_actual->forward_simultaneo) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.7') }}</td>
                                            <td class="text-center">
                                                <select class="form-control input-sm select2" name="language"
                                                        id="language"
                                                        data-placeholder="-">

                                                    <option value=""></option>
                                                    @foreach($languages as $language)
                                                        <option value="{{ $language->id_language }}"
                                                                @if($language->id_language == $extension_actual->language) selected="selected" @endif>{{ substr($language->language, 0, 3) }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.8') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="whitelist_habilitado"
                                                           @if($extension_actual->whitelist_habilitado) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.9') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox"
                                                           name="nunca_pedir_contrasena_en_llamada"
                                                           @if($extension_actual->nunca_pedir_contrasena_en_llamada) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.10') }}</td>
                                            <td class="text-center">
                                                <input type="text" class="form-control text-center"
                                                       name="operador"
                                                       id="operador" value="{{ $extension_actual->operador }}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.11') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="pin_global"
                                                           @if($extension_actual->pin_global) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.12') }}</td>
                                            <td class="text-center">
                                                <input type="text" class="form-control text-center"
                                                       name="segundos_timbre"
                                                       id="segundos_timbre"
                                                       value="{{ $extension_actual->segundos_timbre }}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.13') }}</td>
                                            <td class="text-center">
                                                <input type="text" class="form-control text-center"
                                                       name="segundos_timbre_en_ocupado"
                                                       id="segundos_timbre_en_ocupado"
                                                       value="{{ $extension_actual->segundos_timbre_en_ocupado }}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opcion_de_llamada.14') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="videosupport"
                                                        id="videosupport"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    <option value="yes"
                                                            @if($extension_actual->videosupport == 'yes') selected="selected" @endif>
                                                        Si
                                                    </option>
                                                    <option value="no"
                                                            @if($extension_actual->videosupport == 'no') selected="selected" @endif>
                                                        No
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.nasvox_gui.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.1') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox"
                                                           name="permitir_modificar_email_llamada_perdida"
                                                           @if($extension_actual->permitir_modificar_email_llamada_perdida) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.2') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox"
                                                           name="puede_modificar_directorio_departamental"
                                                           @if($extension_actual->puede_modificar_directorio_departamental) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.3') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox"
                                                           name="mostrar_extension_en_directorio"
                                                           @if($extension_actual->mostrar_extension_en_directorio) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.4') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="nasvox_gui_activo"
                                                           @if($extension_actual->nasvox_gui_activo) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.nasvox_gui.5') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_administrar_bloqueos"
                                                           @if($extension_actual->puede_administrar_bloqueos) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.6') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_administrar_lista_blanca"
                                                           @if($extension_actual->puede_administrar_lista_blanca) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.7') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_editar_display_name"
                                                           @if($extension_actual->puede_editar_display_name) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.8') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_editar_pincode"
                                                           @if($extension_actual->puede_editar_pincode) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.nasvox_gui.9') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_editar_email"
                                                           @if($extension_actual->puede_editar_email) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.permisos_extension.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ __('page.extension.permisos_extension.1') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="permitir_fwd"
                                                           @if($extension_actual->permitir_fwd) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.permisos_extension.2') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox"
                                                           name="permitir_levantamientoextension"
                                                           @if($extension_actual->permitir_levantamientoextension) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.permisos_extension.3') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="permitir_levantarextension"
                                                           @if($extension_actual->permitir_levantarextension) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.permisos_extension.4') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="permitir_hacerpage"
                                                           @if($extension_actual->permitir_hacerpage) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.permisos_extension.5') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="permitir_recibirpage"
                                                           @if($extension_actual->permitir_recibirpage) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.permisos_extension.6') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="puede_activar_desactivar_politica"
                                                           @if($extension_actual->puede_activar_desactivar_politica) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.avanzado.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ __('page.extension.avanzado.1') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="encryption"
                                                        id="encryption"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    <option value="yes"
                                                            @if($extension_actual->encryption == 'yes') selected="selected" @endif>
                                                        Si
                                                    </option>
                                                    <option value="no"
                                                            @if($extension_actual->encryption == 'no') selected="selected" @endif>
                                                        No
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.avanzado.2') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="grabar_llamadas"
                                                           @if($extension_actual->grabar_llamadas) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.avanzado.3') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="tarificador_activo"
                                                           @if($extension_actual->tarificador_activo) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.avanzado.4') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="nunca_grabar"
                                                           @if($extension_actual->nunca_grabar) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.avanzado.5') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="permitir_fax"
                                                           @if($extension_actual->permitir_fax) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.avanzado.6') }}</td>
                                            <td class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="readonly"
                                                           @if($extension_actual->readonly) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.contact_center.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ __('page.extension.contact_center.1') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="nasvox_gui_queue"
                                                        id="nasvox_gui_queue"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    @foreach($ccc_queues as $ccc_queue)
                                                        <option value="{{ $ccc_queue->id_queue }}">{{ $ccc_queue->queue }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.contact_center.2') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="nasvox_gui_es_agente"
                                                           @if($extension_actual->nasvox_gui_es_agente) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.contact_center.3') }}</td>
                                            <td width="30%" class="text-center">
                                                <label class="mt-checkbox" style="position: relative">
                                                    <input type="checkbox" name="activar_desactivar_politica_en_cola"
                                                           @if($extension_actual->activar_desactivar_politica_en_cola) checked="checked" @endif>
                                                    <span></span>
                                                </label>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-hover table-light">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <h5 class="text-center">
                                                    <span class="caption-subject font-blue-sharp bold uppercase">
                                                        {{ __('page.extension.opciones_de_red.titulo') }}
                                                    </span>
                                                </h5>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ __('page.extension.opciones_de_red.1') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="allow[]" id="allow"
                                                        multiple
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    <option value="h263"
                                                            @if(in_array('h263', explode(';', $extension_actual->allow))) selected="selected" @endif>
                                                        h263
                                                    </option>
                                                    <option value="h264"
                                                            @if(in_array('h264', explode(';', $extension_actual->allow))) selected="selected" @endif>
                                                        h264
                                                    </option>
                                                    <option value="g722"
                                                            @if(in_array('g722', explode(';', $extension_actual->allow))) selected="selected" @endif>
                                                        g722
                                                    </option>
                                                    <option value="ulaw"
                                                            @if(in_array('ulaw', explode(';', $extension_actual->allow))) selected="selected" @endif>
                                                        ulaw
                                                    </option>
                                                    <option value="alaw"
                                                            @if(in_array('alaw', explode(';', $extension_actual->allow))) selected="selected" @endif>
                                                        alaw
                                                    </option>
                                                    <option value="g729"
                                                            @if(in_array('g729', explode(';', $extension_actual->allow))) selected="selected" @endif>
                                                        g729
                                                    </option>
                                                    <option value="gsm"
                                                            @if(in_array('gsm', explode(';', $extension_actual->allow))) selected="selected" @endif>
                                                        gsm
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opciones_de_red.2') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="nat" id="nat"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    <option value="yes"
                                                            @if($extension_actual->nat == 'yes') selected="selected" @endif>
                                                        Si
                                                    </option>
                                                    <option value="no"
                                                            @if($extension_actual->nat == 'no') selected="selected" @endif>
                                                        No
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="70%">{{ __('page.extension.opciones_de_red.3') }}</td>
                                            <td width="30%" class="text-center">
                                                <select class="form-control select2" name="vlan" id="vlan"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    @foreach($vlans as $vlan)
                                                        <option value="{{ $vlan->vlan }}"
                                                                @if($extension_actual->vlan == $vlan->vlan) selected="selected" @endif>{{ $vlan->descripcion }}
                                                            : {{ $vlan->ip }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{ __('page.extension.opciones_de_red.4') }}</td>
                                            <td class="text-center">
                                                <select class="form-control select2" name="transport"
                                                        id="transport"
                                                        data-placeholder="-">
                                                    <option value=""></option>
                                                    <option value="udp"
                                                            @if($extension_actual->transport == 'udp') selected="selected" @endif>
                                                        UDP
                                                    </option>
                                                    <option value="tls"
                                                            @if($extension_actual->transport == 'tls') selected="selected" @endif>
                                                        TLS
                                                    </option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                </form>
                <!-- END FORM-->
            </div>

            <div class="row">
                <div class="portlet portlet-sortable light bordered">
                    <div class="portlet-title ui-sortable-handle">
                        <div class="caption font-blue-madison">
                            <i class="icon-list font-blue-madison"></i>
                            <span class="caption-subject bold uppercase">
                                {{ __('page.extension.bloque.inferior.titulo') }}
                            </span>
                            <span class="caption-helper">
                                {{ __('page.extension.bloque.inferior.subtitulo') }}
                            </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#acceso" data-toggle="tab">
                                        {{ __('page.extension.bloque.acceso') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="#backup" data-toggle="tab">
                                        {{ __('page.extension.bloque.backup') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="#bloqueo" data-toggle="tab">
                                        {{ __('page.extension.bloque.bloqueo') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="#click2call" data-toggle="tab">
                                        {{ __('page.extension.bloque.click2call') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="#dispositivos" data-toggle="tab">
                                        {{ __('page.extension.bloque.dispositivos') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="#listablanca" data-toggle="tab">
                                        {{ __('page.extension.bloque.lista_blanca') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="#monitoreo" data-toggle="tab">
                                        {{ __('page.extension.bloque.monitoreo') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="#paralela" data-toggle="tab">
                                        {{ __('page.extension.bloque.paralela') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="#rutas" data-toggle="tab">
                                        {{ __('page.extension.bloque.rutas') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="#numeroasignado" data-toggle="tab">
                                        {{ __('page.extension.bloque.numero_asignado') }}
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="acceso">
                                    @include('extension.acceso')
                                </div>
                                <div class="tab-pane" id="backup">
                                    @include('extension.backup')
                                </div>
                                <div class="tab-pane" id="bloqueo">
                                    @include('extension.bloqueo')
                                </div>
                                <div class="tab-pane" id="click2call">
                                    @include('extension.click2call')
                                </div>
                                <div class="tab-pane" id="dispositivos">
                                    @include('extension.dispositivos')
                                </div>
                                <div class="tab-pane" id="listablanca">
                                    @include('extension.listablanca')
                                </div>
                                <div class="tab-pane" id="monitoreo">
                                    @include('extension.monitoreo')
                                </div>
                                <div class="tab-pane" id="paralela">
                                    @include('extension.paralela')
                                </div>
                                <div class="tab-pane" id="rutas">
                                    @include('extension.rutas')
                                </div>
                                <div class="tab-pane" id="numeroasignado">
                                    @include('extension.numeroasignado')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('pagelevelplugins')
    <script src="{{ asset('/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
            type="text/javascript"></script>
@endsection

@section('pagelevelscripts')
    <script src="{{ asset('pages/scripts/table-datatables-editable.min.js') }}" type="text/javascript"></script>
@endsection

@section('customjs')
    <script>
        function generateMacAddress() {
            $('#ext_mac_address').val('00:00:00:00:00:00');
        }

        function resetPinCode(extension) {
            $.post('{{ asset('extension_pincode_reset/') }}',
                {
                    _token: "{{ csrf_token() }}",
                    id_ext: extension,
                })
                .done(function () {
                    $('#master_modal_title').html('{{ __('page.extension.modal.titulo') }} ' + $('#extension_actual_name').html());
                    $('#master_modal_body').html('{{ __('page.extension.modal.cuerpo') }}');

                    $("#master_modal").modal("show");
                });
        }

        function loadMedidaRespaldo(element) {
            var medida_respaldo = element.find(':selected').val();

            $.get('{{ asset('extension_nueva') }}' + '/' + medida_respaldo + '/medida_respaldo')
                .done(function (data) {
                    $('#contenedor_medida_respaldo_llamada_dato').empty();
                    $('#contenedor_medida_perdida_llamada').empty();
                    $('#contenedor_medida_perdida_llamada_dato').empty();

                    //console.log(data.tipo_input);
                    //console.log(data.bloqueado);
                    //console.log(data.dato_obligatorio);

                    switch (data.tipo_input) {
                        case 'select':
                            var select_respaldo = $('<select>')
                                .prop('name', 'medida_respaldo_llamada_dato')
                                .prop('class', 'form-control')
                                .appendTo('#contenedor_medida_respaldo_llamada_dato');

                            select_respaldo.append($("<option>").attr('value', '').text(''));

                            $(data.opciones_select_medida_respaldo_dato).each(function (index, element) {
                                select_respaldo.append($("<option>").attr('value', element[Object.keys(element)[0]]).text(element[Object.keys(element)[1]]));
                            });

                            select_respaldo.select2({
                                placeholder: 'Escoja Medida Respaldo'
                            });
                            break;
                        default:
                            var input_medida_respaldo_dato = $('<input>').prop('class', 'form-control')
                                .prop('name', 'medida_respaldo_llamada_dato')
                                .prop('placeholder', '{{ __('page.extension.campo.medida_respaldo') }}')
                                .appendTo('#contenedor_medida_respaldo_llamada_dato');

                            if (data.bloqueado) {
                                input_medida_respaldo_dato.prop('readonly', true);
                            }
                            break;
                    }

                    if (data.opciones_select_medida_perdida.length > 0) {
                        var select_perdida = $('<select>')
                            .prop('id', 'select_medida_perdida_llamada')
                            .prop('name', 'contenedor_medida_perdida_llamada')
                            .prop('class', 'form-control')
                            .prop('placeholder', '{{ __('page.extension.placeholder.medida_perdida') }}')
                            .appendTo('#contenedor_medida_perdida_llamada');

                        select_perdida.append($("<option>").attr('value', '').text(''));

                        $(data.opciones_select_medida_perdida).each(function (index, element) {
                            select_perdida.append($("<option>").attr('value', element[Object.keys(element)[0]]).text(element[Object.keys(element)[1]]));
                        });

                        select_perdida.select2({
                            placeholder: 'Escoja Medida Perdida'
                        });
                    } else {
                        var input_medida_perdida = $('<input>').prop('class', 'form-control')
                            .prop('name', 'medida_perdida_llamada')
                            .prop('placeholder', '{{ __('page.extension.campo.medida_perdida') }}')
                            .prop('readonly', true)
                            .appendTo('#contenedor_medida_perdida_llamada');
                    }

                    var input_medida_perdida_dato = $('<input>').prop('class', 'form-control')
                        .prop('name', 'medida_perdida_llamada_dato')
                        .prop('placeholder', '{{ __('page.extension.campo.medida_perdida') }}')
                        .prop('readonly', true)
                        .appendTo('#contenedor_medida_perdida_llamada_dato');
                });
        }

        $(document).on('change', '#select_medida_perdida_llamada', function () {
            var medida_respaldo = $('#medida_respaldo_llamada').find(':selected').val();
            var medida_perdida = $('#select_medida_perdida_llamada').find(':selected').val();

            console.log(medida_respaldo);
            console.log(medida_perdida);

            $.get('{{ asset('extension_nueva') }}' + '/' + medida_respaldo + '/' + medida_perdida + '/medida_perdida')
                .done(function (data) {
                    $('#contenedor_medida_perdida_llamada_dato').empty();

                    switch (data.tipo_input) {
                        case 'select':
                            var select_perdida_dato = $('<select>')
                                .prop('name', 'medida_respaldo_llamada_dato')
                                .prop('class', 'form-control')
                                .appendTo('#contenedor_medida_perdida_llamada_dato');

                            select_perdida_dato.append($("<option>").attr('value', '').text(''));

                            $(data.opciones_select_medida_perdida_dato).each(function (index, element) {
                                select_perdida_dato.append($("<option>").attr('value', element[Object.keys(element)[0]]).text(element[Object.keys(element)[1]]));
                            });

                            select_perdida_dato.select2({
                                placeholder: 'Escoja Medida Respaldo'
                            });
                            break;
                        default:
                            var input_medida_perdida_dato = $('<input>').prop('class', 'form-control')
                                .prop('name', 'medida_perdida_llamada_dato')
                                .prop('placeholder', '{{ __('page.extension.campo.medida_respaldo') }}')
                                .appendTo('#contenedor_medida_perdida_llamada_dato');

                            if (data.bloqueado) {
                                input_medida_perdida_dato.prop('readonly', true);
                            }
                            break;
                    }

                });
        });

        var form = document.getElementById('extensionForm');
        var input = document.createElement("input");

        input.type = "hidden";
        input.name = "_method";

        form.onsubmit = function () {
            form.target = '_self';
        };

        document.getElementById('patchForm').onclick = function () {
            input.value = "PUT";
            form.appendChild(input);
            form.submit();
        };

        document.getElementById('destroyForm').onclick = function () {
            input.value = "DELETE";
            form.appendChild(input);
            form.submit();
        }

        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>

    @stack('scripts')
@endsection
