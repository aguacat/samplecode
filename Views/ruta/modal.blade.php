<!-- BEGIN MODAL SECTION -->
<div class="modal fade bs-modal-lg" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    <span class="bold">{{ __('page.ruta.modal.titulo') }}</span>
                    <div id="modalOrderActions" class="pull-right hidden" style="margin-right: 2%">
                        <button id="modalRutaRespaldoOrderUp" type="button" class="btn blue btn-xs"
                                onclick="orderUpRutaRespaldo()">
                            <i class="fa fa-level-up"></i>
                            {{ __('page.ruta.modal.boton.subir_orden') }}
                        </button>
                        <button id="modalRutaRespaldoOrderDown" type="button" class="btn blue btn-xs"
                                onclick="orderDownRutaRespaldo()">
                            <i class="fa fa-level-down"></i>
                            {{ __('page.ruta.modal.boton.bajar_orden') }}
                        </button>
                    </div>
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <form id="rutaRespaldoForm" method="post" action="">

                            {{ csrf_field() }}

                            <div id="modal-errors-block" class="form-group hidden">
                                <div class="alert alert-danger">
                                    <ul id="modal-form-errors">
                                    </ul>
                                </div>
                            </div>

                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label">
                                        {{ __('page.ruta.modal.campo.ruta') }}
                                    </label>
                                    <div class="input-icon right">
                                        <input type="text" class="form-control" id="modal_ruta"
                                               value="" readonly="readonly">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">
                                        {{ __('page.ruta.modal.campo.canal') }}
                                    </label>
                                    <div class="input-icon right">
                                        <select class="form-control select2" id="modal_canal"
                                                data-placeholder="Elija un Canal" style="width: 100%">
                                            <option></option>
                                            @foreach($canales as $canal)
                                                <option value="{{ $canal->canal }}"
                                                        @if($canal->canal == old('canal')) selected="selected" @endif>{{ $canal->canal }}
                                                    [ {{ $canal->descripcion }} @if($canal->sufijo)
                                                        - {{ $canal-sufujo }} @endif @if($canal->protocolo)
                                                        - {{ $canal->protocolo }}@endif ]
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">
                                        {{ __('page.ruta.modal.campo.prefijo') }}
                                    </label>
                                    <div class="input-icon right">
                                        <input type="text" class="form-control" id="modal_prefijo" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">
                                        {{ __('page.ruta.modal.campo.digito_inicio') }}
                                    </label>
                                    <div class="input-icon right">
                                        <input type="number" min="0" max="100" value="" class="form-control"
                                               id="modal_digito_inicio" placeholder="(-) Dígito Inicio">
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-8 pull-left">
                                        <button id="patchRutaRespaldo" type="button" class="btn blue btn-sm"
                                                onclick="newOrUpdateRutaRespaldo($(this))">
                                            {{ __('page.boton.modificar') }}
                                        </button>
                                        <button id="" type="button" class="btn default btn-sm"
                                                onclick="cancelAction()">
                                            {{ __('page.boton.cancel') }}
                                        </button>
                                    </div>
                                    <div class="col-md-4 pull-right">
                                        <button id="destroyRutaRespaldo" type="button"
                                                class="btn btn-sm red pull-right hidden" onclick="deleteRutaRespaldo()">
                                            {{ __('page.boton.eliminar') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-8">
                        <table id="ruta_respaldo_table" class="table table-hover table-light">
                            <thead>
                            <tr class="uppercase">
                                <th width="25%">
                                    {{ __('page.ruta.modal.tabla.campo.orden') }}
                                </th>
                                <th width="25%">
                                    {{ __('page.ruta.modal.tabla.campo.canal') }}
                                </th>
                                <th width="25%">
                                    {{ __('page.ruta.modal.tabla.campo.prefijo') }}
                                </th>
                                <th width="25%">
                                    {{ __('page.ruta.modal.tabla.campo.cant_digito_sustraer_inicio') }}
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr onclick="">
                                <td>TEXT</td>
                                <td>TEXT</td>
                                <td>TEXT</td>
                                <td>TEXT</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn dark btn-outline" data-dismiss="modal">
                    {{ __('page.boton.cerrar_ventana') }}
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- END MODAL SECTION -->

<script src="{{ asset('global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
<script>
    var id_ruta_respaldo;
    var id_ruta;

    function cancelAction() {
        clearDataForm();
        loadModalTable();
    }

    function loadModalTable() {
        var table = $('#ruta_respaldo_table > tbody');

        $.get('{{ asset('/ruta') }}/' + id_ruta + '/ruta_respaldo')
            .done(function (ruta_respaldo) {
                if (ruta_respaldo[0] !== undefined) {
                    table.empty();

                    for (var key in ruta_respaldo) {
                        var row = '<tr id="' + ruta_respaldo[key].id_ruta_respaldo + '__generated" onclick="loadDataToForm($(this))">';
                        row += '<td class="_orden">' + ruta_respaldo[key].orden + '</td>';
                        row += '<td class="_canal">' + ruta_respaldo[key].canal + '</td>';
                        row += '<td class="_prefijo">' + ruta_respaldo[key].prefijo + '</td>';
                        row += '<td class="_digito_inicio">' + ruta_respaldo[key].cantidad_digito_sustraer_inicio + '</td>';
                        row += '</tr>';
                        table.append(row);
                    }
                } else {
                    table.empty();
                    table.append('<tr><td class="text-center" colspan="4">{{ __('page.ruta.modal.tabla.empty') }}</td></tr>');
                }
            });
    }

    function clearDataForm() {
        $('#modal_canal').val('').select2();
        $('#modal_prefijo').val('');
        $('#modal_digito_inicio').val('0');
        $('#patchRutaRespaldo').html("{{ __('page.boton.guardar') }}");
        $('#patchRutaRespaldo').prop('disabled', false);
        $('#destroyRutaRespaldo').addClass('hidden');
        $('#modal-form-errors').empty();
        $('#modal-errors-block').addClass('hidden');
        $('#modalOrderActions').addClass('hidden');
    }

    function loadDataToForm(row) {
        var local_row = $(row[0]);

        if ($('#ruta_respaldo_table > tbody tr').length > 1) {
            $('#modalOrderActions').removeClass('hidden');
        }

        id_ruta_respaldo = local_row.prop('id');

        var table = $('#ruta_respaldo_table > tbody');
        table.find('tr').removeAttr('style');

        local_row.css('border-left', '0.3em solid #3598dc');
        local_row.css('border-right', '0.3em solid #3598dc');

        $('#modal_canal').val(local_row.find('td._canal').html()).select2();
        $('#modal_prefijo').val(local_row.find('td._prefijo').html());
        $('#modal_digito_inicio').val(local_row.find('td._digito_inicio').html());
        $('#modal_activo').prop('checked', local_row.find('input._activo').is(":checked"));
        $('#patchRutaRespaldo').html("{{ __('page.boton.modificar') }}");
        $('#destroyRutaRespaldo').removeClass('hidden');
    }

    function newOrUpdateRutaRespaldo(element) {
        $('#patchRutaRespaldo').prop('disabled', true);

        var rowId = $('#modal_ruta').val();
        rowId = '#' + rowId + '__generated';

        var ruta = $('#modal_ruta').val();
        var canal = $('#modal_canal').val();
        var prefijo = $('#modal_prefijo').val();
        var digito_inicio = $('#modal_digito_inicio').val();

        switch (element.html()) {
            case '{{ __('page.boton.guardar') }}':
                $.post('{{ asset('/ruta/storeRutaRespaldo') }}',
                    {
                        _token: "{{ csrf_token() }}",
                        _method: "POST",
                        ruta: ruta,
                        canal: canal,
                        cantidad_digito_sustraer_inicio: digito_inicio,
                        prefijo: prefijo
                    })
                    .fail(function (xhr, status, error) {
                        var errors = JSON.parse(xhr.responseText);

                        $('#modal-errors-block').removeClass('hidden');
                        $('#modal-form-errors').empty();

                        if (typeof errors === 'object') {
                            for (var error in errors) {
                                $('#modal-form-errors').append('<li>' + errors[error] + '.</li>');
                            }
                        } else {
                            $('#modal-form-errors').append('<li>' + errors + '.</li>');
                        }

                        $('#patchRutaRespaldo').prop('disabled', false);
                    })
                    .done(function () {
                        clearDataForm();
                        loadModalTable();
                    });
                break;
            case '{{ __('page.boton.modificar') }}':
                $.put('{{ asset('/ruta') }}/' + id_ruta_respaldo.slice(0, -11) + '/updateRutaRespaldo',
                    {
                        _token: "{{ csrf_token() }}",
                        _method: "POST",
                        ruta: ruta,
                        canal: canal,
                        cantidad_digito_sustraer_inicio: digito_inicio,
                        prefijo: prefijo
                    })
                    .fail(function (xhr, status, error) {
                        var errors = JSON.parse(xhr.responseText);

                        $('#modal-errors-block').removeClass('hidden');
                        $('#modal-form-errors').empty();

                        if (typeof errors === 'object') {
                            for (var error in errors) {
                                $('#modal-form-errors').append('<li>' + errors[error] + '.</li>');
                            }
                        } else {
                            $('#modal-form-errors').append('<li>' + errors + '.</li>');
                        }

                        $('#patchRutaRespaldo').prop('disabled', false);
                    })
                    .done(function () {
                        clearDataForm();
                        loadModalTable();
                    });
                break;
            default:
                break;
        }
    }

    function deleteRutaRespaldo() {
        var rowId = $('#modal_ruta').val();
        rowId = '#' + rowId + '__generated';

        bootbox.confirm("{{ __('page.ruta.modal.confirmation') }}", function (result) {
            if (result) {
                $.delete('{{ asset('/ruta') }}/' + id_ruta_respaldo.slice(0, -11) + '/destroyRutaRespaldo', {_token: "{{ csrf_token() }}"})
                    .done(function (data) {
                        if (data) {
                            clearDataForm();
                            loadModalTable();
                        }
                    });
            }
        });
    }

    function orderUpRutaRespaldo() {
        $.put('{{ asset('/ruta') }}/' + id_ruta_respaldo.slice(0, -11) + '/orderUp',
            {
                _token: "{{ csrf_token() }}",
                _method: "PUT",
            })
            .done(function () {
                clearDataForm();
                loadModalTable();
            });
    }

    function orderDownRutaRespaldo() {
        $.put('{{ asset('/ruta') }}/' + id_ruta_respaldo.slice(0, -11) + '/orderDown',
            {
                _token: "{{ csrf_token() }}",
                _method: "PUT",
            })
            .done(function () {
                clearDataForm();
                loadModalTable();
            });
    }

    $('.modal-trigger').on('click', function () {
        id_ruta = $(this).closest('td').closest('tr').attr('id');
        $('#modal_digito_inicio').val('0');

        clearDataForm();
        loadModalTable();

        $.get('{{ asset('/ruta') }}/' + id_ruta + '/ruta')
            .done(function (ruta) {
                $('#modal_ruta').val(ruta[0].ruta);
            });
    });
</script>