@extends('layouts.master')

@section('pagelevelcss')
    <!-- BEGIN SELECT2 -->
    <link href="{{ asset('/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <!-- END SELECT2 -->
    <link href="{{ asset('/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
        @include('layouts.pageheader')

        <!-- BEGIN PAGE BASE CONTENT -->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet portlet-sortable light bordered">
                        <div class="portlet-title ui-sortable-handle">
                            <div class="caption font-blue-madison">
                                <i class="icon-wrench font-blue-madison"></i>
                                <span class="caption-subject bold uppercase">
                                    {{ __('page.bloque.izquierdo.titulo') }}
                                </span>
                                <span class="caption-helper">
                                    {{ __('page.ruta.bloque.izquierdo.subtitulo.modificar') }}
                                </span>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <form class="horizontal-form" id="rutaForm" method="post"
                                  action="{{ asset('/ruta/' . $ruta_actual->id_ruta) }}">

                                {{ csrf_field() }}

                                @include('layouts.errors')

                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            {{ __('page.ruta.campo.ruta') }}
                                                        </label>
                                                        <input type="text" name="ruta" class="form-control"
                                                               placeholder="Ruta" value="{{ $ruta_actual->ruta }}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{ $ruta_actual->id_canal }}
                                                        <label class="control-label">
                                                            {{ __('page.ruta.campo.canal') }}
                                                        </label>
                                                        <select class="form-control select2" name="canal"
                                                                data-placeholder="Elija un Canal">
                                                            <option></option>
                                                            @foreach($canales as $canal)
                                                                <option value="{{ $canal->canal }}"
                                                                        @if($canal->canal == $ruta_actual->canal) selected="selected" @endif>{{ $canal->canal }}
                                                                    [ {{ $canal->descripcion }} @if($canal->sufijo)
                                                                        - {{ $canal-sufijo }} @endif @if($canal->protocolo)
                                                                        - {{ $canal->protocolo }}@endif ]
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            {{ __('page.ruta.campo.costo_por_minuto') }}
                                                        </label>
                                                        <input type="text" name="costo_minuto" class="form-control"
                                                               placeholder="Costo por Minuto"
                                                               value="{{ $ruta_actual->costo_minuto }}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            {{ __('page.ruta.campo.sustraer_digito_inicio') }}
                                                        </label>
                                                        <input type="text" name="cantidad_digitos_sustraer_inicio"
                                                               class="form-control"
                                                               placeholder="Sustraer Dígito Inicio"
                                                               value="{{ $ruta_actual->cantidad_digitos_sustraer_inicio }}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            {{ __('page.ruta.campo.sustraer_digito_final') }}
                                                        </label>
                                                        <input type="text" name="cantidad_digitos_sustraer_final"
                                                               class="form-control"
                                                               placeholder="Sustraer Dígito Final"
                                                               value="{{ $ruta_actual->cantidad_digitos_sustraer_final }}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            {{ __('page.ruta.campo.prefijo') }}
                                                        </label>
                                                        <input type="text" name="prefijo" class="form-control"
                                                               placeholder="Prefijo"
                                                               value="{{ $ruta_actual->prefijo }}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            {{ __('page.ruta.campo.descripcion') }}
                                                        </label>
                                                        <input type="text" name="descripcion" class="form-control"
                                                               placeholder="Descripción"
                                                               value="{{ $ruta_actual->descripcion }}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            {{ __('page.ruta.campo.sustraer_digitos_para_caller_id') }}
                                                        </label>
                                                        <input type="number"
                                                               min="0"
                                                               max="100"
                                                               name="cantidad_digitos_sustraer_para_callerid"
                                                               class="form-control"
                                                               placeholder="Descripción"
                                                               value="{{ $ruta_actual->cantidad_digitos_sustraer_para_callerid }}">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="callerid_activar"
                                                           value="true"
                                                           @if($ruta_actual->callerid_activar) checked="checked "@endif>
                                                    {{ __('page.ruta.campo.activar_caller_id') }}
                                                    <span></span>
                                                </label>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="activo"
                                                           value="true"
                                                           @if($ruta_actual->activo) checked="checked "@endif>
                                                    {{ __('page.ruta.campo.activo') }}
                                                    <span></span>
                                                </label>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="requiere_contrasena"
                                                           value="true"
                                                           @if($ruta_actual->requiere_contrasena) checked="checked "@endif>
                                                    {{ __('page.ruta.campo.requiere_contrasena') }}
                                                    <span></span>
                                                </label>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="enviar_ringback_tone"
                                                           value="true"
                                                           @if($ruta_actual->enviar_ringback_tone) checked="checked "@endif>
                                                    {{ __('page.ruta.campo.enviar_ring') }}
                                                    <span></span>
                                                </label>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="abrir_canal_antes_marcado"
                                                           value="true"
                                                           @if( $ruta_actual->abrir_canal_antes_marcado) checked="checked "@endif>
                                                    {{ __('page.ruta.campo.abrir_canal_antes_de_marcado') }}
                                                    <span></span>
                                                </label>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="ruta_automatica"
                                                           value="true"
                                                           @if($ruta_actual->ruta_automatica) checked="checked "@endif>
                                                    {{ __('page.ruta.campo.ruta_automatica') }}
                                                    <span></span>
                                                </label>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="sobreescribir_ruta_cid"
                                                           value="true"
                                                           @if($ruta_actual->sobreescribir_ruta_cid))
                                                           checked="checked "@endif>
                                                    {{ __('page.ruta.campo.overwrite_call') }}
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-8 pull-left">
                                            <button id="patchForm" type="button" class="btn blue">
                                                {{ __('page.boton.modificar') }}
                                            </button>
                                            <a href="/ruta" class="btn default">
                                                {{ __('page.boton.cancel') }}
                                            </a>
                                        </div>
                                        <div class="col-md-4 pull-right">
                                            <button id="destroyForm" type="button" class="btn red pull-right">
                                                {{ __('page.boton.eliminar') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @include('ruta.table')
                </div>
            </div>
            <!-- END PAGE BASE CONTENT -->

        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
@endsection

@section('pagelevelplugins')
    <script src="{{ asset('/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
            type="text/javascript"></script>
@endsection

@section('pagelevelscripts')
    <script src="{{ asset('pages/scripts/table-datatables-editable.min.js') }}" type="text/javascript"></script>
@endsection

@section('customjs')
    <script>
        var form = document.getElementById('rutaForm');
        var input = document.createElement("input");

        input.type = "hidden";
        input.name = "_method";

        form.onsubmit = function () {
            form.target = '_self';
        };

        document.getElementById('patchForm').onclick = function () {
            input.value = "PUT";
            form.appendChild(input);
            form.submit();
        };

        document.getElementById('destroyForm').onclick = function () {
            input.value = "DELETE";
            form.appendChild(input);
            form.submit();
        }

        $(document).ready(function () {
            $('.select2').select2();
        });
    </script>

    @stack('table_scripts');
@endsection

@section('modal')
    @include('ruta.modal')
@endsection