<div class="portlet portlet-sortable light bordered">
    <div class="portlet-title ui-sortable-handle">
        <div class="caption font-blue-madison">
            <i class="icon-list font-blue-madison"></i>
            <span class="caption-subject bold uppercase">
                {{ __('page.bloque.derecho.titulo') }}
            </span>
            <span class="caption-helper">
                {{ __('page.ruta.bloque.derecho.subtitulo') }}
            </span>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-hover table-light" id="datatable">
            <thead>
            <tr class="uppercase">
                <th class="text-center all" width="15%">
                    {{ __('page.ruta.tabla.campo.ruta') }}
                </th>
                <th class="text-center all" width="20%">
                    {{ __('page.ruta.tabla.campo.descripcion') }}
                </th>
                <th class="text-center all" width="15%">
                    {{ __('page.ruta.tabla.campo.canal') }}
                </th>
                <th class="text-center none" width="10%">
                    {{ __('page.ruta.tabla.campo.prefijo') }}
                </th>
                <th class="all" width="5%">
                    <span aria-hidden="true" class="icon-bulb "></span>
                    {{-- {{ __('page.ruta.tabla.campo.activar_call') }} --}}
                </th>
                <th class="text-center none" width="12%">
                    {{ __('page.ruta.tabla.campo.caller_id') }}
                </th>
                <th class="all" width="5%">
                    <span aria-hidden="true" class="icon-check"></span>
                    {{-- {{ __('page.ruta.tabla.campo.activo') }} --}}
                </th>
                <th class="all" width="5%">
                    <span aria-hidden="true" class="icon-key"></span>
                    {{-- {{ __('page.ruta.tabla.campo.rc') }} --}}
                </th>
                <th class="all" width="5%">
                    <span aria-hidden="true" class="icon-bell"></span>
                    {{-- {{ __('page.ruta.tabla.campo.ring') }} --}}
                </th>
                <th class="all" width="5%">
                    <span aria-hidden="true" class="icon-lock-open"></span>
                    {{-- {{ __('page.ruta.tabla.campo.abrir_canal') }} --}}
                </th>
                <th class="all" width="5%">
                    <span aria-hidden="true" class="icon-magic-wand"></span>
                    {{-- {{ __('page.ruta.tabla.campo.ruta_automatica') }} --}}
                </th>
                <th class="all" width="5%">
                    <span aria-hidden="true" class="icon-layers"></span>
                    {{-- {{ __('page.ruta.tabla.campo.overwrite_call') }} --}}
                </th>
                <th class="text-center">
                    {{ __('page.ruta.tabla.campo.rutas_respaldo') }}
                </th>
                <th class="text-center none" width="5%">
                    {{ __('page.ruta.tabla.campo.sdi') }}
                </th>
                <th class="text-center none" width="5%">
                    {{ __('page.ruta.tabla.campo.sdf') }}
                </th>
                <th class="text-center none" width="5%">
                    {{ __('page.ruta.tabla.campo.cm') }}
                </th>
                <th class="text-center none" width="5%">
                    {{ __('page.ruta.tabla.campo.sdcid') }}
                </th>
            </tr>
            </thead>
            <tbody>
            @forelse($rutas as $ruta)
                <tr id="{{ $ruta->id_ruta }}">
                    <td class="text-center">{{ $ruta->ruta }}</td>

                    <td class="text-center">{{ $ruta->descripcion }}</td>

                    <td class="text-center">{{ $ruta->canal }}</td>

                    <td class="text-center">{{ $ruta->prefijo }}</td>

                    <td class="text-center">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input type="checkbox"
                                   @if($ruta->callerid_activar) checked="checked"
                                   @endif disabled>
                            <span></span>
                        </label>
                    </td>

                    <td class="text-center">{{ $ruta->callerid  }}</td>

                    <td class="text-center">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input type="checkbox" @if($ruta->activo) checked="checked"
                                   @endif disabled>
                            <span></span>
                        </label>
                    </td>

                    <td class="text-center">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input type="checkbox"
                                   @if($ruta->requiere_contrasena) checked="checked"
                                   @endif disabled>
                            <span></span>
                        </label>
                    </td>

                    <td class="text-center">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input type="checkbox"
                                   @if($ruta->enviar_ringback_tone) checked="checked"
                                   @endif disabled>
                            <span></span>
                        </label>
                    </td>

                    <td class="text-center">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input type="checkbox"
                                   @if($ruta->abrir_canal_antes_marcado) checked="checked"
                                   @endif disabled>
                            <span></span>
                        </label>
                    </td>

                    <td class="text-center">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input type="checkbox" @if($ruta->ruta_automatica) checked="checked"
                                   @endif disabled>
                            <span></span>
                        </label>
                    </td>

                    <td class="text-center">
                        <label class="mt-checkbox mt-checkbox-outline">
                            <input type="checkbox"
                                   @if($ruta->sobreescribir_ruta_cid) checked="checked"
                                   @endif disabled>
                            <span></span>
                        </label>
                    </td>

                    <td class="text-center">
                        <a class="btn blue btn-xs bold modal-trigger" data-toggle="modal" href="#modal">
                            {{ __('page.ruta.tabla.boton.rutas_respaldo') }}
                        </a>
                    </td>

                    <td class="text-center">{{ $ruta->cantidad_digitos_sustraer_inicio }}</td>

                    <td class="text-center">{{ $ruta->cantidad_digitos_sustraer_final }}</td>

                    <td class="text-center">{{ $ruta->costo_minuto }}</td>

                    <td>{{ $ruta->cantidad_digitos_sustraer_para_callerid }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="2">
                        <h4 class="text-center">{{ __('page.ruta.tabla.empty') }}</h4>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>

@push('table_scripts')
    <script>
        var item_actual = "{{ !empty($ruta_actual) ? $ruta_actual->id_ruta : '0' }}";

        $(document).ready(function () {
            $("#datatable tbody").on('click', 'tr td:not(:first-child, :nth-child(13))', function () {
                window.location = "{{ asset('/ruta') }}/" + $(this).closest('tr').attr('id') + '/edit';
            });

            $('#datatable').DataTable({
                info: false,
                searching: false,
                paging: false,
                responsive: true,
                ordering: false,
                createdRow: function (row) {
                    if (row.id == item_actual) {
                        $(row.childNodes[1]).css('border-left', '0.3em solid #3598dc');
                        $(row.childNodes[25]).css('border-right', '0.3em solid #3598dc');
                    }
                }
            });
        });
    </script>
@endpush