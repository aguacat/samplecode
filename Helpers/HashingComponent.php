<?php

namespace App\Utilities;

class HashingComponent {
    private function whereIs($command) {
        return exec("whereis $command | cut -d \" \" -f2");
    }
    
    private function hashHardDrive() {
        $commandLocation = $this->whereIs('udevadm');

        if(empty($commandLocation))
            return '';
            
        $hardDriveInfo = exec("$commandLocation info --query=all --name=/dev/sda | grep ID_SERIAL_SHORT| cut -d = -f2");
        return sha1($hardDriveInfo);
    }

    private function hashPasswordFile() {
        $commandLocation = $this->whereIs('sha1sum');

        if(empty($commandLocation))
            return '';
        
        $passwordFile = exec("$commandLocation /etc/passwd | cut -d \" \" -f1");
        return sha1($passwordFile);
    }

    private function hashShadowFile() {
        $commandLocation = $this->whereIs('sha1sum');

        if(empty($commandLocation))
            return '';
        
        $shadowFile = exec("sudo $commandLocation /etc/shadow | cut -d \" \" -f1");
        return sha1($shadowFile);
    }
    private function hashBoardInfo() {
        $commandLocation = $this->whereIs('dmidecode');

        if(empty($commandLocation))
            return '';

        $boardInfo =  exec("sudo $commandLocation -s system-serial-number");
        return sha1($boardInfo);
    }

    private function hashBackupFile() {
        $commandLocation = $this->whereIs('sha1sum');
        
        if(empty($commandLocation))
            return '';
        
        $backupFileLocation = base_path().'/.backup_env';

        $backupFile = exec("$commandLocation $backupFileLocation | cut -d \" \" -f1");
        return sha1($backupFile);
    }

    public function getHashInfo() {
        
        $hashAll = sha1(
            $this->hashHardDrive() . 
            $this->hashPasswordFile() . 
            $this->hashShadowFile() . 
            $this->hashBoardInfo() .
            $this->hashBackupFile()
        );

        return $hashAll;
    }
}