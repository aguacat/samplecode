<?php

namespace App\Utilities;

use App\Models\MgralSistemaAcceso;
use Illuminate\Support\Facades\Auth;
use App\Models\MgralSistemaAccesoUsuario;

class ModulesList {
    public static function getPermisos() {
        $is_root = session()->get('isRoot', false);

        if($is_root) {
            $available_modules = MgralSistemaAcceso::all();
        } else {
            $id_usuario = Auth::guard('web_usuario')->user()->id_usuario;
            $available_modules = MgralSistemaAccesoUsuario::ModulosDisponiblesPorUsuario($id_usuario);
        }

        return $available_modules;
    }
}