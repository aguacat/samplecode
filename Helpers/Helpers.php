<?php 

namespace App\Utilities;

use App\Models\Blog;
use App\Models\Data;
use App\Models\Page;
use App\Models\Video;
use App\Models\Race5k;
use App\Models\Race10k;
use App\Models\Sponsor;
use App\Models\Reference;
use App\Models\Volunteer;

class Helpers {
    private $title_current_year;
    private $title_last_year;

    public $meses_ingles; 
    public $meses_espanol;

    function __construct() {
        $this->title_current_year = Reference::where('name', 'codigo_anio_actual')->value('sequence');
        $this->title_current_year_walk = Reference::where('name', 'codigo_anio_actual_caminata')->value('sequence');
        $this->title_last_year = Reference::where('name', 'codigo_anio_previo')->value('sequence');
        $this->meses_ingles = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $this->meses_espanol = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    }

    /**
     *
     * Set active css class if the specific URI is current URI
     *
     * @param string $path       A specific URI
     * @param string $class_name Css class name, optional
     * @return string            Css class name if it's current URI,
     *                           otherwise - empty string
     */
    function setActive(string $path, string $class_name = "is-active")
    {
        return Request::path() === $path ? $class_name : "";
    }

    function removeSpecialCharacters($string) {
        $utf8 = array(
            '/[áàâãªä]/u'   =>   'a',
            '/[ÁÀÂÃÄ]/u'    =>   'A',
            '/[ÍÌÎÏ]/u'     =>   'I',
            '/[íìîï]/u'     =>   'i',
            '/[éèêë]/u'     =>   'e',
            '/[ÉÈÊË]/u'     =>   'E',
            '/[óòôõºö]/u'   =>   'o',
            '/[ÓÒÔÕÖ]/u'    =>   'O',
            '/[úùûü]/u'     =>   'u',
            '/[ÚÙÛÜ]/u'     =>   'U',
            '/ç/'           =>   'c',
            '/Ç/'           =>   'C',
            '/ñ/'           =>   'n',
            '/Ñ/'           =>   'N',
            '/–/'           =>   '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u'    =>   ' ', // Literally a single quote
            '/[“”«»„]/u'    =>   ' ', // Double quote
            '/ /'           =>   ' ', // nonbreaking space (equiv. to 0x160)
        );

        return preg_replace(array_keys($utf8), array_values($utf8), $string);
    }

    function cleanString($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = str_replace('_', '-', $string); // Replaces all underscore with hyphens.
        $string = $this->removeSpecialCharacters($string);
        $string = strtolower($string); // Set string to lowercase.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    static function loadAllNuestraCausa() {
        return Page::where('code_name', 'like', 'nuestra-causa-%')->orderby('code_name', 'desc')->get();
    }
    
    static function loadPostList($quantity = 6) {
        return Blog::orderBy('id', 'desc')->take($quantity)->get();
    }

    function totalRunners() {
        return Race10k::where('registration_type', 'PERM')->where('registration_number', 'LIKE', $this->title_current_year.'%')->count();
    }

    function totalWalkers() {
        return Race5k::where('registration_type', 'PERM')->where('registration_number', 'LIKE', $this->title_current_year_walk.'%')->count();
    }

    function totalVolunteers() {
        return Volunteer::count();
    }

    function yearsOfRunning() {
        return Reference::where('name', 'anios_corriendo')->value('sequence');
    }

    function videos() {
        return Video::orderBy('position')->orderBy('created_at', 'desc')->take(3)->get();
    }

    function sponsors() {
        return Sponsor::where('main', true)->take(4)->get();
    }

    function getWalkPrice($isPresent = true, $walkerCreatedDate = null) {
        $amountToPay = 0.00;

        $evaluatingDate = $isPresent ? date('Ymd') : date_format($walkerCreatedDate, 'Ymd');

        if (Reference::where('name', 'fecha_descuento')->value('sequence') >= $evaluatingDate) {
            $amountToPay = strip_tags(Data::where('code_name', 'precio-caminata')->value('value'));
            $amountToPay = $amountToPay - ($amountToPay * 0.10);
        } else {
            $amountToPay = strip_tags(Data::where('code_name', 'precio-caminata')->value('value'));
        }
        
        return $amountToPay;
    }

    function getRacePrice($isPresent = true, $runnerCreatedDate = null) {
        $amountToPay = 0.00;

        $evaluatingDate = $isPresent ? date('Ymd') : date_format($runnerCreatedDate, 'Ymd');

        if (Reference::where('name', 'fecha_descuento')->value('sequence') >= $evaluatingDate) {
            $amountToPay = strip_tags(Data::where('code_name', 'precio-carrera')->value('value'));
            $amountToPay = $amountToPay - ($amountToPay * 0.10);
        } else {
            $amountToPay = strip_tags(Data::where('code_name', 'precio-carrera')->value('value'));
        }

        return $amountToPay;
    }
}