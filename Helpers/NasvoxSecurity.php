<?php

namespace App\Http\Middleware;

use Closure;
use App\Utilities\HashingComponent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Contracts\Encryption\DecryptException;

class NasvoxSecurity
{
    private function afterDecryptFailure() {
        if (Storage::disk('nasvox_root')->exists('.backup_env'))
            Storage::disk('nasvox_root')->delete('.backup_env');

        Storage::disk('nasvox_root')->put('.backup_env', '1');
        
        Auth::guard("web_usuario")->logout();
        return redirect('/login');
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $hashingComponent = new HashingComponent();
            $decryptedHash = decrypt(env('NASVOX_KEY'));

            if($hashingComponent->getHashInfo() !== $decryptedHash)
                $this->afterDecryptFailure();            
        } catch(DecryptException $e) {
            $this->afterDecryptFailure();
        }

        return $next($request);
    }
}
