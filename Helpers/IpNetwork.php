<?php

namespace App\Utilities;

class IpNetwork {

    public $direccion_ip = null;
    public $mascara = null;

    public $red = null;
    public $default_gw = null;
    public $broadcast = null;
    public $number_of_hosts = null;
    public $ip_inicio = null;
    public $ip_fin = null;
    public $tftp = null;
    public $ntp = null;
    public $dns = null;

    public function __construct($ip_address, $subnet_mask, $start = 1, $end = 1) {
        $this->direccion_ip = $ip_address;
        $this->mascara = $subnet_mask;

        $this->calculate($start, $end);
    }

    public function calculate($start, $end) {
        $numeric_ip = ip2long($this->direccion_ip);
        $numeric_network_mask = ip2long($this->mascara);

        $numeric_network_address = ($numeric_ip & $numeric_network_mask);
        $numeric_broadcast = $numeric_network_address | (~$numeric_network_mask);

        $this->red = long2ip($numeric_network_address);
        $this->broadcast = long2ip($numeric_broadcast);

        $this->ip_inicio = long2ip($numeric_network_address + $start);
        $this->ip_fin = long2ip($numeric_broadcast - $end);

        $this->default_gw = $this->direccion_ip;
        $this->tftp = $this->direccion_ip;
        $this->ntp = $this->direccion_ip;
        $this->dns = $this->direccion_ip;
    }
}