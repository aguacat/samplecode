<?php

namespace App\Utilities;

class GetCurrentStatusText {
    public $es_servicio;
    public $estado;

    // public function __construct(bool $es_servicio = true) {
    //     $this->es_servicio = $es_servicio;
    // }

    public function setEstado($estado) {
        $this->estado = $estado;
    }

    public function setEsServicio($es_servicio) {
        $this->es_servicio = $es_servicio;
    }

    public function getText() {
        if ($this->es_servicio) {
            return ($this->estado === 0) ? "UP" : "DOWN";
        } else {
            switch ($this->estado) {
                case "0":
                    return "OK";
                    break;
                case "1":
                    return "WARNING";
                    break;
                case "2":
                    return "CRITICAL";
                    break;
                default:
                    return "UNREACHABLE";
                    break;
            }
        }
    }

    public function getColor() {
        if ($this->es_servicio) {
            return ($this->estado === 0) ? "#26C281" : "#D91E18";
        } else {
            switch ($this->estado) {
                case "0":
                    return "#26C281";
                    break;
                case "1":
                    return "#F3C200";
                    break;
                case "2":
                    return "#E87E04";
                    break;
                default:
                    return "#BFCAD1";
                    break;
            }
        }
    }
}