<?php

namespace App\Http\Middleware;

use Closure;;
use Illuminate\Support\Facades\Log;

class LogMiddleware
{
    private $start;
    private $end;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->start = microtime(true);
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $this->end = microtime(true);
        $this->log($request, $response);
    }

    public function log($request, $response)
    {
        $duration = $this->end - $this->start;
        $url = $request->fullUrl();
        $method = $request->getMethod();
        $ip = $request->getClientIp();
        $status = $response->status();

        $log = "{$ip}: [{$status}] {$method}@{$url} - {$duration}ms";

        Log::info($log);
    }
}
