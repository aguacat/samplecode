<?php

namespace App\Helpers;

class AppHelper
{
    /**
     * @param string $str
     *
     * @return string $str
     */
    public static function cleanUpSpecialChars($str)
    {
        $str = str_replace(['À', 'Á', 'Â', 'Ã', 'Ä', 'Å'], 'A', $str);
        $str = str_replace(['à', 'á', 'â', 'ã', 'ä', 'å'], 'a', $str);
        $str = str_replace(['È', 'É', 'Ê', 'Ë'], 'E', $str);
        $str = str_replace(['è', 'é', 'ê', 'ë'], 'e', $str);
        $str = str_replace(['Ì', 'Í', 'Ê', 'Ï'], 'I', $str);
        $str = str_replace(['ì', 'í', 'Î', 'ï'], 'i', $str);
        $str = str_replace(['Ò', 'Ó', 'Ô', 'Ö', 'Õ'], 'O', $str);
        $str = str_replace(['ò', 'ó', 'ô', 'ö', 'õ'], 'o', $str);
        $str = str_replace(['Ù', 'Ú', 'Û', 'Ü'], 'U', $str);
        $str = str_replace(['ù', 'ú', 'û', 'ü'], 'u', $str);
        $str = str_replace(['Ñ'], 'N', $str);
        $str = str_replace(['ñ'], 'n', $str);

        return $str;
    }

    /**
     * @param string $str
     * @param array  $noStrip
     *
     * @return string $str
     */
    public static function toCamelCase($str, array $noStrip = [])
    {
        $str = AppHelper::cleanUpSpecialChars($str);

        // non-alpha and non-numeric characters become spaces
        $str = preg_replace('/[^a-z0-9'.implode('', $noStrip).']+/i', ' ', $str);
        $str = trim($str);

        // uppercase the first character of each word
        $str = ucwords($str);
        $str = str_replace(' ', '', $str);
        $str = lcfirst($str);

        return $str;
    }
}
