<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Crypt;
use App\Utilities\HashingComponent;

class SetHardwareVerification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'key:nasvox';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set the NASVOX hardware verification key';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $commandPassword = '';

        // Ask for password;
        while(empty($commandPassword)) {
            $commandPassword = $this->secret('Please, provide NASVOX key generator password: ');

            if($commandPassword != 'laley159') {
                $this->info('The provided password do not match our records. Try again.');
                $commandPassword = '';
            }
        }

        // Send it to .env file. 
        // Append the new environment variable to the file.
        try {
            \File::get('.env');
            $hashingComponent = new HashingComponent();
            
            // Encrypting Value:
            $encrypted_value = Crypt::encrypt($hashingComponent->getHashInfo());

            $path = base_path('.env');
            if (file_exists($path)) {
                file_put_contents($path, str_replace(
                    'NASVOX_KEY='.$this->laravel['config']['app.nasvox_key'], 'NASVOX_KEY='.$encrypted_value, file_get_contents($path)
                ));
            }

            $this->info($encrypted_value);
            $this->info('NASVOXv4 Key generated successfully.');
        } catch(\Illuminate\Contracts\Filesystem\FileNotFoundException $e) {
            $this->error('Unable to load the .env file.');
        } catch(\Exception $e) {
            $this->error('Somehow something along the process failed.');
            $this->error($e);
        }
    }
}
